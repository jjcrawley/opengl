#pragma once

#include <string>
#include <vector>
#include <fstream>
#include <utility>
#include "Array.h"

struct Vector3
{
	float X;
	float Y;
	float Z;
};

struct Vector4
{
	float X;
	float Y;
	float Z;
	float W;
};

enum class Keyword
{	
	VS,
	PS,
	Shaders,
	Properties,
	True,
	False,
	Unknown
};

enum class PropertyType
{	
	Vector4,
	Vector3,
	Bool,
	Float,
	Int,
	Colour,
	Texture, 
	Unknown
};

enum class ValueType
{
	Bool,
	Float,
	Int,
	String,
	Unknown
};

enum class BlockType
{
	Shaders,
	Properties,
	Unknown
};

enum class Token
{	
	Operator,
	Keyword,
	PropertyType,
	Value,
	Unknown
};

struct MaterialToken
{
	std::string token;
	Token tokenType;

	int line;

	union
	{
		Keyword keyword;
		PropertyType propertyType;
		ValueType value;
	};

	union
	{
		float floatValue;
		bool boolValue;
		int intValue;
	};

	inline bool IsString()
	{
		return tokenType == Token::Value && value == ValueType::String;
	}

	inline bool IsNumber()
	{
		return tokenType == Token::Value && (value == ValueType::Float || value == ValueType::Int);
	}

	inline bool IsUnknown()
	{
		return tokenType == Token::Unknown;
	}

	inline bool IsBool()
	{
		return tokenType == Token::Value && value == ValueType::Bool;
	}

	inline float GetFloat()
	{
		return value == ValueType::Float ? floatValue : intValue;
	}

	inline bool IsOperator(char operatorToCheck)
	{
		return tokenType == Token::Operator && token[0] == operatorToCheck;
	}

	inline bool IsProperty()
	{
		return Token::PropertyType == tokenType;
	}

	inline Keyword GetKeyword()
	{
		return Token::Keyword == tokenType ? keyword : Keyword::Unknown;
	}

	inline PropertyType GetProperty()
	{
		return tokenType == Token::PropertyType ? propertyType : PropertyType::Unknown;
	}

	inline ValueType GetValueType()
	{
		return tokenType == Token::Value ? value : ValueType::Unknown;
	}
};

struct MaterialProperty
{
	std::string name;
	
	PropertyType type;
	
	std::string stringValue;

	union
	{
		int intValue;
		float floatValue;		
		Vector4 vectorValue;
	};	

	void Initialise(std::string name, PropertyType type)
	{
		this->name = name;
		this->type = type;

		switch (type)
		{
			case PropertyType::Vector4:				
			case PropertyType::Vector3:
			case PropertyType::Colour:
				vectorValue.X = 0.0f;
				vectorValue.Y = 0.0f;
				vectorValue.Z = 0.0f;
				vectorValue.W = 0.0f;
				break;
			case PropertyType::Int:
			case PropertyType::Bool:
				intValue = 0;
				break;
			case PropertyType::Float:
				floatValue = 0.0f;
				break;					
			case PropertyType::Texture:
				stringValue = "";
				break;
			case PropertyType::Unknown:
				break;
			default:
				break;
		}
	}
};

struct ParserError
{
	int lineNumber;
	std::string errorMessage;
};

class MaterialParser
{
	std::vector<MaterialProperty> m_propertys;

	std::fstream m_fileStream;

	std::string m_vertexShader;
	std::string m_pixelShader;

	std::vector<ParserError> m_errorLog;
	int m_currentLine;

	std::vector<char> m_operators = { '=', '(', ')', '{', '}', ',', '\"', '\n' };
	std::vector<std::pair<std::string, PropertyType>> m_propertyTypes;
	std::vector<std::pair<std::string, Keyword>> m_keywords;
		
	// Parsing state variables
	std::vector<MaterialToken> m_tokens;
	BlockType m_currentBlock;
	int m_blockCount;

	bool m_parseSuccessful = false;

	void PushError(const std::string& message);
	void PushError(const std::string& message, const MaterialToken& token);
	void PushError(const std::string& message, int line);

	MaterialToken MakeToken(const std::string& token);
	// Checks the token value and populates the token with the correct value
	void ClassifyNumberToken(MaterialToken& token);
	Keyword GetKeywordType(const std::string& prop);
	PropertyType GetPropertyType(const std::string& token);
		
	void ProcessTokens();

	void CreateToken(const std::string& token, bool isString = false);
	void CreateOperatorToken(char token);

public:
	void ParseFile(const std::string& filename);
	void PrintOutStats();
	void PrintErrorLog();
	MaterialParser();
};