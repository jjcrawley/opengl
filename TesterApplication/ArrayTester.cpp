#include "ArrayTester.h"
#include "Algorithms.h"

#define TestingTemplates

#include "Array.h"
#include <iostream>
#include <algorithm>
#include <utility>
#include <string>
#include <type_traits>

size_t Counter::callsToAlloc = 0;
size_t Counter::callsToDestroy = 0;

// A search predicate wrapper that automatically dereferences the array value being checked
template<typename T, typename U, bool dereference, typename Predicate>
struct SearchPredicate
{
	const Predicate& pred;
	Dereferencer<T, dereference> derefT;

	constexpr SearchPredicate(const Predicate& predicate) : pred(predicate)
	{}

	inline bool operator()(const T& left, const U& right) const
	{
		return pred(derefT(left), right);
	}

	inline bool operator()(T& left, U& right)
	{
		return pred(derefT(left), right);
	}
};

// A template that will automatically dereference any pointer values given to it
template<typename T, bool dereference, typename Predicate>
struct SortingPredicate
{
	const Predicate& pred;
	Dereferencer<T, dereference> derefT;

	constexpr SortingPredicate(const Predicate& predicate) : pred(predicate)
	{}

	bool operator()(T& left, T& right)
	{
		return pred(derefT(left), derefT(right));
	}

	constexpr bool operator()(const T& left, const T& right) const
	{
		return pred(derefT(left), derefT(right));
	}
};

template<typename T, typename Predicate = LessThan<T>>
bool IsSorted(T values[], int count, const Predicate& pred = Predicate())
{
	for (int i = 1; i < count; ++i)
	{
		if (pred(values[i], values[i - 1]))
		{
			return false;
		}
	}

	return true;
}

template<typename T, bool dereference = true, typename Predicate = LessThan<T>>
void InsertionSort(T values[], int count, const Predicate& pred = Predicate())
{
	SortingPredicate<T, dereference, Predicate> predicate(pred);

	// Sort the integer array with insertion sort
	// Loop until we reach the end of array
	for (int i = 0; i < count; ++i)
	{
		T current = values[i];

		int j = i - 1;

		// Loop backwards from the current position
		// While looping backwards, move each visited element forward by 1 index until we find the correct position for the current value
		while (j >= 0 && predicate(current, values[j]))
		{
			values[j + 1] = values[j];
			--j;
		}

		// Slot the value into its correct position
		values[j + 1] = current;
	}
}

void InsertionSort()
{
	std::cout << "\n----------------------------\n";
	std::cout << "Sorting using Insertion Sort";
	std::cout << "\n----------------------------\n";
		
	const int arraySize = 20;

	int integers[arraySize];

	for (int i = 0; i < arraySize; ++i)
	{
		integers[i] = rand();
	}

	int* integerPointers[arraySize];

	for (int i = 0; i < arraySize; ++i)
	{
		integerPointers[i] = &integers[i];
	}

	Sort::InsertionSort<Dereference::True>(integerPointers, arraySize, GreaterThan<int>());

	//InsertionSort(integerPointers, arraySize, GreaterThan<int>());
	
	if (!IsSorted(integerPointers, arraySize, SortingPredicate<int*, true, GreaterThan<int>>(GreaterThan<int>())))
	{
		std::cout << "The array isn't ordered\n";
	}

	for (int i = 0; i < arraySize; ++i)
	{
		std::cout << *integerPointers[i] << ", ";
	}

	std::cout << "\n";
}

template<typename T, bool dereference = true, typename Predicate = LessThan<T>>
void SelectionSort(T values[], int count, const Predicate& pred = Predicate())
{
	SortingPredicate<T, dereference, Predicate> predicate(pred);

	for (int i = 0; i < count; ++i)
	{
		int smallest = i;
				
		// Loop over the non-sorted part of the array to find the index of the smallest value
		for (int j = i + 1; j < count; ++j)
		{
			if (predicate(values[j], values[smallest]))
			{
				smallest = j;
			}
		}

		// Swap our smallest value with the current index in the array
		Swap(values[i], values[smallest]);
	}
}

void SelectionSort()
{
	std::cout << "\n----------------------------\n";
	std::cout << "Sorting using Selection Sort";
	std::cout << "\n----------------------------\n";
		
	const int arraySize = 20;

	int integers[arraySize];

	for (int i = 0; i < arraySize; ++i)
	{
		integers[i] = rand();
	}

	Sort::SelectionSort(integers, arraySize);

	for (int i = 0; i < arraySize; ++i)
	{
		std::cout << integers[i] << ", ";
	}

	std::cout << "\n";

	if (!IsSorted(integers, arraySize))
	{
		std::cout << "The array isn't ordered\n";
	}
}

template<typename T, bool dereference = true, typename Predicate = LessThan<T>>
void BubbleSort(T values[], int count, const Predicate& pred = Predicate())
{
	SortingPredicate<T, dereference, Predicate> predicate(pred);

	// Modified version of bubble sort, reduces the number of swaps that take place
	for (int iteration = 0; iteration < count; ++iteration)
	{
		// the value to swap
		T toSwap = values[0];

		int j = 1;

		// Loop till we hit our (size - CurrentIteration)
		while(j < count - iteration)
		{
			// If our integer is less than our current value, we move the integer back one
			if (predicate(values[j], toSwap))
			{
				values[j - 1] = values[j];
			}
			else
			{
				// Otherwise, our current value is placed at our previous index and we store the current value
				values[j - 1] = toSwap;
				toSwap = values[j];
			}

			++j;
		}

		// Place our current value in the correct position
		values[j - 1] = toSwap;
	}
}

void BubbleSort()
{
	std::cout << "\n------------------------\n";
	std::cout << "Sorting using Bubble Sort";
	std::cout << "\n------------------------\n";

	const int arraySize = 20;

	int integers[arraySize];

	for (int i = 0; i < arraySize; ++i)
	{
		integers[i] = rand();
	}

	Sort::BubbleSort(integers, arraySize);
	
	for (int i = 0; i < arraySize; ++i)
	{
		std::cout << integers[i] << ", ";
	}

	std::cout << "\n";

	if (!IsSorted(integers, arraySize))
	{
		std::cout << "The array isn't ordered\n";
	}
}

template<typename T, bool dereference, typename Predicate>
int LomutoPartition(int start, int end, T values[], const Predicate& pred)
{
	SortingPredicate<T, dereference, Predicate> predicate(pred);

	// Select our pivot
	T pivot = values[start];
	
	int pivotIndex = start;

	// Keep looping until we reach the end index
	for (int i = start + 1; i < end; ++i)
	{
		// If the current integer is < pivot we move the pivot index forward and swap the integer at the pivot index with the current value
		if (predicate(values[i], pivot))
		{
			++pivotIndex;
			Swap(values[pivotIndex], values[i]);
		}
	}

	// If we ever moved our pivot index
	if (start != pivotIndex)
	{
		// put the pivot into its correct place
		values[start] = values[pivotIndex];
		values[pivotIndex] = pivot;
	}

	return pivotIndex;
}

template<typename T, bool dereference, typename Predicate>
int HoarePartition(int start, int end, T values[], const Predicate& pred)
{
	SortingPredicate<T, dereference, Predicate> predicate(pred);

	// Select the pivot
	T pivot = values[start];

	int lower = start - 1;
	int higher = end + 1;

	// Keep looping until we break out of the loop ourselves
	while (true)
	{
		// Checks if the lower value is less than the pivot, it is left alone if it is
		while (predicate(values[++lower], pivot))
		{}
				
		// Checks whether the higher value is greater than the pivot, left alone if it is
		while(predicate(pivot, values[--higher]))
		{ }
		
		// if our higher and lower invert or overlap, we can leave
		if (lower >= higher)
		{
			return higher;
		}

		// Swap our higher and lower value
		Swap(values[lower], values[higher]);
	}
}

template<typename T, bool dereference = true, typename Predicate = LessThan<T>>
void QuickSort(int start, int end, T values[], const Predicate& pred = Predicate())
{
	// If our start is > than our end index we can leave, the partition is probably one element
	if (start >= end)
	{
		return;
	}

	// Retrieve the pivot index using either Hoare partitioning 
	int pivotIndex = HoarePartition<T, dereference>(start, end, values, pred);

	// Recur using the left partition, then the right partition
	QuickSort<T, dereference>(start, pivotIndex, values, pred);
	QuickSort<T, dereference>(pivotIndex + 1, end, values, pred);
	
	// Quick sort using the Lomuto partitioning algorithm instead
	//int pivotIndex = LomutoPartition<T, dereference>(start, end, values);

	// Slightly different setup to Hoare quicksorting, as the pivot is already in the correct place
	//QuickSort(start, pivotIndex - 1, integers);
	//QuickSort(pivotIndex + 1, end, integers);
}

template<typename T, bool dereference = true, typename Predicate = LessThan<T>>
void QuickSortIterative(int start, int end, T values[], const Predicate& pred = Predicate())
{
	int* partitionStack = new int[end - start + 1];
	
	int stackIndex = -1;

	partitionStack[++stackIndex] = start;
	partitionStack[++stackIndex] = end;

	while (stackIndex > 0)
	{
		end = partitionStack[stackIndex--];
		start = partitionStack[stackIndex--];
		
		int leftEnd = HoarePartition<T, dereference>(start, end, values, pred);
		
		// Push the right partition if we have more than one element in it
		if(leftEnd + 1 < end)
		{
			partitionStack[++stackIndex] = leftEnd + 1;
			partitionStack[++stackIndex] = end;
		}	
		
		// Push the left partition if we have more than one element in it
		if(start < leftEnd)
		{
			partitionStack[++stackIndex] = start;
			partitionStack[++stackIndex] = leftEnd;
		}					
	}

	delete[] partitionStack;
}

void QuickSort()
{
	std::cout << "\n------------------------\n";
	std::cout << "Sorting using Quick Sort";
	std::cout << "\n------------------------\n";

	const int arraySize = 20;

	int integers[arraySize];

	for (int i = 0; i < arraySize; ++i)
	{
		integers[i] = rand();
	}
	
	Sort::QuickSort(integers, arraySize);

	//QuickSortIterative(0, arraySize - 1, integers);

	//QuickSort(0, arraySize - 1, integers);

	for (int i = 0; i < arraySize; ++i)
	{
		std::cout << integers[i] << ", ";
	}

	std::cout << "\n";

	if (!IsSorted(integers, arraySize))
	{
		std::cout << "The array isn't ordered\n";
	}	
}



// Merges two subsets together into an ordered set
// Start is the index of the first element in subset 1, and mid is the end index of subset 1
// Mid + 1 is the start index of subset 2 with the end index being the final index of the subset
template<typename T, bool dereference, typename Predicate>
void Merge(int start, int mid, int end, T values[], const Predicate& pred)
{	
	SortingPredicate<T, dereference, Predicate> predicate(pred);

	int elementCount = end - start + 1;

	T* tempArray = new T[elementCount];

	int rightPos = mid + 1;
	int leftPos = start;
	int writingIndex = 0;
	
	// If both our partitions still have elements to write, we do a regular comparision check to decide which element to use
	while (leftPos <= mid && rightPos <= end)
	{	
		// If our right value is < left value we write the right value
		if (predicate(values[rightPos], values[leftPos]))
		{
			tempArray[writingIndex] = values[rightPos++];
		}
		else
		{
			// Else we write the left value
			tempArray[writingIndex] = values[leftPos++];
		}
		
		++writingIndex;
	}

	// If we leave the previous loop we know that we've reached the end of one of our partitions
	// This means that we may still have elements in the other partition to write in, so we right in the remaining elements from the appropriate partition
	// If our left partition still has elements in them, slot them in
	while (leftPos <= mid)
	{
		tempArray[writingIndex++] = values[leftPos++];
	}

	// If our right partition still has elements in them, we slot them in
	while (rightPos <= end)
	{
		tempArray[writingIndex++] = values[rightPos++];
	}

	T* current = &values[start];

	for (int i = 0; i < elementCount; ++i, ++current)
	{
		*current = tempArray[i];
	}	

	delete[] tempArray;
}

template<typename T, bool dereference, typename Predicate = LessThan<T>>
void MergeInPlace(int start, int mid, int end, T values[], const Predicate& pred)
{
	SortingPredicate<T, dereference, Predicate> predicate(pred);

	int leftPos = start;
	int leftEnd = mid;

	int rightPos = mid + 1;
	
	// Keep looping while both partitions have elements to examine
	while (leftPos <= leftEnd && rightPos <= end)
	{
		if (predicate(values[rightPos], values[leftPos]))
		{
			// We need to move the value from the right partition into its correct place	

			// We first store the rightValue that we are slotting in, we do this to prevent the shuffle from destroying it
			// We increment rightPos to move the starting position of the right partition forward, as we've essentially removed it from the right partition			
			T rightValue = values[rightPos++];

			int j = leftEnd;

			// Shuffle all elements in the left partition to the right by 1
			while (j >= leftPos)
			{
				values[j + 1] = values[j];
				--j;
			}
			
			// We slot the right value into its correct place, at the old left start
			values[leftPos] = rightValue;

			// Alternatively we perform a left rotation of -1 on the values from leftPos to rightPos + 1
			//RotateArray(values + leftPos, rightPos - leftPos + 1, -1);
			//++rightPos;

			// leftPos and leftEnd are incremented because we moved them forward by one			
			++leftPos;
			++leftEnd;
		}		
		else
		{			
			++leftPos;
		}
	}

	// Once one of the partitions becomes empty the elements are sorted
}

template<typename T, bool dereference = true, typename Predicate = LessThan<T>>
void MergeSort(int start, int end, T values[], const Predicate& pred = Predicate())
{
	if (start >= end)
	{
		return;
	}

	int mid = (start + end) / 2;

	// Here we recur into the left partition, then the right
	MergeSort<T, dereference>(start, mid, values, pred);
	MergeSort<T, dereference>(mid + 1, end, values, pred);

	//Merge<T, dereference>(start, mid, end, integers, pred);
	MergeInPlace<T, dereference>(start, mid, end, values);
}

template<typename T, bool dereference = true, typename Predicate = LessThan<T>>
void MergeUpSort(int start, int end, T values[], const Predicate& pred = Predicate())
{
	int size = end - start;

	int mid;
	int partitionEnd;

	// The bottom-up approach involves treating the incoming array as a group of subsets of one element, then combining those subsets together until the array is sorted
	// The partition size starts out as 1 then is multiplied by 2 after each iteration. The sequence will be: 1, 2, 4, 8, 16, etc.
	for (int partitionSize = 1; partitionSize < size; partitionSize *= 2)
	{
		// Our current partition starting index
		for (int partition = 0; partition < end; partition += 2 * partitionSize)
		{
			// We grab the back of the left partition, aka mid.
			mid = partition + partitionSize - 1;

			// Also grab the end of the right partition, min prevents it from going out of range
			partitionEnd =  std::min(mid + partitionSize, end);
					
			// Combine the left partition with the right partition
			MergeInPlace<T, dereference>(partition, mid, partitionEnd, values, pred);
		}
	}
}

void MergeSort()
{
	std::cout << "\n------------------------\n";
	std::cout << "Sorting using Merge Sort";
	std::cout << "\n------------------------\n";
	
	const int arraySize = 20;

	int integers[arraySize];

	for (int i = 0; i < arraySize; ++i)
	{
		integers[i] = rand();
	}
		
	Sort::MergeSort(integers, arraySize);

	//MergeUpSort(0, arraySize - 1, integers);

	for (int i = 0; i < arraySize; ++i)
	{
		std::cout << integers[i] << ", ";
	}

	std::cout << "\n";

	if (!IsSorted(integers, arraySize))
	{
		std::cout << "The array isn't ordered\n";
	}
}

template<typename T, typename Predicate>
void SiftUpHeap(T values[], int child, const Predicate& pred)
{
	int parent = (child - 1) / 2;

	while (pred(values[parent], values[child]))
	{
		Swap(values[parent], values[child]);

		child = parent;
		parent = (child - 1) / 2;
	}
}

template<typename T, typename Predicate>
void SiftDownHeap(T values[], int parent, int count, const Predicate& pred)
{	
	int child = parent * 2 + 1;

	// While we have a left child
	while (child < count)
	{
		// The right child of this node is always going to be one index greater than the left
		int rightChild = child + 1;

		// If we have a right child check the right value, if greater use that index instead of the left
		if (rightChild < count && pred(values[child], values[rightChild]))
		{
			child = rightChild;
		}

		if (pred(values[parent], values[child]))
		{
			Swap(values[parent], values[child]);
		}

		parent = child;
		child = parent * 2 + 1;
	}
}

template<typename T, bool dereference = true, typename Predicate = LessThan<T>>
void CreateHeap(T values[], int count, const Predicate& pred = Predicate())
{
	// We use -2 as it's akin to ((count - 1) - 1) / 2
	// (count - 1) gives us the end index. It then becomes (endIndex - 1) / 2, which is the formula used to grab the parent
	int parent = (count - 2) / 2;

	SortingPredicate<T, dereference, Predicate> predicate(pred);

	while (parent >= 0)
	{
		SiftDownHeap(values, parent, count, predicate);
		--parent;
	}
}

template<typename T, bool dereference = true, typename Predicate = LessThan<T>>
void InsertHeap(T values[], int& count, const T& value, const Predicate& pred = Predicate())
{	
	++count;

	int childIndex = count - 1;

	values[childIndex] = value;	

	SortingPredicate<T, dereference, Predicate> predicate(pred);
	
	SiftUpHeap(values, childIndex, predicate);
}

template<int childrenCount, typename T, typename Predicate>
void SiftDownHeapMulti(T values[], int parent, int count, const Predicate& pred)
{	
	int child = parent * childrenCount + 1;

	// While we have a left child
	while (child < count)
	{
		// Calculate the index of the right most child
		// Use a min to prevent it from going out of the range of the array
		int rightChild = std::min(child + childrenCount - 1, count - 1);

		// Loop through each child and grab the largest one
		for (int currentChild = child + 1; currentChild <= rightChild; ++currentChild)
		{
			if (pred(values[child], values[currentChild]))
			{
				child = currentChild;
			}
		}
		
		if (pred(values[parent], values[child]))
		{
			Swap(values[child], values[parent]);
		}
					
		// Check the next parent
		parent = child;
		child = parent * childrenCount + 1;
	}
}

template<int childrenCount, typename T, typename Predicate>
void SiftUpHeapMulti(T values[], int child, const Predicate& pred)
{
	static_assert(childrenCount > 1, "Child count less than two in SiftUpheapMulti method");

	int parent = (child - 1) / childrenCount;

	// Keep swapping child with parent whilst the child is greater than the parent
	while (pred(values[parent], values[child]))
	{
		Swap(values[parent], values[child]);

		child = parent;
		parent = (child - 1) / childrenCount;
	}
}

template<int childrenCount, typename T, bool dereference = true, typename Predicate = LessThan<T>>
void HeapInsert(T values[], int& count, const T& value, const Predicate& pred = Predicate())
{
	static_assert(childrenCount > 1, "Child count less than two in HeapInsert method");

	++count;

	int childIndex = count - 1;

	values[childIndex] = value;

	SortingPredicate<T, dereference, Predicate> predicate(pred);

	SiftUpHeapMulti<childrenCount>(values, childIndex, predicate);
}

template<int childrenCount, typename T, bool dereference = true, typename Predicate = LessThan<T>>
void Heapify(T values[], int count, const Predicate& pred = Predicate())
{
	static_assert(childrenCount > 1, "Child count less than two in Heapify method");

	int parent = (count - 2) / childrenCount;

	SortingPredicate<T, dereference, Predicate> predicate(pred);

	while (parent >= 0)
	{
		SiftDownHeapMulti<childrenCount>(values, parent, count, predicate);
		--parent;
	}
}

template<typename T, bool dereference = true, typename Predicate = LessThan<T>>
void HeapSort(T values[], int count, const Predicate& pred = Predicate())
{
	// Heapsort is a sorting algorithm that operates on data that has been arranged in a heap
	// We assume that the data is already in a heap structure to start off.

	SortingPredicate<T, dereference, Predicate> predicate(pred);

	while (--count > 0)
	{
		Swap(values[count], values[0]);
				
		SiftDownHeap(values, 0, count, predicate);
	}
}

template<int childrenCount, typename T, bool dereference = true, typename Predicate = LessThan<T>>
void HeapSortMulti(T values[], int count, const Predicate& pred = Predicate())
{
	static_assert(childrenCount > 1, "Child count less than two in HeapSortMulti method");

	SortingPredicate<T, dereference, Predicate> predicate(pred);

	while (--count > 0)
	{
		Swap(values[count], values[0]);

		SiftDownHeapMulti<childrenCount>(values, 0, count, predicate);
	}
}

void HeapSort()
{	
	std::cout << "\n------------------------\n";
	std::cout << "Sorting using Heap Sort";
	std::cout << "\n------------------------\n";

	const int arraySize = 20;

	int integers[arraySize];

	for (int i = 0; i < arraySize; ++i)
	{
		integers[i] = rand();
	}

	//MultiHeap<2>::Heapify(integers, arraySize);

	//MultiHeap<2>::HeapSort(integers, arraySize);

	//Heap::Heapify(integers, arraySize);

	Heap::HeapSort(integers, arraySize);

	//CreateHeap(integers, arraySize);

	//HeapSort(integers, arraySize);

	for (int i = 0; i < arraySize; ++i)
	{
		std::cout << integers[i] << ", ";
	}

	std::cout << "\n";

	if (!IsSorted(integers, arraySize))
	{
		std::cout << "The array isn't ordered\n";
	}	
}

template<typename T, bool dereference = true, typename Predicate = LessThan<T>>
void ShellSort(T integers[], int count, const Predicate& pred = Predicate())
{
	// Shell sort is a derivation of insertion sort.
	// It uses a gap value to create a subset of the data and performs insertion sort on that data	
	// For example, array {2, 3, 6, 1, 4, 9} with a gap value of 2 will have the subset: {2, 6, 4}, we then perform insertion sort on the subset
	
	// Here, we get the largest gap value and work backwards, reducing the size of the gap with each iteration
	//for (int gap = count - 1; gap > 0; gap /= 2)

	// Alternatively, we can have a smaller gap and increase it, in this case we use a formula from Wikipedia: 2 ^ k + 1
	// We don't need to remember k as we can infer it from the starting gap value, 3.
	// The original sequence starts with 1 but that would be a regular insertion sort, defeating the purpose of using shell sort 
	int gap = 3;

	SortingPredicate<T, dereference, Predicate> predicate(pred);

	while(gap < count)
	{
		// Check each value that is part of this set, ie: all values that are gap indexes away from one another
		for (int i = gap; i < count; i += gap)
		{
			T current = integers[i];

			int j = i - gap;

			// Shuffle the elements backwards in the subset
			while (j >= 0 && predicate(current, integers[j]))
			{
				integers[j + gap] = integers[j];
				j -= gap;
			}

			// Slot in the value to its correct position
			integers[j + gap] = current;
		}

		// Calculate the new gap value, this math means we don't have to remember k
		// This should be commented out if we want to start with the largest gap value
		gap = (gap - 1) * 2 + 1;
	}

	InsertionSort<T, dereference>(integers, count, predicate);
}

void ShellSort()
{
	std::cout << "\n------------------------\n";
	std::cout << "Sorting using Shell Sort";
	std::cout << "\n------------------------\n";

	const int arraySize = 20;

	int integers[arraySize];

	for (int i = 0; i < arraySize; ++i)
	{
		integers[i] = rand();
	}
		
	Sort::ShellSort(integers, arraySize);

	for (int i = 0; i < arraySize; ++i)
	{
		std::cout << integers[i] << ", ";
	}

	std::cout << "\n";

	if (!IsSorted(integers, arraySize))
	{
		std::cout << "The array isn't ordered\n";
	}
}

template<typename T, bool dereference = true, typename Predicate = LessThan<T>>
void CombSort(T values[], int count, const Predicate& pred = Predicate())
{
	int gap = count - 1;

	bool swapped = false;

	SortingPredicate<T, dereference, Predicate> predicate(pred);

	// We keep looping if we have a gap > 1 or we made a swap
	// With a gap size of 1 it's just a bubble sort
	while (gap > 1 || swapped)
	{
		// Same as dividing by 1.3, but keeps it as an int
		gap = (gap * 10) / 13;

		gap = std::max(gap, 1);

		swapped = false;

		for (int i = gap; i < count; ++i)
		{	
			if (predicate(values[i], values[i - gap]))
			{
				Swap(values[i - gap], values[i]);
				swapped = true;
			}			
		}
	}
}

void CombSort()
{
	std::cout << "\n------------------------\n";
	std::cout << "Sorting using Comb Sort";
	std::cout << "\n------------------------\n";

	const int arraySize = 20;

	int integers[arraySize];
		
	for (int i = 0; i < arraySize; ++i)
	{
		integers[i] = rand();
	}
	
	Sort::CombSort(integers, arraySize, GreaterThan<int>());

	//CombSort(integers, arraySize, GreaterThan<int>());

	for (int i = 0; i < arraySize; ++i)
	{
		std::cout << integers[i] << ", ";
	}

	std::cout << "\n";

	if (!IsSorted(integers, arraySize, GreaterThan<int>()))
	{
		std::cout << "The array isn't ordered\n";
	}
}

int GetMax(int integers[], int count)
{
	int maxIndex = 0;

	for (int i = 0; i < count; ++i)
	{
		if (integers[maxIndex] < integers[i])
		{
			maxIndex = i;
		}
	}

	return maxIndex;
}

void Flip(int integers[], int start, int end)
{
	while (start < end)
	{
		int temp = integers[start];
		integers[start] = integers[end];
		integers[end] = temp;

		++start;
		--end;
	}
}

void PancakeSort(int integers[], int count)
{	
	// Pancake sort is a sorting algorithm that orders using flips rather than shuffling
	do
	{
		// First we have to find the maximum value of the array
		int maxIndex = GetMax(integers, count);
				
		// If the max index isn't already at the back
		if (maxIndex != count - 1)
		{
			// Default implementation
			{
				// We need to flip the array up to the max index.
				// Eg: In array {20, 30, 100, 2}, 100 is max 
				// So we flip up to that index, changing the array to: {100, 30, 20, 2}
				Flip(integers, 0, maxIndex);

				// After we've flipped the max value to the front we flip the array, up to count elements, to get it to the back.
				// Eg: {100, 30, 20, 2} becomes {2, 20, 30, 100}
				Flip(integers, 0, count - 1);
			}
			// Modified implementation
			{
				// Rather than flip to start then flip to end, we can flip straight to the end
				//Flip(integers, maxIndex, count - 1);
			}
		}
	}
	// Count is decremented so the last position is locked in, as it's in the correct position
	while (count--);
}

void PancakeSort()
{
	std::cout << "\n--------------------------\n";
	std::cout << "Sorting using Pancake Sort";
	std::cout << "\n--------------------------\n";

	const int arraySize = 20;

	int integers[arraySize];

	for (int i = 0; i < arraySize; ++i)
	{
		integers[i] = rand();
	}

	PancakeSort(integers, arraySize);

	for (int i = 0; i < arraySize; ++i)
	{
		std::cout << integers[i] << ", ";
	}

	std::cout << "\n";

	if (!IsSorted(integers, arraySize))
	{
		std::cout << "The array isn't ordered\n";
	}
}

template<typename T, typename U, bool deref = true, typename Predicate = LessThan<T>>
int BinarySearch(T* values, const int count, const U& value, const Predicate& pred = Predicate())
{
	Dereferencer<T, deref> derefT;
	Dereferencer<U, deref> derefU;

	int endIndex = count - 1;
	int start = 0;

	bool foundValue = false;

	int mid = 0;

	// While our start point doesn't go beyond the end index
	while (start <= endIndex)
	{
		mid = (start + endIndex) / 2;

		// First check the lower half
		if (pred(derefU(value), derefT(values[mid])))
		{
			endIndex = mid;
		}
		// Check the upper half
		else if (pred(derefT(values[mid]), derefU(value)))
		{
			start = mid + 1;
		}
		// If we aren't less than or greater than, we have our value
		else
		{
			start = mid;
			foundValue = true;
			break;
		}
	}

	if (foundValue)
	{
		return start;
	}
	else
	{
		return -1;
	}
}

template<bool deref, typename T, typename U, typename Predicate = LessThan<T>>
int BinarySearch(T* values, const int count, const U& value, const Predicate& pred = Predicate())
{
	return BinarySearch<T, U, deref>(values, count, value, pred);
}

struct PairLessThan
{
	bool operator()(const std::string& first, const std::pair<std::string, int>& pair) const
	{
		return first < pair.first;
	}

	bool operator()(const std::pair<std::string, int>& pair, const std::string& first) const 
	{
		return pair.first < first;
	}
};

struct PairEqualTo
{
	bool operator()(const std::pair<std::string, int>& pair, const std::string& first) const
	{
		return pair.first == first;
	}
};

void BinarySearch()
{
	std::cout << "\n--------------------------\n";
	std::cout << "Binary Search";
	std::cout << "\n--------------------------\n";

	std::pair<std::string, int> testers[7];

	testers[0] = std::make_pair("Bool", 2);
	testers[1] = std::make_pair("Float", 3);
	testers[2] = std::make_pair("Int", 4);
	testers[3] = std::make_pair("Vector3", 0);
	testers[4] = std::make_pair("Colour", 1);
	testers[5] = std::make_pair("Vector4", 9);
	testers[6] = std::make_pair("Texture", 7);

	Sort::InsertionSort(testers, 7);
		
	for (int i = 0; i < 7; ++i)
	{
		std::cout << "(" << testers[i].first << ", " << testers[i].second << "), ";
	}

	std::cout << "\n";

	if (!IsSorted(testers, 7))
	{
		std::cout << "The array isn't ordered\n";
	}	
	else
	{		
		int numb = 2;
		
		struct IntTest
		{
			bool operator()(int num1, int num2) const
			{
				return num1 < num2;
			}
		};
			
		auto wrapped = DeferenceWrapper(IntTest());
		
		std::pair<std::string, int>* pointerTest[7];

		pointerTest[0] = &testers[0];
		pointerTest[1] = &testers[1];
		pointerTest[2] = &testers[2];
		pointerTest[3] = &testers[3];
		pointerTest[4] = &testers[4];
		pointerTest[5] = &testers[5];
		pointerTest[6] = &testers[6];
				
		// Check algorithms with pointer types, appears to be a minor issue with instantiation of LessThan types with pointers
		int index = Search::BinarySearch<Dereference::True>(pointerTest, 7, std::string("Float"), PairLessThan());
				
		if (index != 2)
		{
			std::cout << "Got the wrong index for the test value\n";
		}

		index = Search::LinearSearch(testers, 7, std::string("Float"), PairEqualTo());

		if (index != 2)
		{
			std::cout << "Unable to find the value Float " << index << " \n";
		}
	}
}

template<typename T, typename U, bool dereference = true, typename Predicate = LessThan<T>>
int LowerBound(T* values, int count, const U& value, const Predicate& pred = Predicate())
{	
	Dereferencer<T, dereference> derefT;
	
	int start = 0;
	int mid = 0;

	while (count > 0)
	{
		count /= 2;

		mid = start + count;

		// Check the upper half of the array
		if (pred(derefT(values[mid]), value))
		{
			start = ++mid;
			// - 1 to consume mid
			count -= 1;
		}
	}
		
	return start;
}

template<typename T, typename U, bool dereference = true, typename Predicate = LessThan<T>>
int UpperBound(T* values, int count, const U& value, const Predicate& pred = Predicate())
{
	Dereferencer<T, dereference> derefT;

	int start = 0;
	int mid = 0;
	
	while (count > 0)
	{
		count /= 2;

		mid = start + count;

		if (!pred(value, derefT(values[mid])))
		{
			start = ++mid;
			count -= 1;
		}
	}

	return start;
}

void BoundSearch()
{
	std::cout << "\n--------------------------\n";
	std::cout << "Bound Search";
	std::cout << "\n--------------------------\n";

	int numbers[] = { 0, 2, 2, 2, 4, 4, 4, 6, 6, 7, 7, 8, 8, 8, 9 };

	int index = Search::LowerBound(numbers, 15, 0);

	std::cout << index << "\n";

	index = Search::UpperBound(numbers, 15, 2);

	std::cout << index << "\n";
}

// GCD is Greatest Common Divisor
int GCD(int a, int b)
{
	int mod = 0;

	while (b != 0)
	{
		mod = a % b;
		a = b;
		b = mod;
	}

	return a;
}

template<typename T>
void RotateArrayLeft(T values[], int count, int rotateBy)
{
	int gcd = GCD(count, rotateBy);

	for (int i = 0; i < gcd; ++i)
	{
		T temp = values[i];

		int currentIndex = i;

		while (true)
		{
			// Wrap the moveToIndex
			int moveToIndex = (currentIndex + rotateBy) % count;

			if (moveToIndex == i)
			{
				break;
			}

			// Replace the currentIndex with the value at moveToIndex
			values[currentIndex] = values[moveToIndex];
			currentIndex = moveToIndex;
		}

		values[currentIndex] = temp;
	}
}

template<typename T>
void RotateArray(T values[], int count, int rotateBy)
{
	if (rotateBy == 0 || count <= 1)
	{
		return;
	}

	rotateBy %= count;

	// Check to see if rotateBy is negative, this indicates a right rotation
	if (rotateBy < 0)
	{
		// Convert the rotation into a positive left rotation
		rotateBy = count + rotateBy;		
	}

	RotateArrayLeft(values, count, rotateBy);
}

void RotateArray()
{
	std::cout << "\n--------------------------\n";
	std::cout << "Array Rotation";
	std::cout << "\n--------------------------\n";

	int numbers[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };

	Algorithm::RotateArray(numbers, 12, -2);

	for (int i = 0; i < 12; ++i)
	{
		std::cout << numbers[i] << ", ";
	}

	std::cout << "\n";	
}

void IntroSort()
{
	std::cout << "\n--------------------------\n";
	std::cout << "Intro Sort";
	std::cout << "\n--------------------------\n";

	const int arraySize = 50;

	int integers[arraySize];

	for (int i = 0; i < arraySize; ++i)
	{
		integers[i] = rand();
	}

	Sort::IntroSort(integers, arraySize, LessThan<int>());

	for (int i = 0; i < arraySize; ++i)
	{
		std::cout << integers[i] << ", ";
	}

	std::cout << "\n";

	if (!IsSorted(integers, arraySize, LessThan<int>()))
	{
		std::cout << "The array isn't ordered\n";
	}
}

void ArrayTester::RunTests()
{
	std::cout << "Running array tests\n";

	//Array<int> numbers = {2, 3, 4, 5, 6, 7, 8, 10};
	//
	//for (int i = 0; i < numbers.Size(); ++i)
	//{
	//	if (numbers.ContainsSorted(numbers[i]) == -1)
	//	{
	//		std::cout << "Unable to find " << numbers.GetData()[i] << ".\n";
	//	}
	//}

	//if (numbers.ContainsSorted(2) == -1)
	//{
	//	//std::cout << "Unable to find " << numbers.GetData()[i] << ".\n";
	//}

	//if (numbers.ContainsSorted(0) != -1)
	//{
	//	std::cout << "Found 0 in the array, shouldn't be there is in array but couldn't be found\n";
	//}
	//	
	//if (numbers.ContainsSorted(40) != -1)
	//{
	//	std::cout << "Found 40 in the array, shouldn't be there\n";
	//}

	InsertionSort();		
	SelectionSort();
	BubbleSort();
	QuickSort();
	MergeSort();
	HeapSort();
	ShellSort();
	CombSort();
	PancakeSort();

	BinarySearch();
	BoundSearch();

	RotateArray();
	IntroSort();
}

#undef TestingTemplates