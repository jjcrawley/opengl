#include "MaterialParser.h"
#include <cstdio>
#include <iostream>
#include <algorithm>

using namespace std;

bool IsDigit(const char& character)
{
	return character >= '0' && character <= '9';
}

bool IsWhiteSpace(const char& character)
{
	return character == ' ' || character == '\t';
}

void MaterialParser::PushError(const std::string & message)
{
	PushError(message, m_currentLine);
}

void MaterialParser::PushError(const std::string& message, const MaterialToken& token)
{
	PushError(message + " \nToken: " + token.token, token.line);
}

void MaterialParser::PushError(const std::string& message, int line)
{
	ParserError error;
	error.errorMessage = message;
	error.lineNumber = line;

	m_errorLog.push_back(error);
}

struct PropertyComparator
{
	bool operator()(const string& first, const pair<string, PropertyType>& second)
	{
		return first < second.first;
	}

	bool operator()(const pair<string, PropertyType>& first, const string& second)
	{
		return first.first < second;
	}
};

struct KeywordComparator
{
	bool operator()(const pair<string, Keyword>& first, const string second)
	{
		return first.first < second;
	}

	bool operator()(const string first, const pair<string, Keyword>& second)
	{
		return first < second.first;
	}
};

MaterialToken MaterialParser::MakeToken(const std::string& token)
{	
	MaterialToken newToken;

	newToken.tokenType = Token::Unknown;
	newToken.keyword = Keyword::Unknown;
	newToken.token = token;
	
	auto keywordIterator = lower_bound(m_keywords.begin(), m_keywords.end(), token, KeywordComparator());

	if (keywordIterator != m_keywords.end() && keywordIterator->first == token)
	{
		newToken.tokenType = Token::Keyword;
		newToken.keyword = keywordIterator->second;

		// Special treatment for the true/false keywords
		if (newToken.keyword == Keyword::False)
		{
			//newToken.tokenType = Token::Value;
			newToken.tokenType = Token::Value;
			newToken.value = ValueType::Bool;
			newToken.boolValue = false;
		}
		else if (newToken.keyword == Keyword::True)
		{
			//newToken.tokenType = Token::Value;
			newToken.tokenType = Token::Value;
			newToken.value = ValueType::Bool;
			newToken.boolValue = true;
		}

		return newToken;
	}
	
	auto propertyIterator = lower_bound(m_propertyTypes.begin(), m_propertyTypes.end(), token, PropertyComparator());
	
	// If our current token is a value type
	if (propertyIterator != m_propertyTypes.end() && propertyIterator->first == token)
	{
		newToken.tokenType = Token::PropertyType;
		newToken.propertyType = propertyIterator->second;
		return newToken;
	}		
		
	// We attempt to classify the token as a number
	ClassifyNumberToken(newToken);		
			
	return newToken;
}

Keyword MaterialParser::GetKeywordType(const std::string& currentToken)
{
	if (currentToken == "VS")
	{
		return Keyword::VS;		
	}
	else if (currentToken == "PS")
	{
		return Keyword::PS;
	}	
	else if (currentToken == "true" || currentToken == "false")
	{
		if (currentToken == "true")
		{
			return Keyword::True;
		}
		else
		{
			return Keyword::False;
		}
	}
	else if (currentToken == "Shaders")
	{
		return Keyword::Shaders;
	}
	else if (currentToken == "Properties")
	{
		return Keyword::Properties;
	}
	else
	{
		return Keyword::Unknown;
	}	
}

PropertyType MaterialParser::GetPropertyType(const std::string& token)
{
	if (token == "Float")
	{
		return PropertyType::Float;
	}
	else if (token == "Texture")
	{
		return PropertyType::Texture;
	}
	else if (token == "Colour" || token == "Vector4" || token == "Vector3")
	{
		if (token == "Colour")
		{
			return PropertyType::Colour;
		}
		else if (token == "Vector4")
		{
			return PropertyType::Vector4;
		}
		else
		{
			return PropertyType::Vector3;
		}
	}
	else if (token == "Int" || token == "Bool")
	{
		if (token == "Int")
		{
			return PropertyType::Int;
		}
		else
		{
			return PropertyType::Bool;
		}
	}

	return PropertyType::Unknown;
}

// Populates a token with the necessary values for ints and floats
void MaterialParser::ClassifyNumberToken(MaterialToken& token)
{	
	// No need to check through string tokens or tokens that have a type
	if (token.IsString() || token.tokenType != Token::Unknown)
	{
		return;
	}	

	ValueType type = ValueType::Unknown;
		
	int size = (int) token.token.size();
		
	bool foundSign = false;
	bool foundPoint = false;

	// This loop will loop through and classify the token
	// Since this is only meant for ints and floats values, we are only expecting to see digits, one sign and one point
	for (int i = 0; i < size; ++i)
	{
		const char& currentCharacter = token.token[i];

		// If we are a digit add it to buffer
		if (IsDigit(currentCharacter))
		{
			// If we haven't found a point but we have a digit then we can assume it's an int
			if (!foundPoint)
			{
				type = ValueType::Int;
			}
			else
			{
				type = ValueType::Float;
			}
		}
		else if (currentCharacter == '-' || currentCharacter == '+' || currentCharacter == '.')
		{
			// Not allowed more than one point in the token
			if (currentCharacter == '.' && !foundPoint)
			{					
				foundPoint = true;				
			}
			// The + and - should only appear at the start of the number, any where else and we can't handle it
			else if (i == 0 && (currentCharacter == '+' || currentCharacter == '-'))
			{
				foundSign = true;
			}
			else
			{
				// In case the - or + are in places where they shouldn't be, rock up more than once or we have more than one point
				return;
			}
		}	
		else
		{
			// This would only happen if we see characters that we aren't expecting
			// This also means that an f at the end of a float is illegal, same goes for whitespace
			return;
		}
	}
		
	if (type == ValueType::Float)
	{
		token.floatValue = (float) atof(token.token.c_str());
	}
	else if (type == ValueType::Int)
	{
		token.intValue = (int) atoi(token.token.c_str());
	}
	else
	{
		return;
	}

	token.tokenType = Token::Value;
	token.value = type;
}

#define ThrowError(message) cout << message << endl; return false;

void MaterialParser::ProcessTokens()
{

#define ThrowTokenError(message, token) PushError(message, token); break

	// Treat the token list as a stack
	// Start at back, move to front
	// If current token == \n, consume and keep moving. This is required anyway
	// If current token == '}' || '{' do block operators
	// If current token == ')' || '(' do bracket operations, we've opened or closed a vector value
	// If current token == keyword, check keyword, store token to be used later
	// If current token == valueType, check value and store to be used later
	// If current token == propertyname, store use later
	// If current token == propertyType, store use later, probably create the variable if we have a name and a value

	bool processedProperty = false;
	bool processedPropertyValue = false;
	
	// This flag is set when there are more than one property declarations on a line
	bool tooManyProperties = false;
	bool closeBlock = false;

	int index = (int) m_tokens.size();
	
	vector<MaterialToken*> tokenStack;
	tokenStack.reserve(16);
		
	MaterialProperty prop;
	
	while (index--)
	{
		MaterialToken& currentToken = m_tokens[index];

		if (currentToken.tokenType == Token::Keyword)
		{
			if (m_currentBlock == BlockType::Properties)
			{
				// Check for invalid keywords
				if (currentToken.keyword == Keyword::PS || currentToken.keyword == Keyword::VS ||
					currentToken.keyword == Keyword::Shaders || currentToken.keyword == Keyword::Properties)
				{
					PushError("Invalid keyword in the properties block of the material", currentToken);
				}
			}
			// If we're in the current block
			else if (m_currentBlock == BlockType::Shaders)
			{				
				// Check that we only have shader keywords inside the shader block			
				if ((currentToken.keyword != Keyword::PS && currentToken.keyword != Keyword::VS))
				{
					ThrowTokenError("Invalid keyword in the shaders block of the material", currentToken);					
				}
				// Ensure that we have a value that we can assign to the shader
				else if (tokenStack.empty())
				{
					ThrowTokenError("Shader keyword is missing a value", currentToken);					
				}
				// Check that the token is a compatible type, string in this case
				else if (!tokenStack.back()->IsString())
				{
					ThrowTokenError("Incorrect value being assigned to shader, expected a string", *tokenStack.back());
				}
				
				if (currentToken.keyword == Keyword::PS)
				{	
					m_pixelShader = tokenStack.back()->token;
				}
				else
				{
					m_vertexShader = tokenStack.back()->token;
				}

				tokenStack.pop_back();
			}
			// If we aren't in the shader block or properties block we check for the keywords
			else
			{
				if (currentToken.keyword == Keyword::Shaders)
				{					
					m_currentBlock = BlockType::Shaders;
				}
				// if we found the properties keyword
				else if (currentToken.keyword == Keyword::Properties)
				{					
					m_currentBlock = BlockType::Properties;
				}
				else
				{
					PushError("Keyword is invalid outside of a block", currentToken);
				}
			}
		}
		else if (currentToken.tokenType == Token::Operator)
		{
			if (currentToken.token[0] == '{')
			{
				++m_blockCount;
			}
			else if (currentToken.token[0] == '}')
			{
				--m_blockCount;

				// We only say we've closed a block if the block count is 0
				if (m_blockCount == 0)
				{
					closeBlock = true;
				}
			}
			// Push the '=' , ',' , '(' or ')' onto the token stack
			else if (currentToken.token[0] == '=' || currentToken.token[0] == ')' || 
				currentToken.token[0] == '(' || currentToken.token[0] == ',')
			{
				tokenStack.push_back(&currentToken);
			}
		}
		else if (currentToken.tokenType == Token::PropertyType)
		{
			// If we've already processed a property or our tokenStack is empty we leave
			// An empty tokenstack implies a property type is sitting on it's own
			if (tokenStack.empty() || processedProperty)
			{
				tooManyProperties = true;
				break;
			}
			// If the current token is a property type and the back of the stack is unknown, then we treat it as a token
			// Currently this allows you to use certain operators and symbols as a variable name, wouldn't recommend it
			else if (tokenStack.back()->IsUnknown())
			{
				prop.Initialise(tokenStack.back()->token, currentToken.propertyType);

				tokenStack.pop_back();

				processedProperty = true;

				if (!tokenStack.empty())
				{
					// If the back of the tokenstack is the = operator the rest of the tokens are the values to assign
					if (tokenStack.back()->IsOperator('='))
					{
						tokenStack.pop_back();

						// We don't have a value to look at
						if (tokenStack.empty())
						{
							PushError("Missing a value to assign to property", currentToken.line);
							break;
						}
					}
					else
					{
						PushError("Syntax error in property declaration");
						break;
					}

					// Grab the appropriate values from the stack
					if (prop.type == PropertyType::Float)
					{
						if (tokenStack.back()->IsNumber())
						{
							prop.floatValue = tokenStack.back()->GetFloat();
							tokenStack.pop_back();
						}
						else
						{
							PushError("Incorrect value being assigned to float property, expected a float or int", tokenStack.back()->line);
							break;
						}
					}
					else if (prop.type == PropertyType::Bool)
					{
						if (tokenStack.back()->IsBool())
						{
							prop.intValue = tokenStack.back()->boolValue;
							tokenStack.pop_back();
						}
						else
						{
							PushError("Incorrect value being assigned to bool property, expected true or false", tokenStack.back()->line);
							break;
						}
					}
					else if (prop.type == PropertyType::Int)
					{
						if (tokenStack.back()->value == ValueType::Int)
						{
							prop.intValue = tokenStack.back()->intValue;
							tokenStack.pop_back();
						}
						else
						{
							PushError("Incorrect value being assigned to int property, expected an integer", tokenStack.back()->line);
							break;
						}
					}
					else if (prop.type == PropertyType::Texture)
					{
						if (tokenStack.back()->value == ValueType::String)
						{
							prop.stringValue = tokenStack.back()->token;
							tokenStack.pop_back();
						}
						else
						{
							PushError("Incorrect value being assigned to Texture property, expected a string", tokenStack.back()->line);
							break;
						}
					}
					else if (prop.type == PropertyType::Vector3 || prop.type == PropertyType::Vector4 || prop.type == PropertyType::Colour)
					{
						float values[4];
						int extractedComponents = 0;

						bool processingVector = false;

						MaterialToken* current = nullptr;

						// while we have tokens to process
						// Since vector values are built from 3 to 4 float components, we have to process them differently
						while (!tokenStack.empty())
						{
							current = tokenStack.back();
							tokenStack.pop_back();

							// We check for the (, ) and , operators to tell us what we need to do with the values that we have
							if (current->tokenType == Token::Operator)
							{
								if (current->token[0] == '(')
								{
									if (processingVector)
									{
										break;
									}

									processingVector = true;
								}
								else if (current->token[0] == ')')
								{
									if (processingVector)
									{
										processingVector = false;
									}
									break;
								}
								else if (current->token[0] == ',' && processingVector)
								{
									++extractedComponents;

									// Not allowed more than 4 components
									if (extractedComponents > 3)
									{
										break;
									}
								}
								else
								{
									break;
								}
							}
							else if (current->tokenType == Token::Value)
							{
								// Only grab the value if it is a number, otherwise we can't do anything with it
								if (current->IsNumber())
								{
									values[extractedComponents] = current->GetFloat();
								}
								else
								{
									break;
								}
							}
							else
							{
								break;
							}
						}

						// If we have more than 4 components
						if (extractedComponents > 3)
						{
							ThrowTokenError("Too many components in vector/colour value", currentToken.line);
						}
						// if we have more than 3 and we're a vec3
						else if (extractedComponents > 2 && prop.type == PropertyType::Vector3)
						{
							ThrowTokenError("Too many components in vector3 value", currentToken.line);
						}
						// if we've extracted less than the minimum amount, 3
						else if (extractedComponents < 2)
						{
							ThrowTokenError("Too few components in vector/colour value", currentToken.line);
						}
						// if we left the loop whilst processing a vector then we have an issue
						else if (processingVector)
						{
							ThrowTokenError("Invalid value being assigned to vector/colour property, expecting a vector value", currentToken.line);
						}						

						prop.vectorValue.X = values[0];
						prop.vectorValue.Y = values[1];
						prop.vectorValue.Z = values[2];
						prop.vectorValue.W = values[3];
					}
				}
			}
			else
			{
				PushError("Syntax error in property declaration", currentToken);
			}
		}
		else
		{
			tokenStack.push_back(&currentToken);
		}
	}

	if (closeBlock)
	{
		// If we closed a block in this scan then we can reset the current block
		m_currentBlock = BlockType::Unknown;
	}

	// If the token stack isn't empty then we haven't processed all the tokens in it, implying an early break
	if (!tokenStack.empty())
	{
		// If we processed a property then we know that we have more than one property declaration on the line
		if (tooManyProperties)
		{
			PushError("Only one property is permitted per line", m_tokens[0].line);
		}
		// Otherwise we've got a syntax error
		else
		{
			PushError("Syntax error in material file", m_tokens[0].line);
		}
	}
	// Only push back a property if it is a valid property
	else if(processedProperty)
	{
		m_propertys.push_back(prop);
	}
	
    // All the tokens that we've just evaluated can be forgotten, as we've already got our data from them
	m_tokens.clear();
}

void MaterialParser::CreateToken(const std::string& token, bool isString)
{
	if (token.empty())
	{
		return;
	}

	MaterialToken newToken;

	if (isString)
	{
		newToken.token = token;
		newToken.tokenType = Token::Value;
		newToken.value = ValueType::String;		
	}
	else
	{				
		newToken = MakeToken(token);
	}

	newToken.line = m_currentLine;

	m_tokens.push_back(newToken);
}

void MaterialParser::CreateOperatorToken(char token)
{
	// We then push the operator back as it's own token
	MaterialToken newToken;
	
	newToken.tokenType = Token::Operator;	
	newToken.token = token;
	newToken.line = m_currentLine;

	m_tokens.push_back(newToken);
}

void MaterialParser::ParseFile(const std::string& filename)
{	
	m_fileStream.open(filename, ios::in | ios::binary);

	if (!m_fileStream.is_open())
	{
		cout << "Issue with opening the material file" << filename << endl;
		return;
	}

	string token;
	
	char currentCharacter = '\0';
	
	bool parsingString = false;

	bool foundOperator = false;
	bool parseTokens = false;
	
	m_currentBlock = BlockType::Unknown;
	m_blockCount = 0;
	m_currentLine = 1;

	while (m_fileStream.get(currentCharacter))
	{
		// Some string handling operators, strings are treated as a single token
		if (parsingString)
		{
			// If we reach the end of the string
			if (currentCharacter == '\"')
			{
				CreateToken(token, true);

				token.clear();
				parsingString = false;			
				continue;
			}
			// Strings aren't allowed to be multiline
			else if (currentCharacter == '\n')
			{
				PushError("Strings are not valid on multiple lines", m_currentLine);
			}
			else
			{
				token.push_back(currentCharacter);
				continue;
			}
		}
		else
		{
			foundOperator = binary_search(m_operators.begin(), m_operators.end(), currentCharacter);
		}

		if (foundOperator)
		{
			// Push back the current token that we have
			CreateToken(token);

			token.clear();			
			
			if (currentCharacter == '\"')
			{
				parsingString = true;
				continue;
			}

			CreateOperatorToken(currentCharacter);			

			// We only parse our tokens if we hit one of these characters
			if (currentCharacter == '{' || currentCharacter == '}' || currentCharacter == '\n')
			{				
				parseTokens = true;								

				if (currentCharacter == '\n')
				{
					++m_currentLine;
				}
			}
		}
		else if (IsWhiteSpace(currentCharacter) || currentCharacter == '\r')
		{			
			CreateToken(token);
				
			token.clear();			
		}
		else
		{
			token.push_back(currentCharacter);
		}

		if (!parseTokens)
		{
			continue;
		}

		parseTokens = false;
		
		// Process the tokens that we've collected		
		ProcessTokens();
	}

	if (m_blockCount > 0)
	{
		PushError("A block was not closed correctly", m_currentLine);
	}

	if (m_vertexShader.empty())
	{
		PushError("Missing a vertex shader file");
	}

	if (m_pixelShader.empty())
	{
		PushError("Missing a pixel shader file");
	}

	// If the error log is empty after the parse then we've were able to successfully parse the file
	if (m_errorLog.empty())
	{
		m_parseSuccessful = true;
	}
}

void MaterialParser::PrintOutStats()
{
	cout << "\n----------------------------------------\nShaders\n----------------------------------------\n";

	cout << "Vertex shader is: " << m_vertexShader << endl;
	cout << "Pixel shader is: " << m_pixelShader << endl;

	cout << "\n----------------------------------------\n";
	cout << "Properties";
	cout << "\n----------------------------------------\n";

	for (int i = 0; i < m_propertys.size(); ++i)
	{
		switch (m_propertys[i].type)
		{
			case PropertyType::Vector3:
				cout << "Vector3 " << m_propertys[i].name << " = (" << m_propertys[i].vectorValue.X << ", " << m_propertys[i].vectorValue.Y << ", " << m_propertys[i].vectorValue.Z << ")\n";
				break;
			case PropertyType::Vector4:
				cout << "Vector4 " << m_propertys[i].name << " = (" << m_propertys[i].vectorValue.X << ", " << m_propertys[i].vectorValue.Y << ", " << m_propertys[i].vectorValue.Z << ", " << m_propertys[i].vectorValue.W << ")\n";
				break;
			case PropertyType::Colour:
				cout << "Colour " << m_propertys[i].name << " = (" << m_propertys[i].vectorValue.X << ", " << m_propertys[i].vectorValue.Y << ", " << m_propertys[i].vectorValue.Z << ", " << m_propertys[i].vectorValue.W << ")\n";
				break;
			case PropertyType::Bool:
				cout << "Bool " << m_propertys[i].name << " = " << m_propertys[i].intValue << "\n";
				break;
			case PropertyType::Int:
				cout << "Int " << m_propertys[i].name << " = " << m_propertys[i].intValue << "\n";
				break;
			case PropertyType::Float:
				cout << "Float " << m_propertys[i].name << " = " << m_propertys[i].floatValue << "\n";
				break;
			case PropertyType::Texture:
				cout << "Texture " << m_propertys[i].name << " = " << m_propertys[i].stringValue << "\n";
				break;
			default:
				break;
		}
	}

	cout << "---------------------------------------\n";

	PrintErrorLog();
}

void MaterialParser::PrintErrorLog()
{
	if (m_errorLog.size() > 0)
	{
		cout << "\n------------------------------------------\n";
		cout << "Error log for material" << "\n------------------------------------------\n";

		for (int i = 0; i < m_errorLog.size(); ++i)
		{
			cout << m_errorLog[i].errorMessage << " \nLine: " << m_errorLog[i].lineNumber << endl;
		}
	}
}

MaterialParser::MaterialParser()
{
	m_propertyTypes.reserve(7);
	//std::vector<std::string> m_propertyTypes = { "Bool", "Colour", "Float", "Int", "Texture", "Vector3", "Vector4" };

	m_propertyTypes.push_back(make_pair("Bool", PropertyType::Bool));
	m_propertyTypes.push_back(make_pair("Colour", PropertyType::Colour));
	m_propertyTypes.push_back(make_pair("Float", PropertyType::Float));
	m_propertyTypes.push_back(make_pair("Int", PropertyType::Int));
	m_propertyTypes.push_back(make_pair("Texture", PropertyType::Texture));
	m_propertyTypes.push_back(make_pair("Vector3", PropertyType::Vector3));
	m_propertyTypes.push_back(make_pair("Vector4", PropertyType::Vector4));
	
	//std::vector<std::string> m_keyWords = { "false", "Properties", "PS", "Shaders", "true", "VS" };

	m_keywords.reserve(6);

	m_keywords.push_back(make_pair("false", Keyword::False));
	m_keywords.push_back(make_pair("Properties", Keyword::Properties));
	m_keywords.push_back(make_pair("PS", Keyword::PS));
	m_keywords.push_back(make_pair("Shaders", Keyword::Shaders));
	m_keywords.push_back(make_pair("true", Keyword::True));
	m_keywords.push_back(make_pair("VS", Keyword::VS));

	sort(m_propertyTypes.begin(), m_propertyTypes.end());
	sort(m_keywords.begin(), m_keywords.end());
	sort(m_operators.begin(), m_operators.end());
}