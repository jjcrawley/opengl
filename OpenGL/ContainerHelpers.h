#pragma once
#include <memory>
#include <type_traits>

template<typename DataType>
struct IsBitwiseConstructable
{
	static constexpr bool Value = ((std::is_trivially_constructible<DataType>::value || std::is_pod<DataType>::value) && std::is_trivially_destructible<DataType>::value);
};

// A variadic template that allows multiple arguments to be used to construct an object at a position in the array
template<typename Address, typename... TVal>
void Construct(Address* address, TVal&& ... val)
{
	::new ((void*) address) Address(std::forward<TVal>(val)...);
}

template<typename DataType>
typename std::enable_if<IsBitwiseConstructable<DataType>::Value>::type Relocate(DataType* fromData, DataType* toData, int count)
{
	memmove_s(toData, count * sizeof(DataType), fromData, count * sizeof(DataType));
}

template<typename DataType>
typename std::enable_if<!IsBitwiseConstructable<DataType>::Value>::type Relocate(DataType* fromData, DataType* toData, int count)
{
	// This will check if there is any overlap in the buffer regions
	// The only major overlap that needs to be considered is when toData is inside the fromData region. 
	// If it is, then to ensure correct movement of data you need to iterate from the back instead.
	if (toData > fromData && toData < (fromData + count - 1))
	{
		fromData += count - 1;
		toData += count - 1;

		while (count--)
		{
			Construct<DataType>(toData, *fromData);
			fromData->~DataType();
			--fromData;
			--toData;
		}
	}
	else
	{
		while (count--)
		{
			Construct<DataType>(toData, *fromData);
			fromData->~DataType();
			++fromData;
			++toData;
		}
	}
}

template<typename Type>
typename std::enable_if<std::is_trivially_destructible<Type>::value>::type DestroyRange(Type* start, size_t size)
{}

template<typename Type>
typename std::enable_if<!std::is_trivially_destructible<Type>::value>::type DestroyRange(Type* start, size_t size)
{
	while (size)
	{
		start->~Type();
		++start;
		--size;
	}
}

template<typename T, typename Iterator>
void ConstructRange(T* values, Iterator begin, int sizeList)
{
	while (sizeList--)
	{
		Construct<T>(values, *begin);
		++begin;
		++values;
	}
}