#include "Engine.h"
#include "GL/glew.h"

#pragma push_macro("TEXT")
#undef TEXT
#include <Windows.h>
#pragma pop_macro("TEXT")

Engine* Singleton<Engine>::s_pointer = nullptr;

void Engine::StartFrame()
{
	//m_currentTime = glfwGetTime();

	//m_deltaTime = (float)(m_currentTime - m_previousTime);

	unsigned long long current;

	QueryPerformanceCounter((LARGE_INTEGER*) &current);

	m_currentTime = (double) current;

	m_deltaTime = (float) ((m_currentTime - m_previousTime) / m_tickFrequency);		
}

void Engine::EndFrame()
{
	WindowManager::Get().SwapBuffers(); 
	PollEvents();

	m_previousTime = m_currentTime;
}

bool Engine::Initialise()
{	
	m_windowManager = WindowManager::GetPointer();
	
	m_inputManager = new InputManager();
	m_inputManager->Setup();
	m_resourceManager = new ResourceManager();	
	
	m_rendererManager = new RendererManager();
	m_rendererManager->Initialise();

	m_lightingEngine = new LightingEngine();
	m_lightingEngine->Initialise();

	unsigned long long frequency, startTime;

	if (!QueryPerformanceFrequency((LARGE_INTEGER*) &frequency))
	{
		m_tickFrequency = 1000.0f;
	}
	else
	{
		m_tickFrequency = (double) frequency;
	}

	QueryPerformanceCounter((LARGE_INTEGER*) &startTime);
	
	m_startTime = (double) startTime;
	m_currentTime = m_startTime;
	m_previousTime = m_currentTime;

	//m_startTime = glfwGetTime();
	//m_currentTime = m_startTime;
	//m_previousTime = m_currentTime;

	return true;
}

void Engine::Cleanup()
{
	if (m_inputManager)
	{		
		delete m_inputManager;
	}

	if (m_resourceManager)
	{
		m_resourceManager->ReleaseAll();
		delete m_resourceManager;
	}

	if (m_windowManager)
	{
		m_windowManager->Cleanup();
		delete m_windowManager;
	}

	if (m_rendererManager)
	{
		m_rendererManager->TidyUp();
		delete m_rendererManager;
	}

	if (m_lightingEngine)
	{
		m_lightingEngine->TidyUp();
		delete m_lightingEngine;
	}

	int size = m_updateEntities.Size();

	if (size)
	{
		for (int i = 0; i < size; ++i)
		{
			if (m_updateEntities[i])
			{
				m_updateEntities[i]->Destroy();
				delete m_updateEntities[i];
			}
		}

		m_updateEntities.Clear();
	}

	//glfwTerminate();
}

void Engine::UpdateEngine()
{
	m_inputManager->Update(m_deltaTime);

	int size = m_updateEntities.Size();

	for (int i = 0; i < size; ++i)
	{
		if (m_updateEntities[i]->IsActive())
		{
			m_updateEntities[i]->Update(m_deltaTime);
		}
	}
}

void Engine::Render()
{
	m_lightingEngine->UpdateLighting();
	m_rendererManager->Render();
}

void Engine::MainLoop()
{
	StartFrame();

	UpdateEngine();
	Render();

	EndFrame();
}

void Engine::Shutdown()
{
	b_shutdownEngine = true;
	//glfwSetWindowShouldClose((GLFWwindow*)WindowManager::Get().GetWindow(), GLFW_TRUE);
}

bool Engine::ShouldShutdown()
{
	return b_shutdownEngine;
}

void Engine::DeleteEntity(GameEntity* entity)
{
	if (!entity)
	{
		return;
	}

	int size = m_updateEntities.Size();

	for (int i = 0; i < size; ++i)
	{
		if (m_updateEntities[i] == entity)
		{
			m_updateEntities.RemoveAt(i, false);

			entity->Destroy();
			delete entity;
			break;
		}
	}
}