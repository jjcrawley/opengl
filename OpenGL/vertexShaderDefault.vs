#version 330 core

// this is the main vertex attribute, the position of the vertex in this case
layout(location = 0) in vec3 vertexPos;

layout(location = 1) in vec2 vertexUV;

layout(location = 2) in vec3 vertexNormal;

// The MVP that is currently being used to render the set of vertices

layout(std140) uniform Matrices
{
	mat4 MVP;
	mat4 ModelMatrix;
};

//out vec2 UV;
//out vec3 Position;
//out vec3 Normal;

void main()
{
	// gl_Position is the main output for the vertex shader, this is the position of the vertex	
	gl_Position = MVP * vec4(vertexPos, 1.0f);
		
	//UV = vertexUV;
	
	//Normal = (ModelMatrix * vec4(vertexNormal, 0.0f)).xyz;
	//Normal = normalize(Normal);

	//Position = (ModelMatrix * vec4(vertexPos, 1.0f)).xyz;
}