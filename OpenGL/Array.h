#pragma once

#include "Assertions.h"
#include "ContainerHelpers.h"
//#include "Algorithms.h"

struct Counter
{
	static size_t callsToAlloc;
	static size_t callsToDestroy;
};

template<typename Type>
class Allocator
{
	typedef Type* Pointer;

public:

	Pointer Allocate(size_t count)
	{
		Counter::callsToAlloc++;

		return static_cast<Pointer>(std::_Allocate(count, sizeof(Type)));
	}

	void Deallocate(Pointer pointer, size_t count)
	{
		Counter::callsToDestroy++;

		std::_Deallocate(pointer, count, sizeof(Type));
	}
};

template<typename T, typename TAlloc = Allocator<T>>
class Array
{
	typedef void(*ForElement)(T& ref);

	static const int extraSpace = 5;

private:
	TAlloc m_allocator;
	T* m_data;
	int m_size;
	int m_realSize;

public:
	Array() : m_realSize(extraSpace), m_size(0)
	{
		m_data = m_allocator.Allocate(m_realSize);
	}

	Array(std::initializer_list<T> list)
	{
		m_realSize = (int)list.size() + extraSpace;

		m_data = m_allocator.Allocate(m_realSize);

		ConstructFromRange(list.begin(), (int)list.size());
	}

	Array(const Array<T>& other) : m_realSize(0), m_size(0), m_data(nullptr)
	{
		Reserve(other.m_realSize);

		m_realSize = other.m_realSize;

		ConstructFromRange(other.m_data, other.Size());
	}

	Array(const T* data, int count)
	{
		m_realSize = count + extraSpace;

		m_data = m_allocator.Allocate(m_realSize);

		ConstructFromRange(data, count);
	}

	~Array()
	{
		Destroy();
	}

private:

	template<typename Iterator>
	void ConstructFromRange(Iterator begin, int sizeList)
	{		
		ConstructRange<T>(m_data, begin, sizeList);

		m_size = sizeList;
	}	
				
	void Destroy()
	{
		if (m_data != nullptr)
		{
			DestroyRange<T>(m_data, m_size);

			m_allocator.Deallocate(m_data, m_realSize);
		}
	}

public:

	Array<T>& operator=(const Array<T>& other)
	{
		if (&other != this)
		{
			if (other.IsEmpty())
			{
				Clear();
			}
			else if (m_size >= other.m_size)
			{
				if (m_data != nullptr)
				{
					DestroyRange<T>(m_data, m_size);
				}

				ConstructFromRange(other.GetData(), other.m_size);
			}
			else if (m_realSize >= other.m_size)
			{
				ConstructFromRange(other.GetData(), other.m_size);
			}
			else
			{
				Destroy();

				m_data = m_allocator.Allocate(other.m_size);

				ConstructFromRange(other.GetData(), other.m_size);

				m_realSize = m_size;
			}
		}

		return *this;
	}

	Array<T>& operator=(std::initializer_list<T> list)
	{		
		Clear();

		if (list.size() > m_realSize)
		{
			m_allocator.Deallocate(m_data, m_realSize);

			m_realSize = (int)list.size() + extraSpace;

			m_data = m_allocator.Allocate(m_realSize);
		}

		ConstructFromRange(list.begin(), (int)list.size());

		return *this;
	}

	void Add(const T& val)
	{
		++m_size;

		if (m_size > m_realSize)
		{
			const int oldSize = m_realSize;

			m_realSize += extraSpace;

			T* oldData = m_data;

			m_data = m_allocator.Allocate(m_realSize);

			Relocate<T>(oldData, m_data, (m_size - 1));
						
			m_allocator.Deallocate(oldData, oldSize);
		}

		Construct<T>(&m_data[m_size - 1], val);
	}

	void AddRange(std::initializer_list<T> list)
	{
		const int size = (int) list.size();

		m_size += size;

		if (m_size > m_realSize)
		{
			const int oldSize = m_realSize;

			m_realSize = m_size + extraSpace;

			T* oldData = m_data;

			m_data = m_allocator.Allocate(m_realSize);

			Relocate<T>(oldData, m_data, (m_size - size));
						
			m_allocator.Deallocate(oldData, oldSize);
		}

		ConstructRange<T>(&m_data[m_size - size], list.begin(), size);
	}	

	void InsertAt(int index, const T& val)
	{
		Assert(InRange(index), TEXT("The index to insert at is out of range"));
				
		if (index == m_size)
		{
			Add(val);
		}
		else
		{
			++m_size;
						
			T* oldData = m_data;

			if (m_size > m_realSize)
			{
				m_realSize += extraSpace;

				m_data = m_allocator.Allocate(m_realSize);
								
				Relocate<T>(oldData, m_data, index);
				Relocate<T>(oldData + index, m_data + index + 1, m_size - index - 1);

				m_allocator.Deallocate(oldData, m_size - 1);
			}			
			else
			{
				Relocate<T>(oldData + index, m_data + index + 1, m_size - index - 1);
			}

			Construct<T>(m_data + index, val);
		}		
	}

	void RemoveAt(int index, bool retainOrder = true)
	{
		Assert(InRange(index), TEXT("Can't remove from the array as the index is out of range"));

		if (!retainOrder)
		{
			RemoveAtShuffle(index);
		}
		else
		{
			RemoveRetain(index);
		}
	}

	void PopBack()
	{
		if (IfCheck(IsEmpty(), TEXT("Trying to remove from the back of an empty array")))
		{
			T* toDelete = &m_data[m_size - 1];

			DestroyRange<T>(toDelete, 1);
			
			--m_size;
		}
	}

	void Clear()
	{
		if (m_data != nullptr)
		{
			DestroyRange<T>(m_data, m_size);

			m_size = 0;
		}
	}

	void Reserve(int count)
	{
		Assert(count >= 0, TEXT("Count for reservation can't be less than 0"))

		if (count > m_realSize && count >= 0)
		{
			T* pointer = m_allocator.Allocate(count);

			if (m_size > 0)
			{
				Relocate<T>(m_data, pointer, m_size);				
			}

			if (m_data != nullptr)
			{
				m_allocator.Deallocate(m_data, m_realSize);
			}

			m_realSize = count;
			
			m_data = pointer;
		}
	}

	void Trim()
	{
		if (m_realSize > m_size)
		{
			T* pointer = m_allocator.Allocate(m_size);

			Relocate<T>(m_data, pointer, m_size);
								
			m_allocator.Deallocate(m_data, m_realSize);

			m_data = pointer;

			m_realSize = m_size;
		}
	}

	/*template<typename U>
	int Contains(const U& item)
	{
		return Search::LinearSearch(m_data, m_size, value);
	}

	template<typename U, typename Predicate>
	int Contains(const U& item, const Predicate& pred)
	{
		return Search::LinearSearch(m_data, m_size, value, pred);
	}

	template<bool dereference, typename U, typename Predicate>
	int Contains(const U& item, const Predicate& pred)
	{
		return Search::LinearSearch<T, U, dereference>(m_data, m_size, value, pred);
	}
	
	template<typename U>
	int ContainsSorted(const U& toFind)
	{
		return Search::BinarySearch<T, U>(m_data, m_size, toFind, LessThan<T>());
	}

	template<typename U, typename Predicate>
	int ContainsSorted(const U& toFind, const Predicate& pred)
	{
		return Search::BinarySearch<T, U>(m_data, m_size, toFind, pred);
	}

	template<bool dereference, typename U, typename Predicate>
	int ContainsSorted(const U& toFind, const Predicate& pred)
	{
		return Search::BinarySearch<T, U, dereference>(m_data, m_size, toFind, pred);
	}*/

	void ForEach(const ForElement& func)
	{
		for (int i = 0; i < m_size; ++i)
		{
			func(m_data[i]);
		}
	}

	inline bool InRange(int index) const
	{
		return index >= 0 && index < m_size;
	}

	T& operator[](int index)
	{
		Assert(InRange(index), TEXT("Can't retrieve index as the index is out of range"))
			
		return m_data[index];
	}

	const T& operator[](int index) const
	{
		Assert(InRange(index), TEXT("Can't retrieve index as the index is out of range"))

		return m_data[index];
	}

	inline const T* GetData() const
	{
		return m_data;
	}

	inline T* GetData()
	{
		return m_data;
	}

	inline bool IsEmpty() const
	{
		return m_size == 0;
	}

	inline int Capacity() const
	{
		return m_realSize;
	}

	inline int Size() const
	{
		return m_size;
	}

	inline T& Back() const
	{
		return m_data[m_size - 1];
	}

	inline T& Back()
	{
		return m_data[m_size - 1];
	}

private:

	void RemoveRetain(int index)
	{		
		T* toDelete = &m_data[index];
				
		if (index != m_size - 1)
		{			
			Relocate<T>(m_data + index + 1, toDelete, m_size - index);
		}
		else
		{
			DestroyRange<T>(toDelete, 1);
		}

		--m_size;		
	}

	void RemoveAtShuffle(int index)
	{
		T* value = &m_data[index];	
				
		if (index != m_size - 1)
		{
			Relocate<T>(m_data + m_size - 1, value, 1);
		}
		else
		{
			DestroyRange<T>(value, 1);
		}

		--m_size;		
	}
};