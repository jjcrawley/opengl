#pragma once

#include "Array.h"

template<typename HandleType, typename Data>
class HandleCollection;

template<typename T>
class Handle
{
	template<typename HandleType, typename Data>
	friend class HandleCollection;

	union
	{
		struct
		{
			unsigned short m_index;
			unsigned short m_handleID;
		};

		unsigned int m_ID;
	};	

	inline void Invalidate()
	{
		m_id = 0;
	}

public:
	Handle() : m_ID(0)
	{}

	Handle(unsigned short index)
	{
		Initialise(index);
	}

	inline unsigned int GetID() const
	{
		return m_ID;
	}

	inline bool IsNull()
	{
		return m_ID == 0;
	}

	inline operator unsigned int()
	{
		return m_ID;
	}

	inline unsigned short HandleID() const
	{
		return m_handleID;
	}

	inline unsigned short GetIndex() const
	{
		return m_index;
	}

	inline void Initialise(unsigned short index)
	{
		if (m_handleID != 0)
		{
			return;
		}

		static unsigned short s_handleCounter = 1;

		if (s_handleCounter == 0)
		{
			s_handleCounter = 1;
		}

		m_handleID = s_handleCounter++;
		m_index = index;
	}

	friend bool operator==(const Handle<T>& left, const Handle<T>& right)
	{
		return left.m_ID == right.m_ID;
	}

	friend bool operator!=(const Handle<T>& left, const Handle<T>& right)
	{
		return !(left == right);
	}
};

template<typename HandleType, typename Data>
class HandleCollection
{
	struct HandleData
	{
		HandleType DataHandle;

		~HandleData()
		{
			DataHandle.Invalidate();
		}
	};

	Array<Data> m_data;
	Array<HandleData> m_handleData;
	Array<int> m_freeList;

	int m_activeHandleCount = 0;

	inline bool ValidHandle(HandleType handle)
	{
		const unsigned short index = handle.GetIndex();

		return m_handleData.InRange(index) && m_handleData[index].DataHandle == handle;
	}

public:
	HandleCollection()
	{}

	HandleType GetHandle(const Data& object)
	{		
		HandleType handle;

		if (m_freeList.IsEmpty())
		{
			HandleData data;
			handle.Initialise((unsigned short)m_data.Size() - 1);

			data.DataHandle = handle;

			m_handleData.Add(data);
		}
		else
		{
			unsigned short index = m_freeList[m_freeList.Size() - 1];
			m_freeList.PopBack();

			m_data[index] = object;
			handle.Initialise(index);

			m_handleData[index].DataHandle = handle;
		}

		++m_activeHandleCount;

		return handle;
	}

	void ReleaseHandle(HandleType handle)
	{		
		if (ValidHandle(handle))
		{
			unsigned short index = handle.GetIndex();

			m_handleData[index].DataHandle.Invalidate();
			m_data[index] = nullptr;

			m_freeList.Add(index);

			--m_activeHandleCount;
		}
	}

	inline Data& GetData(HandleType handle)
	{
		if (ValidHandle(handle))
		{
			return m_data[handle.GetIndex()];
		}
	}

	inline Data& GetData(HandleType handle) const
	{
		if (ValidHandle(handle))
		{
			return m_data[handle.GetIndex()];
		}
	}	

	inline int ActiveHandleCount()
	{
		return m_activeHandleCount;
	}	
};