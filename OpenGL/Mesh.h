#pragma once
#include "Mathfwd.h"
#include "Array.h"
#include "Vector3.h"
#include "Vector2.h"
#include "BaseResource.h"

class Mesh : public BaseResource
{	
	Array<Vector3> m_vertices;
	Array<Vector2> m_uvs;
	Array<Vector3> m_normals;

	Array<unsigned int> m_indices;

public:
	Mesh() : BaseResource() {};	

	inline const Array<Vector3>& Vertices() const
	{
		return m_vertices;
	}

	inline void SetVertices(const Array<Vector3>& vertices)
	{
		m_vertices = vertices;
	}

	inline const Array<Vector2>& UVs() const
	{
		return m_uvs;
	}

	inline void SetUVs(const Array<Vector2>& uvs)
	{
		m_uvs = uvs;
	}

	inline void SetIndices(const Array<unsigned int>& indices)
	{
		m_indices = indices;
	}

	inline const Array<unsigned int>& Indices() const
	{
		return m_indices;
	}

	inline void SetNormals(const Array<Vector3>& normals)
	{
		m_normals = normals;
	}

	inline const Array<Vector3>& Normals() const
	{
		return m_normals;
	}

	virtual void Release() override
	{
		m_vertices.Clear();
		m_uvs.Clear();
		m_normals.Clear();
		m_indices.Clear();
	}
};