#include "GameEntity.h"
#include "Component.h"

void GameEntity::RemoveComponent(int index)
{	
	if (IfCheck(m_components.InRange(index), TEXT("Unable to remove component as the index is out of range")))
	{
		Component* component = m_components[index];

		m_components.RemoveAt(index, false);

		component->Destroy();
		delete component;
	}
}

void GameEntity::RemoveComponent(const std::string& name)
{
	int size = m_components.Size();

	Component* component = nullptr;

	for (int i = 0; i < size; ++i)
	{
		if (m_components[i]->SameName(name))
		{
			component = m_components[i];
			m_components.RemoveAt(i, false);
			break;
		}
	}

	if (component)
	{
		component->Destroy();
		delete component;
	}
}