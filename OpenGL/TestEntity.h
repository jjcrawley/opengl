#pragma once
#include "GameEntity.h"
#include "Delegate.h"
#include "MyMath.h"

class Light;
class Camera;

class TestEntity : public GameEntity
{
	typedef GameEntity Super;

	Light* m_light;
	Camera* m_camera;

	DelegateHandle m_mouseHandle;
	DelegateHandle m_scrollHandle;

	float m_horizontalAngle = 0.0f;
	float m_verticalAngle = 0.0f;
	float m_moveSpeed = 3.0f;
	float m_turnSpeed = HALF_PI / 4.0f;

	float m_mouseX, m_mouseY;
	float m_xMovement, m_yMovement, m_zMovement;

	float m_lightBackwardAndForward;

	Vector3 m_forward;
	Vector3 m_right;
	Vector3 m_up;

public:
	
	virtual void OnCreate() override;
	virtual void Update(float deltaTime) override;

private:

	void OnMouseMove(float x, float y);
	void ToggleCameraControl();
	void ShutdownEngine();
	
	void ForwardDirection(float value);
	void LeftAndRightMovement(float value);
	void UpAndDownMovement(float value);
	void LightBackwardAndForward(float value);
	
	void MakeSpotLight();
	void MakeDirectionalLight();
	void MakePointLight();
	void ToggleLight();
};