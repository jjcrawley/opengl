#pragma once
#include "Singleton.h"

struct GLFWwindow;

void PollEvents();

class WindowManager : public Singleton<WindowManager>
{
	GLFWwindow* m_window;
public:
	WindowManager() : m_window(nullptr)
	{}
	
	static bool InitialiseWindow();
	void Cleanup();
	
	void SwapBuffers();

	int GetKey(int keyCode);
	void SetMouseHidden(bool hidden);
	void GetCursorPosition(double &x, double& y);
};