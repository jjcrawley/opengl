#include "Quaternion.h"
#include "MyMath.h"
#include "Debugger.h"
#include <math.h>

const Quaternion Quaternion::Identity(1, 0, 0, 0);

Quaternion::Quaternion(const Matrix& M)
{
	float trace = M.M[0][0] + M.M[1][1] + M.M[2][2];

	if (trace > 0.0f)
	{
		W = sqrt(1.0f + trace) * 0.5f;

		float invFourW = 1.0f / (4.0f * W);

		X = (M.M[1][2] - M.M[2][1]) * invFourW;
		Y = (M.M[2][0] - M.M[0][2]) * invFourW;
		Z = (M.M[0][1] - M.M[1][0]) * invFourW;
	}
	else if (M.M[0][0] > M.M[1][1] && M.M[0][0] > M.M[2][2])
	{
		X = sqrt(M.M[0][0] - M.M[1][1] - M.M[2][2] + 1.0f) * 0.5f;

		float invFourX = 1.0f / (4.0f * X);

		W = (M.M[1][2] - M.M[2][1]) * invFourX;
		Y = (M.M[0][1] + M.M[1][0]) * invFourX;
		Z = (M.M[2][0] + M.M[0][2]) * invFourX;	
	}
	else if (M.M[1][1] > M.M[2][2])
	{
		Y = sqrt(-M.M[0][0] + M.M[1][1] - M.M[2][2] + 1.0f) * 0.5f;

		float invFourY = 1.0f / (4.0f * Y);

		W = (M.M[2][0] - M.M[0][2]) * invFourY;
		X = (M.M[0][1] + M.M[1][0]) * invFourY;
		Z = (M.M[1][2] + M.M[2][1]) * invFourY;
	}
	else
	{
		Z = sqrt(-M.M[0][0] - M.M[1][1] + M.M[2][2] + 1.0f) * 0.5f;

		float invFourZ = 1.0f / (4.0f * Z);

		W = (M.M[0][1] - M.M[1][0]) * invFourZ;
		X = (M.M[2][0] + M.M[0][2]) * invFourZ;
		Y = (M.M[1][2] + M.M[2][1]) * invFourZ;
	}
}

Quaternion::Quaternion(float angle, const Vector3& axis)
{
	W = cos(angle / 2.0f);

	float sinAngle = sin(angle / 2.0f);

	Vector3 nAxis = axis;

	if (!axis.IsNormalised())
	{
		nAxis = axis.Normalise();
	}
	
	X = nAxis.X * sinAngle;
	Y = nAxis.Y * sinAngle;
	Z = nAxis.Z * sinAngle;
}

Quaternion::Quaternion(const ERotation& eulerAngles)
{
	float cp = cos(eulerAngles.Pitch / 2.0f);
	float sp = sin(eulerAngles.Pitch / 2.0f);

	float ch = cos(eulerAngles.Heading / 2.0f);
	float sh = sin(eulerAngles.Heading / 2.0f);

	float cr = cos(eulerAngles.Roll / 2.0f);
	float sr = sin(eulerAngles.Roll / 2.0f);

	W = ch * cp * cr + sh * sp * sr;
	X = ch * sp * cr + sh * cp * sr;
	Y = sh * cp * cr - ch * sp * sr;
	Z = ch * cp * sr - sh * sp * cr;
}

float Quaternion::Magnitude() const
{	
	return sqrt(SquareMagnitude());
}

float Quaternion::SquareMagnitude() const
{
	return X * X + Y * Y + Z * Z + W * W;
}

float Quaternion::Dot(const Quaternion& Q) const
{
	return W * Q.W + X * Q.X + Y * Q.Y + Z * Q.Z;
}

Quaternion Quaternion::Exp() const
{
	Quaternion result = Identity;

	float angle = X * X + Y * Y + Z * Z;

	if (angle != 0.0f)
	{
		angle = sqrt(angle);

		float sinRatio = sin(angle) / angle;

		result.W = cos(angle);
		result.X = X * sinRatio;
		result.Y = Y * sinRatio;
		result.Z = Z * sinRatio;
	}

	return result;
}

Quaternion Quaternion::Exp(float power) const
{
	Quaternion result = Identity;
	
	if (abs(W) < 1.0f)
	{
		float angle = acos(W);

		float newAngle = angle * power;

		float sinRatio = sin(newAngle) / sin(angle);

		result.W = cos(newAngle);
		result.X = X * sinRatio;
		result.Y = Y * sinRatio;
		result.Z = Z * sinRatio;
	}

	return result;
}

Quaternion Quaternion::Log() const
{
	float angle = acos(W);

	float sinRatio = angle / sin(angle);
	
	return Quaternion(
		0.0f,
		X * sinRatio,
		Y * sinRatio,
		Z * sinRatio
		);
}

Quaternion Quaternion::Inverse() const
{
	return Quaternion(W, -X, -Y, -Z);
}

Quaternion Quaternion::Normalise() const
{
	Quaternion result;

	float magnitude = SquareMagnitude();

	if (magnitude <= 1e-8f)
	{
		result = Identity;
	}
	else if (abs(1.0f - magnitude) < 1e-8f)
	{
		result = *this;
	}
	else
	{
		magnitude = 1.0f / sqrt(magnitude);

		result.X = X * magnitude;
		result.Y = Y * magnitude;
		result.Z = Z * magnitude;
		result.W = W * magnitude;
	}

	return result;
}

Vector3 Quaternion::Forward() const
{
	return Vector3(
		2.0f * (W * Y + X * Z),
		2.0f * (Y * Z - W * X),
		1.0f - 2.0f * (X * X + Y * Y)
		);
}

Vector3 Quaternion::Right() const
{
	return Vector3(
		1.0f - 2.0f * (Y * Y + Z * Z),
		2.0f * (X * Y + W * Z),
		2.0f * (X * Z - W * Y)		
		);
}

Vector3 Quaternion::Up() const
{
	return Vector3(
		2.0f * (X * Y - W * Z),
		1.0f - 2.0f * (X * X + Z * Z),
		2.0f * (Y * Z + W * X)
		);
}

inline bool Quaternion::IsNormalised() const
{
	return SquareMagnitude() >= 0.9999f;
}

Quaternion Quaternion::operator*(const Quaternion& Q) const
{
	return Quaternion(
		W * Q.W - X * Q.X - Y * Q.Y - Z * Q.Z,
		W * Q.X + X * Q.W + Y * Q.Z - Z * Q.Y,
		W * Q.Y + Y * Q.W + Z * Q.X - X * Q.Z,
		W * Q.Z + Z * Q.W + X * Q.Y - Y * Q.X		
		);
}

Quaternion& Quaternion::operator*=(const Quaternion& Q)
{
	W = W * Q.W - X * Q.X - Y * Q.Y - Z * Q.Z;
	X = W * Q.X + X * Q.W + Y * Q.Z - Z * Q.Y;
	Y = W * Q.Y + Y * Q.W + Z * Q.X - X * Q.Z;
	Z = W * Q.Z + Z * Q.W + X * Q.Y - Y * Q.X;

	return *this;
}

Quaternion Quaternion::operator*(const float scale) const
{
	return Quaternion(
		X * scale,
		Y * scale,
		Z * scale,
		W * scale);
}

Vector3 Quaternion::operator*(const Vector3& V) const
{
	return RotateVector(V);
}

Vector4 Quaternion::operator*(const Vector4& v) const
{
	return Vector4(RotateVector(v), v.W);
}

Quaternion Quaternion::operator-(const Quaternion& Q) const
{
	return Q * Inverse();
}

Quaternion Quaternion::operator-() const
{
	return Quaternion(-X, -Y, -Z, -W);
}

Vector3 Quaternion::RotateVector(const Vector3& V) const
{
	Vector3 result;

	Vector3 u(X, Y, Z);
	
	result = u * 2.0f * u.Dot(V) + V * (W * W - u.Dot(u)) + u.Cross(V) * 2.0f * W;

	return result;
}

Vector3 Quaternion::UnRotateVector(const Vector3& v) const
{
	Vector3 result;
	Vector3 uInverse(-X, -Y, -Z);

	result = uInverse * 2.0f * uInverse.Dot(v) + v * (W * W - uInverse.Dot(uInverse)) + uInverse.Cross(v) * 2.0f * W;

	return result;
}

ERotation Quaternion::EulerAngles() const
{
	ERotation result;

	float testThreshold = Y * Z - W * X;

	// This will potentially address any issues with gimbal lock that can occur when converting to euler rotations
	if (abs(testThreshold) > 0.49995f)
	{
		result.Pitch = testThreshold < 0.0f ? -HALF_PI : HALF_PI;
		result.Heading = atan2(-X * Z + W * Y, 0.5f - Y * Y - Z * Z);
		result.Roll = 0;
	}
	else
	{
		result.Pitch = asin(-2.0f * testThreshold);
		result.Heading = atan2(X * Z + W * Y, 0.5f - X * X - Y * Y);
		result.Roll = atan2(X * Y + W * Z, 0.5f - X * X - Z * Z);
	}

	return result;
}

Quaternion Quaternion::Slerp(const Quaternion& from, const Quaternion& to, float alpha)
{
	Quaternion result;
	
	Quaternion flipTo = to;

	float cosOmega = from.Dot(to);
		
	float scale0, scale1;

	if (cosOmega < 0.0f)
	{
		flipTo = -to;
		cosOmega = -cosOmega;		
	}

	if (cosOmega > 0.9999f)
	{
		scale0 = 1.0f - alpha;
		scale1 = alpha;
	}
	else
	{
		float angle = acos(cosOmega);

		float sinOmega = sin(angle);

		scale0 = sin((1.0f - alpha) * angle) / sinOmega;

		scale1 = sin(angle * alpha) / sinOmega;
	}

	result.W = scale0 * from.W + scale1 * flipTo.W;
	result.X = scale0 * from.X + scale1 * flipTo.X;
	result.Y = scale0 * from.Y + scale1 * flipTo.Y;
	result.Z = scale0 * from.Z + scale1 * flipTo.Z;

	return result;
}

void Quaternion::ToAxisAngle(const Quaternion& Q, float& angle, Vector3& axis)
{
	angle = acos(Q.W);

	float sinAngle = 1.0f / sin(angle);

	axis.X = Q.X * sinAngle;
	axis.Y = Q.Y * sinAngle;
	axis.Z = Q.Z * sinAngle;
}

std::ostream & operator<<(std::ostream& stream, const Quaternion& Q)
{
	return stream << "[" << Q.W << ", " << Q.X << ", " << Q.Y << ", " << Q.Z << "]";
}