#pragma once
#include <math.h>

#include "Mathfwd.h"
#include "Assertions.h"

#define PI		3.14159265359f
#define INV_PI	0.31830988618f
#define HALF_PI	1.57079632679f
#define TWO_PI  (PI * 2.0f)

class FMath
{
public:
	static bool InFOV(float FOV, const Vector3& Pos, const Vector3& Direction, const Vector3& X);
	static void PolarToCartesian2D(float Angle, float Radius, Vector2& Result);
	static void CartesianToPolar2D(const Vector2& Coord, float& Angle, float& Radius);
	static void CylindricalToCartesian(float Angle, float Radius, float Height, Vector3& Result);
	static void CartesianToCylindrical(const Vector3& Coord, float& Radius, float& Angle, float& Height);
	static void SphericalToCartesian(float Heading, float Pitch, float Radius, Vector3& Result);
	static Vector3 SphericalToCartesianUnit(float Heading, float Pitch);
	static void CartesianToSpherical(const Vector3& Coord, float& Heading, float& Pitch, float& Radius);
	static void CartesianToSphericalUnit(const Vector3& direction, float& heading, float& pitch);
	static float WrapAngleRadians(float Rad);

	static float Log(float value)
	{
		return logf(value);
	}
	
	template<typename T>
	static __forceinline auto DegToRad(const T& degrees) -> decltype(degrees * PI / 180)
	{
		return degrees * PI / 180;
	}

	template<typename T>
	static __forceinline auto RadToDeg(const T& radians) -> decltype(radians * 180 / PI)
	{
		return radians * 180 / PI;
	}

	template<typename T>
	static inline T Min(T first, T second)
	{
		return first < second ? first : second;
	}

	template<typename T>
	static inline T Max(T first, T second)
	{
		return first < second ? second : first;
	}

	template<typename T, typename U>
	static T Lerp(T from, T to, U percent)
	{
		return from + (to - from) * percent;
	}

	template<typename T>
	static Quaternion Slerp(const Quaternion& from, const Quaternion& to, T alpha);

	template<typename T>
	static const T& Clamp(const T& toClamp, const T& max, const T& min)
	{
		return toClamp > max ? max : toClamp < min ? min : toClamp;
	}
};