#pragma once

// The dereferencer is responsible for dereferencing pointer types, if we want to
// The default template will spit out the original value
template<typename T, bool dereference = false>
struct Dereferencer
{
	inline T& operator()(T& t)
	{
		return t;
	}

	inline const T& operator()(const T& t) const
	{
		return t;
	}
};

// A specialisation of the Dereferencer that will dereference pointer types
template<typename T>
struct Dereferencer<T*, true>
{
	inline T& operator()(T* t)
	{
		return *t;
	}

	inline const T& operator()(const T* t) const
	{
		return *t;
	}
};