#pragma once

#include "Mathfwd.h"
#include <iostream>

std::ostream& operator<<(std::ostream& out, const Matrix& M);
std::ostream& operator<<(std::ostream& out, const ERotation& R);
std::ostream& operator<<(std::ostream& out, const Vector2& V);
std::ostream& operator<<(std::ostream& out, const Vector3& V);
std::ostream& operator<<(std::ostream& stream, const Quaternion& Q);
std::ostream& operator<<(std::ostream& stream, const Vector4& v);
std::ostream& operator<<(std::ostream& stream, const Transform& transform);