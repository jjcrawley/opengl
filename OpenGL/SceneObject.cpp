#include "SceneObject.h"

SceneObject* SceneObject::RemoveChildInternal(int index)
{
	SceneObject* object = m_children[index];

	m_children.RemoveAt(index);

	object->m_parent = nullptr;

	return object;
}

void SceneObject::UpdateToWorld()
{	
	Transform finalTransform(m_relativePosition, m_relativeRotation, m_relativeScale);

	if (m_parent != nullptr)
	{
		m_parent->ConditionalUpdateToWorld();

		Transform parentToWorld = m_parent->GetWorldTransform();
			
		finalTransform *= parentToWorld;
	}

	if (m_toWorldTransform != finalTransform)
	{
		m_toWorldTransform = finalTransform;
		b_updateWorldTransform = false;

		UpdateChildrenTransforms();
	}
	else
	{
		b_updateWorldTransform = false;
	}	
}

void SceneObject::UpdateChildrenTransforms()
{
	int size = m_children.Size();

	for (int i = 0; i < size; ++i)
	{
		SceneObject* current = m_children[i];

		current->UpdateToWorld();
	}
}

void SceneObject::ConditionalUpdateToWorld()
{
	if (b_updateWorldTransform)
	{
		UpdateToWorld();
	}
}

void SceneObject::SetWorldPosition(const Vector3& position)
{	
	Vector3 newPosition = position;

	if (m_parent)
	{
		m_parent->ConditionalUpdateToWorld();

		newPosition = m_parent->m_toWorldTransform.InverseTransformPosition(position);
	}

	SetRelativePosition(newPosition);
}

void SceneObject::SetWorldRotation(const Quaternion& rotation)
{
	if (!m_toWorldTransform.SameRotation(rotation))
	{	
		Quaternion rot = rotation;

		if (m_parent)
		{
			m_parent->ConditionalUpdateToWorld();

			rot = m_parent->m_toWorldTransform.UnRotateQuat(rot);
		}
						
		SetRelativeRotation(rot);
	}
}

void SceneObject::AddChild(SceneObject* object)
{
	if (IfCheck(object, TEXT("Unable to add a null child to the scene object")))
	{
		if (object->m_parent == this || object == this)
		{
			return;
		}

		m_children.Add(object);

		if (object->m_parent)
		{
			object->m_parent->RemoveChild(object);
		}

		object->m_parent = this;
		object->UpdateToWorld();
	}
}

void SceneObject::RemoveChild(SceneObject* object)
{
	if (IfCheck(object, TEXT("Can't remove a null child from the scene object")))
	{
		int size = m_children.Size();

		for (int i = 0; i < size; ++i)
		{
			if (m_children[i] == object)
			{
				RemoveChildInternal(i);
				
				break;
			}
		}

		object->UpdateToWorld();
	}
}

void SceneObject::RemoveChild(int index)
{
	if (IfCheck(m_children.InRange(index), TEXT("Unable to remove child as index is out of range")))
	{
		SceneObject* oldObject = RemoveChildInternal(index);
		oldObject->UpdateToWorld();
	}
}