#pragma once

#include "Macros.h"
#include "StringHelpers.h"
#include <string>

class ShaderHelpers
{
public:
	ShaderHelpers();
	~ShaderHelpers();
	static unsigned int LoadShader(const TCHAR* vertexFilePath, const TCHAR* fragmentPath);
	static bool CompileShader(unsigned int shader, const std::string& shaderSource);
};