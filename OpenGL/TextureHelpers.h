#pragma once

#include <gl/glew.h>

#include "Mathfwd.h"
#include "StringHelpers.h"

class TextureHelpers
{
public:
	static GLuint LoadBMP(const TCHAR* imagePath);
	static GLuint LoadTGA(const TCHAR* imagePath);
	static GLuint LoadDDS(const TCHAR* imagePath, bool flip = false);
	static GLuint CreateSinglePixelColouredTexture(Vector3 colour);
};