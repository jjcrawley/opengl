#pragma once
#include "Singleton.h"
#include "GL/glew.h"
#include "Array.h"
#include "MyMath.h"

class RenderableMesh;
class Material;
class Camera;

class RendererManager : public Singleton<RendererManager>
{
	static const GLuint s_matrixBufferIndex;
	static const GLuint s_cameraBufferIndex;

	GLuint m_matrixBuffer;
	GLuint m_cameraBuffer;
		
	Array<RenderableMesh*> m_renderers;
	Array<Camera*> m_activeCameras;
	Camera* m_mainCamera;

public:
	RendererManager();
	~RendererManager();

	void Initialise();
	void RegisterRenderer(RenderableMesh* renderer);
	void DeregisterRenderer(RenderableMesh* renderer);
	void SetupMaterial(Material* material);
	void SetMainCamera(Camera* camera);
	void UpdateMatrixBuffer(const Matrix& MVP, const Matrix& world);
	void Render();
	void TidyUp();
	Camera* CurrentCamera();
};