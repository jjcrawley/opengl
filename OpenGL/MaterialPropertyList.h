#pragma once
#include <string>
#include "BaseResource.h"
#include "Texture.h"
#include "MyMath.h"

typedef unsigned int MatVariable;

class Material;

// The int type is used for boolean values and regular integer values
// The Vector type is used for both vector4 and colour types
enum class PropertyType
{
	Float,
	Int,
	Texture,
	Vector,
	Vector3,
	Unknown
};

class MaterialPropertyList : public BaseResource
{
	friend Material;

	struct BaseMaterialProperty
	{
		MatVariable PropHandle;
		std::string Name;
		unsigned int Location;
		PropertyType Type;

		BaseMaterialProperty() : Type(PropertyType::Unknown), Location(0), Name(""), PropHandle(0)
		{}

		BaseMaterialProperty(PropertyType type) : Type(type), Location(0), Name(""), PropHandle(0)
		{}
	};

	struct IntProperty : public BaseMaterialProperty
	{
		int Value;

		IntProperty() : BaseMaterialProperty(PropertyType::Int)
		{}
	};

	struct FloatProperty : public BaseMaterialProperty
	{
		float Value;

		FloatProperty() : BaseMaterialProperty(PropertyType::Float)
		{}
	};

	struct VectorProperty : public BaseMaterialProperty
	{
		Vector4 Value;

		VectorProperty() : BaseMaterialProperty(PropertyType::Vector)
		{}
	};

	struct Vector3Property : public BaseMaterialProperty
	{
		Vector3 Value;

		Vector3Property() : BaseMaterialProperty(PropertyType::Vector3)
		{}
	};

	struct TextureProperty : public BaseMaterialProperty
	{
		Texture* Texture;

		TextureProperty() : BaseMaterialProperty(PropertyType::Texture)
		{}
	};

	Array<IntProperty> m_intProperties;
	Array<FloatProperty> m_floatProperties;
	Array<TextureProperty> m_textureProperties;
	Array<VectorProperty> m_vectorProperties;
	Array<Vector3Property> m_vector3Properties;

	Material* m_owningMaterial;

	void InitialisePropertyList(Material* owner, unsigned int program);

	MatVariable GetVariableHandleDefault(const char* name) const;

public:
		
	void SetFloat(MatVariable handle, float value);
	void SetFloat(const char* name, float value);
	void SetBool(MatVariable handle, bool value);
	void SetBool(const char* name, bool value);
	void SetInt(MatVariable handle, int value);
	void SetInt(const char* name, int value);
	void SetTexture(MatVariable handle, Texture* texture);
	void SetTexture(const char* name, Texture* texture);
	void SetVector(MatVariable handle, const Vector3& vector);
	void SetVector(const char* name, const Vector3& vector);
	void SetVector(MatVariable handle, const Vector4& vector);
	void SetVector(const char* name, const Vector4& vector);
	void SetColour(MatVariable handle, const Colour& colour);
	void SetColour(const char* name, const Colour& colour);
	MatVariable GetVariableHandle(const char* var, PropertyType hint = PropertyType::Unknown) const;

	void ApplyProperties();

	virtual void Release() override
	{
		m_owningMaterial = nullptr;

		m_intProperties.Clear();
		m_floatProperties.Clear();
		m_textureProperties.Clear();
		m_vector3Properties.Clear();
		m_vectorProperties.Clear();
	}

	MaterialPropertyList* CreateCopy() const;

	Material* GetMaterial() const
	{
		return m_owningMaterial;
	}
};