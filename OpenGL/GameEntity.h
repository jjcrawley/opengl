#pragma once
#include "Assertions.h"
#include "Object.h"
#include "Array.h"
#include "SceneObject.h"

class GameEntity : public Object
{	
	Array<Component*> m_components;
protected:

	bool b_active;
	SceneObject* m_rootObject;
public:
	
	virtual void OnCreate() {}
	virtual void OnDestroy() {}

	virtual void Update(float deltaTime)
	{
		int size = m_components.Size();

		for (int i = 0; i < size; ++i)
		{
			if (m_components[i]->IsEnabled())
			{
				m_components[i]->UpdateComponent(deltaTime);
			}
		}
	}

	template<typename T>
	T* AddComponent()
	{
		T* component = new T();
		component->InitialiseComponent(this);
		m_components.Add(component);

		return component;
	}

	template<typename T>
	T* GetComponent(int index)
	{
		if (m_components.InRange(index))
		{
			return static_cast<T*>(m_components[index]);
		}

		return nullptr;
	}

	template<typename T>
	T* GetComponent(const std::string& name)
	{
		int size = m_components.Size();

		for (int i = 0; i < size; ++i)
		{
			if (m_components[i]->SameName(name))
			{
				return static_cast<T*>(m_components[i]);
			}
		}
	}

	void RemoveComponent(int index);	

	void RemoveComponent(const std::string& name);	

	void SetActive(bool active)
	{
		if (b_active == active)
		{
			return;
		}

		b_active = active;

		if (b_active)
		{
			OnActivate();
		}
		else
		{
			OnDeactivate();
		}

		int size = m_components.Size();

		for (int i = 0; i < size; ++i)
		{
			m_components[i]->SetEnabled(b_active);
		}
	}

	inline bool IsActive()
	{
		return b_active;
	}

	virtual void OnActivate() {}
	virtual void OnDeactivate() {}

	void SetRoot(SceneObject* object)
	{
		if (object->GetOwner() == this)
		{
			m_rootObject = object;
		}
	}

	template<typename T = SceneObject>
	T* GetRoot() const
	{
		return static_cast<T*>(m_rootObject);
	}

	virtual void Destroy() override
	{
		int size = m_components.Size();

		for (int i = 0; i < size; ++i)
		{
			if (m_components[i])
			{
				m_components[i]->Destroy();
				delete m_components[i];
			}
		}

		OnDestroy();
	}
};