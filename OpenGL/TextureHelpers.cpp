#include "TextureHelpers.h"
#include "Macros.h"
#include "MyMath.h"
#include <iostream>
#include <fstream>
#include <algorithm>

GLuint TextureHelpers::LoadBMP(const TCHAR* imagePath)
{
	// Header will hold the necessary data to read the file:
	// Header[0] holds the type of bmp, should be BM, two chars or ushort
	// Header[2] holds file size, 4 bytes, uint
	// Header[6 and 8] are reserved, 4 bytes in total, two ushorts
	// Header[10] is the offset to the image data, 4 bytes, uint
	// Header[14] is the size of the header, 4 bytes, uint
	// Header[18] is the image width, 4 bytes, int
	// Header[22] is the image height, 4 bytes, int
	// Header[26] is the number of colour planes, 2 bytes, ushort
	// Header[28] is the bits per pixel, 2 bytes, ushort
	// Header[30] is the compression type, 4 bytes, uint
	// Header[34] is the image size in bytes, 4 bytes, uint
	// Header[38] is the xResolution, 4 bytes, int
	// Header[42] is the yResolution, 4 bytes, int
	// Header[46] is the number of colours, 4 bytes, uint
	// Header[50] is the important colours, 4 bytes, uint
		
	unsigned char header[54];	// Necessary as each BMP file begins with 54 byte header, 14 byte file header and 40 byte image info
	unsigned int dataPos;		// This is the position in the file where data begins
	unsigned int width, height;
	unsigned int imageSize;		// width * height * 3, * 3 due to BGR values

	unsigned char* data;		// This is the actual data

	std::wfstream fileStream;

	fileStream.open(imagePath, std::ios::in | std::ios::binary);

	// If we couldn't open the file stream
	if (!fileStream.is_open())
	{
		wprintf(TEXT("Unable to open the file %s"), imagePath);
		return 0;
	}

	fileStream.read((TCHAR*) header, 54);

	// Checks that we've been able to extract the header file correctly.
	// If the fileStream didn't extract 54 bytes and fill the header buffer, then we know something is wrong
	if (fileStream.gcount() != 54)
	{
		wprintf(TEXT("Something happenend with the file %s. Is it a BMP file?"), imagePath);

		fileStream.close();
		return 0;
	}
		
	// Checks that the file is something resembling a bitmap file, the first two bytes are BM
	if (header[0] != 'B' || header[1] != 'M')
	{
		wprintf(TEXT("The file %s is not a bmp file"), imagePath);

		fileStream.close();
		return 0;
	}
	
	// These conversions will make sure the necessary data is extracted from the header data
	dataPos = *(int*)&header[10];			 
	imageSize = *(int*) &header[34];		
	width = *(int*) &header[18];			
	height = *(int*) &header[22];			
		
	//std::cout << dataPos << ", " << imageSize << ", " << width << ", " << height << ", " << std::endl;

	if (imageSize == 0)
	{
		imageSize = width * height * 3;  // *3 for colour components
	}

	if (dataPos == 0)
	{
		dataPos = 54;
	}

	// Allocate a buffer for the data, then read the data in
	data = new unsigned char[imageSize];

	fileStream.read((TCHAR*) data, imageSize);
		
	// Create the texture
	GLuint textureID;
	glGenTextures(1, &textureID);

	// Bind the texture, this will cause future texture functions to modify this texture
	glBindTexture(GL_TEXTURE_2D, textureID);
		
	glTexImage2D(
		GL_TEXTURE_2D,		// The target to modify, texture 2d in this case
		0,					// The level of detail number. Level 0 is the base level, with other values specifying the mipmap reduction image
		GL_RGB,				// The components that the colour will have internally.
		width,				// Width of the image
		height,				// Height of the image
		0,					// Border value, must be 0
		GL_BGR,				// This is the format of the pixel data
		GL_UNSIGNED_BYTE,	// The data type of the pixels data
		data);				// The actual data

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	//glGenerateMipmap(GL_TEXTURE_2D);

	fileStream.close();

	delete[] data;	
	
	return textureID;
}

struct TGAHeader
{
	// The length of the id field in the file
	int idLength;

	// The type of the colour map, 0 for none, 1 for has a 256 entry palette
	int colourMapType;

	// The type of data used for the image
	// 0 = no image
	// 2 = uncompressed RGB image
	int imageType;

	// Index of the first entry in the colour palette
	short int colourMapOrigin;

	// The number of entries in the colour map
	short int colourMapLength;

	// The number of bits each pixel has in the colour palette
	int colourMapBits;

	// X coord of the lower left corner of the image
	short int xOrigin;

	// y coord of the lower left corner of the image
	short int yOrigin;

	// width and height of the image in pixels
	short int imageWidth;
	short int imageHeight;

	// Number of bytes per pixel
	int bitsPerPixel;

	// an image description, extract values using a bitmask
	char imageDescriptor;
};

GLuint TextureHelpers::LoadTGA(const TCHAR* imagePath)
{
	std::wcout << TEXT("Loading a tga image at file path: ") << imagePath << std::endl;

	TGAHeader header;

	unsigned char* pixels;

	std::fstream tgaFile(imagePath, std::ios::in | std::ios::binary);

	if (!tgaFile.is_open())
	{
		std::wcout << TEXT("Unable to load the texture at path: ") << imagePath << std::endl;
		return 0u;
	}
		
	// extract necessary values from the header of the tga file
	header.idLength = tgaFile.get();

	header.colourMapType = tgaFile.get();

	if (header.colourMapType != 2 && header.colourMapType != 0)
	{
		std::wcout << TEXT("Only support for uncompressed tga's is used: ") << header.colourMapType << std::endl;
		tgaFile.close();
		return 0u;
	}

	header.imageType = tgaFile.get();

	tgaFile.read((char*) &header.colourMapOrigin, 2);
	tgaFile.read((char*) &header.colourMapLength, 2);
		
	header.colourMapBits = tgaFile.get();

	tgaFile.read((char*) &header.xOrigin, 2);
	tgaFile.read((char*) &header.yOrigin, 2);
	tgaFile.read((char*) &header.imageWidth, 2);
	tgaFile.read((char*) &header.imageHeight, 2);
	header.bitsPerPixel = tgaFile.get();
	
	tgaFile.read(&header.imageDescriptor, 1);

	// Figure out the size of the colour map in bytes
	int colourMapByteSize = header.colourMapBits / 8 * header.colourMapLength;

	// calculate where in the file to find the pixel data, the offset from the current file position
	int offset = header.idLength + colourMapByteSize;

	tgaFile.seekg(offset, std::ios_base::cur);

	// Will see about extracting colour palette data some other time, focus on loading uncompressed images first

	unsigned char pixel[5];

	int pixelsToRead = header.imageHeight * header.imageWidth;

	int bytesPerPixel = header.bitsPerPixel / 8;

	pixels = new unsigned char[pixelsToRead * 3];

	// read in the pixel data from the file
	for (int i = 0; i < pixelsToRead; ++i)
	{
		tgaFile.read((char*) &pixel, bytesPerPixel);

		if (bytesPerPixel == 3 || bytesPerPixel == 4)
		{
			// extracts the necessary pixel values from the read pixel, TGA stores the pixel colours as BGR.
			//pixels[i].a = bytesPerPixel == 3 ? 1.0 : pixel[3];
			pixels[i * 3] = pixel[0];
			pixels[i * 3 + 1] = pixel[1];
			pixels[i * 3 + 2] = pixel[2];
		}
		else if (bytesPerPixel == 2)
		{
			unsigned char r, g, b;

			// extracts the ARGB values from the sequence in the data: ARRRRRGG GGGBBBBB
			// This sequence is stored using a little endian format
			// The new sequence is: GGGBBBBB ARRRRRGG
			r = (pixel[1] & 0x7c) << 1;
			g = ((pixel[1] & 0x03) << 6) | ((pixel[0] & 0xe0) >> 2);
			b = (pixel[0] & 0x1f) << 3;

			//pixels[i].a = pixel[1] & 0x80;		// Grab the alpha value
			
			// These extra bitshifts are to map the 31 range of the 5 bit values to the 255 range of the 8 bit values:
			pixels[i * 3] = r | (r >> 5);
			pixels[i * 3 + 1] = g | (g >> 5);
			pixels[i * 3 + 2] = b | (b >> 5);
		}
	}

	/*std::cout << idLength << std::endl;
	std::cout << colourMapType << std::endl;
	std::cout << imageType << std::endl;
	std::cout << colourMapOrigin << std::endl;
	std::cout << colourMapLength << std::endl;
	std::cout << colourMapBits << std::endl;
	std::cout << xOrigin << std::endl;
	std::cout << yOrigin << std::endl;
	std::cout << imageWidth << std::endl;
	std::cout << imageHeight << std::endl;
	std::cout << bitsPerPixel << std::endl;*/

	tgaFile.close();	
	
	GLuint textureID;

	glGenTextures(1, &textureID);

	//glActiveTexture(GL_TEXTURE0 + 1);

	glBindTexture(GL_TEXTURE_2D, textureID);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, header.imageWidth, header.imageHeight, 0, GL_BGR, GL_UNSIGNED_BYTE, pixels);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glGenerateMipmap(GL_TEXTURE_2D);

	delete pixels;

	return textureID;
}

#pragma pack(push, 1)

// A structure that encompasses a DDS header
struct DDSHeader
{
	// The size of the header file, excluding the magic word DDS
	// Should be 124 bytes
	unsigned int size;

	// Bitmask to indicate which fields in the header have valid data
	unsigned int flags;

	// Height of texture in pixels
	unsigned int height;

	// Width of texture in pixels
	unsigned int width;

	// Number of bytes per scanline in an uncompressed texture

	union
	{
		unsigned int linearSize;
		unsigned int pitch;
	};

	// The depth of a volume texture
	unsigned int depth;

	// The number of mipmap levels
	unsigned int mipMapCount;

	// Unused
	unsigned int reservedOne[11];

	// A helper struct to represent the pixel format of the dds image
	struct
	{
		// size of structure, should be 32 bytes
		unsigned int size;

		// bit flags to indicate which fields have valid data in them
		unsigned int flags;

		// Four character code to specify compression or custom formats. 
		// Could be DXT1, DXT2, DXT3, DXT 4 or DXT5. 
		unsigned int fourCC;

		// Number of bits per RGB value, could include Alpha.
		unsigned int RGBBitCount;

		// Bit mask for red data
		unsigned int RBitMask;

		// Bit mask for green data
		unsigned int GBitMask;

		// Bit mask for blue data
		unsigned int BBitMask;

		// Bit mask for alpha data
		unsigned int ABitMask;
	} pixelFormat;

	// Specifies the surface complexity
	unsigned int surfaceComplexity;

	// Additional data on the surface that is stored
	unsigned int additionalSurfaceData;

	// Unused
	unsigned int caps3;

	// Unused
	unsigned int caps4;

	// Unused
	unsigned int reservedTwo[2];
};

#define DDSD_CAPS         0x1
#define DDSD_HEIGHT       0x2
#define DDSD_WIDTH        0x4
#define DDSD_PITCH        0x8
#define DDSD_PIXELFORMAT  0x1000
#define DDSD_MIPMAPCOUNT  0x20000
#define DDSD_LINEARSIZE   0x80000
#define DDSD_DEPTH        0x800000

#define DDPF_ALPHAPIXELS 0x1
#define DDPF_ALPHA		 0x2
#define DDPF_FOURCC		 0x4
#define DDPF_RGB		 0x40
#define DDPF_YUV		 0x200
#define DDPF_LUMINANCE	 0x20000

#define DDSCAPS_COMPLEX 0x8
#define DDSCAPS_MIPMAP 0x400000
#define DDSCAPS_TEXTURE 0x1000

#define FOURCC_DXT1  0x31545844
#define FOURCC_DXT3  0x33545844
#define FOURCC_DXT5  0x35545844
#define FOURCC_DXT10 0x0a545844

#pragma pack(pop)

GLuint TextureHelpers::LoadDDS(const TCHAR* imagePath, bool flip)
{
	std::fstream file(imagePath, std::ios::in | std::ios::binary);

	if (!file.is_open())
	{
		std::cout << "Couldn't open the dds file" << std::endl;

		file.close();
		return 0u;
	}

	unsigned int magicNum;

	file.read((char*) &magicNum, 4);

	// The first four bytes are the magic number
	// This tells us that it is a DDS file, akin to checking for the ascii characters: "DDS "
	if (magicNum != 0x20534444)
	{
		std::cout << "Not a dds file" << std::endl;
		file.close();
		return 0u;
	}

	DDSHeader header;

	file.read((char*) &header, 124);

	// Header size should always be 124, the pixel format size should always be 32.
	// The sizes tell us how many bytes the header is and how many bytes the pixel format is
	if (header.size != 124 || header.pixelFormat.size != 32)
	{
		std::cout << "Issue with header size " << std::endl;
		file.close();
		return 0u;
	}

	// Check that we have all the important DDS features
	int flags = DDSD_CAPS | DDSD_HEIGHT | DDSD_WIDTH | DDSD_PIXELFORMAT;

	if (!(flags & header.flags))
	{
		std::cout << "The dds file " << imagePath << " is missing important data" << std::endl;
		file.close();
		return 0u;
	}

	// Check that the fourcc field has data that we can use. 
	// Tells us the format/compression type for the pixels
	flags = DDPF_FOURCC;

	if (!(flags & header.pixelFormat.flags))
	{
		std::cout << "The file " << imagePath << " requires a compression type" << std::endl;
		file.close();
		return 0u;
	}

	std::cout << "Reading dds file" << std::endl;

	/*cout << "size: " << header.size << endl;
	cout << "flags: " << std::bitset<32>(header.flags) << endl;
	cout << "height: " << header.height << endl;
	cout << "width: " << header.width << endl;
	cout << "pitchOrLinearSize: " << header.linearSize << endl;
	cout << "depth: " << header.depth << endl;
	cout << "mipMapCount: " << header.mipMapCount << endl;
	cout << "surfaceComplexity: " << header.surfaceComplexity << endl;
	cout << "additionalSurfaceData: " << header.additionalSurfaceData << endl;

	cout << "Pixel info: " << endl;
	cout << "size: " << header.pixelFormat.size << endl;
	cout << "flags: " << std::bitset<32>(header.pixelFormat.flags) << endl;
	cout << "fourCC: " << header.pixelFormat.fourCC << endl;
	cout << "BitCount: " << header.pixelFormat.RGBBitCount << endl;
	cout << "RBitMask: " << std::bitset<32>(header.pixelFormat.RBitMask) << endl;
	cout << "GBitMask: " << std::bitset<32>(header.pixelFormat.GBitMask) << endl;
	cout << "RBitMask: " << std::bitset<32>(header.pixelFormat.RBitMask) << endl;
	cout << "ABitMask: " << std::bitset<32>(header.pixelFormat.ABitMask) << endl;*/

	unsigned char* pixels;

	unsigned int blockSize = header.pixelFormat.fourCC == FOURCC_DXT1 ? 8 : 16;

	unsigned int maxSliceSize = std::max<unsigned int>(1, (header.width + 3) / 4) * std::max<unsigned int>(1, (header.height + 3) / 4) * blockSize;

	unsigned int pixelCount;

	// Calculate a rough pixel count for the texture.
	// This count includes space for the mip maps to.
	// Sometimes the linear size isn't set, as such, it is necessary to calculate a rough size using the size, in bytes, of the largest mip
	pixelCount = header.mipMapCount > 1 ?
		(header.linearSize > 0 ? header.linearSize * 2 : maxSliceSize * 2) :
		header.linearSize;

	if (pixelCount == 0)
	{
		pixelCount = maxSliceSize * 2;
	}

	pixels = new unsigned char[pixelCount * sizeof(unsigned char)];

	file.read((char*) pixels, pixelCount);

	file.close();
		
	unsigned int width = header.width;
	unsigned int height = header.height;
	unsigned int offset = 0;

	GLuint returnTexture;
	glGenTextures(1, &returnTexture);

	//glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, returnTexture);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	GLuint format;

	switch (header.pixelFormat.fourCC)
	{
		case FOURCC_DXT1:
			//format = GL_compressed_argb
			format = GL_COMPRESSED_RGBA_S3TC_DXT1_EXT;
			break;
		case FOURCC_DXT3:
			format = GL_COMPRESSED_RGBA_S3TC_DXT3_EXT;
			break;
		case FOURCC_DXT5:
			format = GL_COMPRESSED_RGBA_S3TC_DXT5_EXT;
			break;
		default:
			delete pixels;
			return 0u;
	}
		
	unsigned char* flippedBuffer = new unsigned char[maxSliceSize * sizeof(unsigned char)];
			
	if (header.mipMapCount == 0)
	{
		header.mipMapCount = 1;
	}

	for (unsigned int level = 0; level < header.mipMapCount && (width || height); ++level)
	{
		// The number of blocks in a single row, since the texture is compressed
		unsigned int blockCount = std::max<unsigned int>(1, (width + 3) / 4);

		// The number of bytes per mip row
		unsigned int pitch = blockCount * blockSize;

		// The amount of data for the current mip level, measured in bytes 
		unsigned int slice = pitch * std::max<unsigned int>(1, (height + 3) / 4);
		
		if (!flip)
		{
			glCompressedTexImage2D(GL_TEXTURE_2D, level, format, width, height, 0, slice, pixels + offset);
		}
		else
		{
			// Ensures that we are at the beginning of the last row, as opposed to the end
			unsigned int rowOffset = slice - pitch;

			// The number of lines we need to write
			unsigned int linesToWrite = slice / pitch;

			// Ensures we have at least one line to write, which should always be the case
			linesToWrite = linesToWrite == 0 ? 1 : linesToWrite;

			// A buffer to write our data to, the original data but flipped
			unsigned char* blockBuffer = flippedBuffer;

			// A pointer to the beginning of the current mip
			unsigned char* currentMip = pixels + offset;

			// This will iterate up the texture, writing the data in reverse order. This has the effect of flipping the texture
			while (linesToWrite)
			{
				// Get the right row of the current mip to right
				unsigned char* row = currentMip + rowOffset;

				// This will flip the blocks in a single row.				
				for (unsigned int j = 0; j < blockCount; ++j)
				{
					unsigned int blockOffset = j * blockSize;

					if (header.pixelFormat.fourCC == FOURCC_DXT3)
					{
						// Flip the alpha values, each alpha value is 4 bits
						blockBuffer[blockOffset] = row[6 + blockOffset];
						blockBuffer[1 + blockOffset] = row[7 + blockOffset];
						blockBuffer[2 + blockOffset] = row[4 + blockOffset];
						blockBuffer[3 + blockOffset] = row[5 + blockOffset];
						blockBuffer[4 + blockOffset] = row[2 + blockOffset];
						blockBuffer[5 + blockOffset] = row[3 + blockOffset];
						blockBuffer[6 + blockOffset] = row[blockOffset];
						blockBuffer[7 + blockOffset] = row[1 + blockOffset];

						// Flip DXT1 block
						// The colours of the block, don't need to be flipped
						blockBuffer[8 + blockOffset] = row[8 + blockOffset];
						blockBuffer[9 + blockOffset] = row[9 + blockOffset];
						blockBuffer[10 + blockOffset] = row[10 + blockOffset];
						blockBuffer[11 + blockOffset] = row[11 + blockOffset];

						// The next four bytes represent the pixels compressed in the block
						// Each byte represents a single row, going from top to bottom
						blockBuffer[12 + blockOffset] = row[15 + blockOffset];
						blockBuffer[13 + blockOffset] = row[14 + blockOffset];
						blockBuffer[14 + blockOffset] = row[13 + blockOffset];
						blockBuffer[15 + blockOffset] = row[12 + blockOffset];
					}
					else if (header.pixelFormat.fourCC == FOURCC_DXT5)
					{
						// The alpha block for the texture
						// First 2 bytes are reference alphas, don't need to be flipped						
						blockBuffer[blockOffset] = row[blockOffset];
						blockBuffer[1 + blockOffset] = row[1 + blockOffset];

						// The next 6 bytes are the alpha codes for each texel in the block
						// Each texel has 3 bits each, representing how to combine the reference alphas
						// Letters a through p represent the pixels:

						// B2 a,a,a b,b,b c,c
						// B3 c d,d,d e,e,e f
						// B4 f,f g,g,g h,h,h
						// B5 i,i,i j,j,j k,k
						// B6 k l,l,l m,m,m n
						// B7 n,n o,o,o p,p,p

						// After flipping it becomes:
						// B`2 m,m,m n,n,n o,o
						// B`3 o p,p,p i,i,i j
						// B`4 j,j k,k,k l,l,l
						// B`5 e,e,e f,f,f g,g
						// B`6 g h,h,h a,a,a b
						// B`7 b,b c,c,c d,d,d

						blockBuffer[2 + blockOffset] = (row[7 + blockOffset] << 4) | (row[6 + blockOffset] >> 4);
						blockBuffer[3 + blockOffset] = (row[5 + blockOffset] << 4) | (row[7 + blockOffset] >> 4);
						blockBuffer[4 + blockOffset] = (row[6 + blockOffset] << 4) | (row[5 + blockOffset] >> 4);
						blockBuffer[5 + blockOffset] = (row[4 + blockOffset] << 4) | (row[3 + blockOffset] >> 4);
						blockBuffer[6 + blockOffset] = (row[2 + blockOffset] << 4) | (row[4 + blockOffset] >> 4);
						blockBuffer[7 + blockOffset] = (row[3 + blockOffset] << 4) | (row[2 + blockOffset] >> 4);

						// The colours of the block, don't need to be flipped
						blockBuffer[8 + blockOffset] = row[8 + blockOffset];
						blockBuffer[9 + blockOffset] = row[9 + blockOffset];
						blockBuffer[10 + blockOffset] = row[10 + blockOffset];
						blockBuffer[11 + blockOffset] = row[11 + blockOffset];

						// The next four bytes represent the pixels compressed in the block
						// Each byte represents a single row, going from top to bottom
						blockBuffer[12 + blockOffset] = row[15 + blockOffset];
						blockBuffer[13 + blockOffset] = row[14 + blockOffset];
						blockBuffer[14 + blockOffset] = row[13 + blockOffset];
						blockBuffer[15 + blockOffset] = row[12 + blockOffset];
					}
					else if (header.pixelFormat.fourCC == FOURCC_DXT1)
					{
						// The colours of the block. 
						// Each colour is represented by two bytes in little endian format
						// Don't need to be swapped
						blockBuffer[blockOffset] = row[blockOffset];
						blockBuffer[1 + blockOffset] = row[1 + blockOffset];
						blockBuffer[2 + blockOffset] = row[2 + blockOffset];
						blockBuffer[3 + blockOffset] = row[3 + blockOffset];

						// The next four bytes represent the pixels compressed in the block
						// Each byte represents a single row, going from top to bottom
						blockBuffer[4 + blockOffset] = row[7 + blockOffset];
						blockBuffer[5 + blockOffset] = row[6 + blockOffset];
						blockBuffer[6 + blockOffset] = row[5 + blockOffset];
						blockBuffer[7 + blockOffset] = row[4 + blockOffset];
					}
				}

				// Iterating in reverse to write the texture from bottom to top
				rowOffset -= pitch;
				blockBuffer += pitch;

				--linesToWrite;
			}

			glCompressedTexImage2D(GL_TEXTURE_2D, level, format, width, height, 0, slice, flippedBuffer);
		}		

		offset += slice;
		width /= 2;
		height /= 2;

		if (width == 0)
		{
			width = 1;
		}
		
		if (height == 0)
		{
			height = 1;
		}
	}
			
	delete pixels;
	delete flippedBuffer;

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

	return returnTexture;
}

GLuint TextureHelpers::CreateSinglePixelColouredTexture(Vector3 colour)
{
	GLuint textureID;
	glGenTextures(1, &textureID);

	glBindTexture(GL_TEXTURE_2D, textureID);

	colour.X = FMath::Clamp(colour.X, 1.0f, 0.0f);
	colour.Y = FMath::Clamp(colour.Y, 1.0f, 0.0f);
	colour.Z = FMath::Clamp(colour.Z, 1.0f, 0.0f);
	
	unsigned char data[] = { (unsigned char)(colour.X * 255.0f), (unsigned char)(colour.Y * 255.0f), (unsigned char)(colour.Z * 255.0f)};
	
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 1, 1, 0, GL_RGB, GL_UNSIGNED_BYTE, data);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	return textureID;
}