#include "ShaderHelpers.h"
#include <GL/glew.h>
#include <stdio.h>
#include <fstream>

using namespace std;

bool ReadFile(const TCHAR* filePath, string& code)
{
	ifstream vertexFileStream(filePath, ios::in);

	// If the file stream was opened
	if (vertexFileStream.is_open())
	{
		string line = "";

		// While there are lines to read, keep reading
		while (getline(vertexFileStream, line))
		{
			code += "\n" + line;
		}

		vertexFileStream.close();
		
		return true;
	}
	
	return false;
}

ShaderHelpers::ShaderHelpers()
{
}

ShaderHelpers::~ShaderHelpers()
{
}

unsigned int ShaderHelpers::LoadShader(const TCHAR* vertexFilePath, const TCHAR* fragmentPath)
{	
	string vertexCode;
	string fragmentCode;

	if (!ReadFile(vertexFilePath, vertexCode))
	{
		wprintf(L"No vertex file in %s", vertexFilePath);

		return 0;
	}
		
	if (!ReadFile(fragmentPath, fragmentCode))
	{
		wprintf(L"No fragment shader at this location %s", fragmentPath);

		return 0;
	}
	
	GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
	GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);

	// Compile the vertex shader
	wprintf(TEXT("Compiling shader : %s\n"), vertexFilePath);
	
	if (!CompileShader(vertexShader, vertexCode))
	{
		wprintf(TEXT("The vertex shader didn't compile correctly"));
	}

	wprintf(TEXT("Compiling fragment shader : %s\n"), fragmentPath);

	if (!CompileShader(fragmentShader, fragmentCode))
	{
		wprintf(TEXT("The fragment shader didn't compile correctly"));
	}	
	
	printf("Linking program\n");

	GLint result;
	GLint infoLogLength;

	GLuint program = glCreateProgram();

	// The created shaders are attached to the created program
	glAttachShader(program, vertexShader);
	glAttachShader(program, fragmentShader);

	// The program is linked with necessary information being kept transferred to the right piece of the pipeline
	glLinkProgram(program);
	
	// This will retrieve information about the program, namely the link status and the info length
	glGetProgramiv(program, GL_LINK_STATUS, &result);
	glGetProgramiv(program, GL_INFO_LOG_LENGTH, &infoLogLength);

	// If we have information, print it to the console
	if (result == GL_FALSE)
	{
		char* info = new char[infoLogLength + 1];

		glGetProgramInfoLog(program, infoLogLength, nullptr, &info[0]);

		printf("%s\n", &info[0]);

		delete[] info;
	}

	// Detach the shaders from the program and delete them.
	glDetachShader(program, vertexShader);
	glDetachShader(program, fragmentShader);

	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);

	return program;
}

bool ShaderHelpers::CompileShader(unsigned int shader, const std::string& shaderSource)
{
	int result = 0;

	int infoLogLength;

	const char* source = shaderSource.c_str();

	// This will populate a shader object with the necessary information to let it be generated
	glShaderSource(shader, 1, &source, NULL);

	// This will actually submit the code to be compiled and pushed up to the GPU
	glCompileShader(shader);

	// This will get the status of the compilation, if there are any errors it will put gl_true into the result
	glGetShaderiv(shader, GL_COMPILE_STATUS, &result);

	// This will retrieve the length of the info log, so a suitable buffer can be allocated
	glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLogLength);

	// If the shader had an info log, this would be > 0
	if (result == GL_FALSE)
	{
		char* infoString = new char[infoLogLength + 1];

		// Retrieve the shader info log 
		glGetShaderInfoLog(shader, infoLogLength, nullptr, &infoString[0]);

		printf("%s\n", &infoString[0]);

		delete[] infoString;
	}

	return true;
}