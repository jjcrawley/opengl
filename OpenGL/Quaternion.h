#pragma once

#include "Mathfwd.h"
#include "FMath.h"

struct Quaternion
{
	float X;
	float Y;
	float Z;
	float W;

public:

	static const Quaternion Identity;

	Quaternion()
	{}

	Quaternion(float w, float x, float y, float z) : W(w), X(x), Y(y), Z(z)
	{}

	Quaternion(const Matrix& M);
	Quaternion(float angle, const Vector3& axis);
	Quaternion(const ERotation& eulerAngles);
	
	float Magnitude() const;
	float SquareMagnitude() const;
	float Dot(const Quaternion& Q) const;
	Quaternion Exp() const;
	Quaternion Exp(float power) const;
	Quaternion Log() const;
	Quaternion Inverse() const;
	Quaternion Normalise() const;
	Vector3 Forward() const;
	Vector3 Right() const;
	Vector3 Up() const;

	inline bool IsNormalised() const;

	Quaternion operator*(const Quaternion& Q) const;
	Quaternion& operator*=(const Quaternion& Q);
	Quaternion operator*(const float scale) const;
	
	Vector3 operator*(const Vector3& V) const;
	Vector4 operator*(const Vector4& v) const;

	Quaternion operator-(const Quaternion& Q) const;
	Quaternion operator-() const;	

	Vector3 RotateVector(const Vector3& V) const;
	Vector3 UnRotateVector(const Vector3& v) const;
	
	ERotation EulerAngles() const;

	static Quaternion Slerp(const Quaternion& from, const Quaternion& to, float alpha);

	static void ToAxisAngle(const Quaternion& Q, float& angle, Vector3& axis);
};

inline bool operator==(const Quaternion& left, const Quaternion& right)
{
	return left.W == right.W && left.X == right.X && left.Y == right.Y && left.Z == right.Z;
}

inline bool operator!=(const Quaternion& left, const Quaternion& right)
{
	return !(left == right);
}

template<typename T>
inline Quaternion FMath::Slerp(const Quaternion& from, const Quaternion& to, T alpha)
{
	return Quaternion::Slerp(from, to, alpha);
}