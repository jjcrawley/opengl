#pragma once
#include "Object.h"

class GameEntity;

class Component : public Object
{
	GameEntity* m_owner;

protected:
	bool b_enabled;

public:
	Component(){}
	virtual ~Component(){}

	virtual void InitialiseComponent(GameEntity* entity);
	virtual void Destroy() override;

	virtual void UpdateComponent(float deltaTime) {}

	inline GameEntity* GetOwner() const
	{
		return m_owner;
	}

	void SetEnabled(bool enabled)
	{
		if (b_enabled == enabled)
		{
			return;
		}

		b_enabled = enabled;

		if (enabled)
		{
			OnEnable();
		}
		else if (!enabled)
		{
			OnDisable();
		}
	}

	inline bool IsEnabled()
	{
		return b_enabled;
	}

	virtual void OnEnable() {}
	virtual void OnDisable() {}
};