#include "ERotation.h"
#include "MyMath.h"
#include "Debugger.h"

ERotation::ERotation(const Matrix& m)
{
	// Use z axis to find polar coords, gives the pitch and heading values
	// Construct a new basis using heading and pitch
	// Take the dot product of the x and y axis using the new x axis
	// Use -atan2(x.dot(xBasis), y.dot(xBasis)) this is necessary to extract the sign for the roll, because cos(-theta) = -cos(theta)
	Vector3 ZAxis = m.ZAxis();
	
	Pitch = -asin(ZAxis.Y);
		
	if (abs(Pitch - HALF_PI) > 0.0001f)
	{
		Heading = atan2(ZAxis.X, ZAxis.Z);

		const float ch = cos(Heading);
		const float sh = sin(Heading);
		const float sp = sin(Pitch);
		const float cp = cos(Pitch);

		Vector3 XBasis = Vector3(ch, 0, -sh);

		Roll = -atan2(m.YAxis().Dot(XBasis), m.XAxis().Dot(XBasis));
	}
	else
	{
		Heading = atan2(-m.M[0][2], m.M[0][0]);

		Roll = 0;
	}

	// Alternative approach:
	// Deconstruct rotation matrix to find heading, pitch and roll. This approach will differ depending on the rotation matrix
	// p = -arcsin(M[2][1])
	// h = atan2(M[2][0], M[2][2])
	// r = atan2(M[0][1], M[1][1]) 
	// Check for pitch approx == 90 degrees. This indicates issues with gimbal lock.
	// To fix it, have the roll set to 0, and calculate heading.
	// The new heading can be calculated by: atan2(-M[0][2], M[0][0])
}

ERotation::ERotation(const Quaternion& q)
{
	*this = q.EulerAngles();
}

Quaternion ERotation::ToQuaternion() const
{
	return Quaternion(*this);
}

inline Matrix ERotation::ToMatrix() const
{
	return MatrixFactory::Rotation(Pitch, Heading, Roll);
}

void ERotation::Add(float pitch, float heading, float roll)
{
	Pitch += pitch;
	Heading += heading;
	Roll += roll;
}

std::ostream& operator<<(std::ostream& out, const ERotation& R)
{
	return out << '[' << R.Pitch << ", " << R.Heading << ", " << R.Roll << "]" << std::endl;
}