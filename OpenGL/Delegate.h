#pragma once

// A helper struct to resolve a function pointer type for member functions
template<typename Class, typename RetVal, typename... Args>
struct MemFPtr
{
	typedef RetVal(Class::*Func)(Args...);
};

// A function pointer resolver for a standard C function
template<typename RetVal, typename... Args>
struct FPtr
{
	using Func = RetVal(*)(Args...);
};

template<typename RetVal, typename... Args>
class Delegate;

// This interface defines the base functionality a method wrapper must provide
template<typename RetVal, typename... Args>
class IMethodWrapper
{
	friend class Delegate<RetVal, Args...>;

public:
	virtual RetVal Execute(Args...) const = 0;
	virtual RetVal SafeExecute(Args...) const = 0;
	virtual void* MakeCopy() const = 0;
	virtual bool CanExecute() const = 0;
	virtual void* GetObj() const = 0;

protected:
	virtual void* GetFunction() const = 0;	
};

// A method wrappr for C++ member function pointers, should use the delegate class to instantiate these instead of using them directly
template<typename UserClass, typename RetVal, typename... Args>
class ClassMethodWrapper : public IMethodWrapper<RetVal, Args...>
{
	using Function = typename MemFPtr<UserClass, RetVal, Args...>::Func;

	UserClass* m_object;
	Function m_function;

	friend class Delegate<RetVal, Args...>;
public:

	ClassMethodWrapper(UserClass* object, const Function& func) : m_object(object), m_function(func)
	{}

	virtual RetVal Execute(Args... args) const override
	{
		(m_object->*m_function)(args...);
		//cout << "Executing" << endl;
	}

	virtual bool CanExecute() const override
	{
		if (m_object == nullptr)
		{
			//cout << "Object is null" << endl;
			return false;
		}
		else if (m_function == nullptr)
		{
			//cout << "Function is null" << endl;
			return false;
		}

		return true;
	}

	virtual RetVal SafeExecute(Args... args) const override
	{
		if (CanExecute())
		{
			return (m_object->*m_function)(args...);
		}
		else
		{
			return RetVal();
		}
	}

	virtual void* MakeCopy() const override
	{
		ClassMethodWrapper<UserClass, RetVal, Args...>* copy;
		copy = new ClassMethodWrapper<UserClass, RetVal, Args...>(m_object, m_function);

		return (void*) copy;
	}
protected:
	virtual void* GetFunction() const override
	{
		return (void*) &m_function;
	}

	virtual void* GetObj() const override
	{
		return (void*) m_object;
	}
};

// A wrapper for a standard C function pointer, not for member functions.
// Shouldn't be used directly, use the delegate class instead
template<typename RetVal, typename... Args>
class StaticMethodWrapper : public IMethodWrapper<RetVal, Args...>
{
	friend class Delegate<RetVal, Args...>;	
public:
	using Function = typename FPtr<RetVal, Args...>::Func;

	StaticMethodWrapper(const Function& func) : m_function(func)
	{}

	virtual RetVal SafeExecute(Args... args) const override
	{
		if (m_function == nullptr)
		{
			//cout << "Function is null" << endl;
			return RetVal();
		}
		else
		{
			return m_function(args...);
		}
	}

	virtual RetVal Execute(Args... args) const override
	{
		//cout << "Executing" << endl;		
		return m_function(args...);
	}

	virtual bool CanExecute() const override
	{
		return m_function != nullptr;
	}

	virtual void* MakeCopy() const override
	{
		using ClassType = StaticMethodWrapper<RetVal, Args...>;

		ClassType* copy = new ClassType(m_function);

		return (void*) copy;
	}

	virtual void* GetFunction() const override
	{
		return (void*) &m_function;
	}

	virtual void* GetObj() const override
	{
		return nullptr;
	}
private:
	Function m_function;
};

template<typename RetVal, typename Functor, typename... Args>
class FunctorMethodWrapper : public IMethodWrapper<RetVal, Args...>
{
public:

	FunctorMethodWrapper(const Functor& function) : m_function(function)
	{}

	/*FunctorMethodWrapper(Functor&& function) : m_function(std::move(function))
	{}*/

	inline virtual RetVal Execute(Args... args) const override
	{
		m_function(args...);
		//cout << "Executing" << endl;
	}

	inline virtual bool CanExecute() const override
	{
		return true;
	}

	virtual RetVal SafeExecute(Args... args) const override
	{
		if (CanExecute())
		{
			return Execute(args...);
		}
		else
		{
			return RetVal();
		}
	}

	virtual void* MakeCopy() const override
	{
		FunctorMethodWrapper<RetVal, Functor, Args...>* copy;
		copy = new FunctorMethodWrapper<RetVal, Functor, Args...>(m_function);

		return (void*) copy;
	}

protected:
	inline virtual void* GetFunction() const override
	{
		return nullptr;
	}

	inline virtual void* GetObj() const override
	{
		return nullptr;
	}

private:
	Functor m_function;
};

// A wrapper factory to generate the necessary method wrappers for the delegates to use
class WrapperFactory
{
public:
	template<typename Class, typename RetVal, typename... Args>
	static IMethodWrapper<RetVal, Args...>* CreateObjectWrapper(Class* object, typename MemFPtr<Class, RetVal, Args...>::Func func)
	{
		using ValueType = ClassMethodWrapper<Class, RetVal, Args...>;

		ValueType* instance = new ValueType(object, func);

		return static_cast<IMethodWrapper<RetVal, Args...>*>(instance);
	}

	template<typename RetVal, typename... Args>
	static IMethodWrapper<RetVal, Args...>* CreateFunctionWrapper(typename FPtr<RetVal, Args...>::Func func)
	{
		using ValueType = StaticMethodWrapper<RetVal, Args...>;

		ValueType* instance = new ValueType(func);

		return static_cast<IMethodWrapper<RetVal, Args...>*>(instance);
	}

	template<typename Functor, typename RetVal, typename... Args>
	static IMethodWrapper<RetVal, Args...>* CreateFunctorWrapper(const Functor& function)
	{
		using ValueType = FunctorMethodWrapper<RetVal, Functor, Args...>;

		ValueType* instance = new ValueType(function);

		return static_cast<IMethodWrapper<RetVal, Args...>*>(instance);
	}
};

class DelegateHandle
{
	unsigned int m_id;

	static unsigned int s_idCounter;

public:
	DelegateHandle()
	{
		if (s_idCounter == 0)
		{
			s_idCounter = 1;
		}

		m_id = s_idCounter++;
	}

	DelegateHandle(const DelegateHandle& handle) : m_id(handle.m_id)
	{}

	DelegateHandle& operator=(const DelegateHandle& handle)
	{
		if (this != &handle)
		{
			m_id = handle.m_id;
		}

		return *this;
	}

	friend bool operator==(const DelegateHandle& left, const DelegateHandle& right)
	{
		return left.m_id == right.m_id;
	}

	friend bool operator!=(const DelegateHandle& left, const DelegateHandle& right)
	{
		return !(left == right);
	}
};

// A delegate class that allows simple binding for C functions and C++ member functions
template<typename RetVal, typename... Args>
class Delegate
{
	IMethodWrapper<RetVal, Args...>* m_method;
	DelegateHandle m_handle;

public:	
		
	template<typename Object>
	using MemberFunction = typename MemFPtr<Object, RetVal, Args...>::Func;
	using CFunction = typename FPtr<RetVal, Args...>::Func;

	Delegate() : m_method(nullptr), m_handle()
	{}

	~Delegate()
	{
		Unbind();
	}

	Delegate(const Delegate& del)
	{
		*this = del;
		//cout << "Made a double" << endl;
	}

	Delegate& operator=(const Delegate& del)
	{
		if (&del != this)
		{
			if (del.m_method != nullptr)
			{
				m_method = (IMethodWrapper<RetVal, Args...>*)del.m_method->MakeCopy();
				m_handle = del.m_handle;
			}
			else
			{
				Unbind();
			}
		}

		return *this;
	}

	template<typename Class>
	inline void BindObject(Class* object, typename MemFPtr<Class, RetVal, Args...>::Func func)
	{
		if (m_method)
		{
			Unbind();
		}

		m_method = WrapperFactory::CreateObjectWrapper<Class, RetVal, Args...>(object, func);
	}

	inline void BindFunction(typename FPtr<RetVal, Args...>::Func func)
	{
		if (m_method)
		{
			Unbind();
		}

		m_method = WrapperFactory::CreateFunctionWrapper<RetVal, Args...>(func);
	}

	template<typename Functor>
	inline void BindFunctor(const Functor& function)
	{
		if (m_method)
		{
			Unbind();
		}

		m_method = WrapperFactory::CreateFunctorWrapper<Functor, RetVal, Args...>((function));
	}

	inline void Unbind()
	{
		if (m_method != nullptr)
		{
			delete m_method;
		}

		m_method = nullptr;
		//cout << "Was just unbound" << endl;
	}

	inline bool IsBound()
	{
		return m_method != nullptr;
	}

	template<typename Class>
	inline bool SameFunction(Class* object, typename MemFPtr<Class, RetVal, Args...>::Func function)
	{
		return *(static_cast<typename MemFPtr<Class, RetVal, Args...>::Func*>(m_method->GetFunction())) == function && m_method->GetObj() == object;
	}

	inline bool SameFunction(typename FPtr<RetVal, Args...>::Func function)
	{
		return *(static_cast<typename FPtr<RetVal, Args...>::Func*>(m_method->GetFunction())) == function;
	}

	inline RetVal SafeExecute(Args... args)
	{
		if (m_method != nullptr)
		{
			return m_method->SafeExecute(args...);
		}
		else
		{
			//std::cout << "The delegate isn't bound" << std::endl;
			return RetVal();
		}
	}

	inline RetVal Execute(Args... args)
	{
		return m_method->Execute(args...);
	}

	inline bool SameHandle(const Delegate<RetVal, Args...>& del)
	{
		return m_handle == del.m_handle;
	}

	inline bool SameHandle(const DelegateHandle& handle)
	{
		return m_handle == handle;
	}

	inline DelegateHandle GetHandle()
	{
		return m_handle;
	}
};