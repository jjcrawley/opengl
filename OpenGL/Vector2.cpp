#include "Vector2.h"
#include "Debugger.h"

std::ostream& operator<<(std::ostream& out, const Vector2& M)
{
	return out << "[" << M.X << ", " << M.Y << "]";
}