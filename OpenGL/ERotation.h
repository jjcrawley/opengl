#pragma once

#include "Mathfwd.h"
#include "FMath.h"

struct Degrees
{};

// E for euler
// Represents an euler rotation
// The sequence of the rotations is: pitch, heading then roll. This is a rotation around X, Y then Z
// The rotations are stored in radians
struct ERotation
{
public:
	float Pitch;
	float Heading;
	float Roll;
	
	ERotation() {}

	// The constructor PHR constructor takes radians as the arguments
	ERotation(float pitch, float heading, float roll) : Pitch(pitch), Heading(heading), Roll(roll)
	{}

	// This version of the constructor takes the angles in as degrees, then converts them to radians
	ERotation(float pitch, float heading, float roll, Degrees) : Pitch(FMath::DegToRad(pitch)), Heading(FMath::DegToRad(heading)), Roll(FMath::DegToRad(roll))
	{}
		
	ERotation(const Matrix& m);
	ERotation(const Quaternion& q);
	
	Quaternion ToQuaternion() const;
	Matrix ToMatrix() const;

	void Add(float pitch, float heading, float roll);
	
	// Set the pitch using degrees
	inline void PitchDeg(float degrees)
	{
		Pitch = FMath::DegToRad(degrees);
	}

	// Set the heading using degress
	inline void HeadingDeg(float degrees)
	{
		Heading = FMath::DegToRad(degrees);
	}

	// Set the roll using degrees
	inline void RollDeg(float degrees)
	{
		Roll = FMath::DegToRad(degrees);
	}

	inline auto PitchDeg() const -> decltype(FMath::RadToDeg(Pitch))
	{
		return FMath::RadToDeg(Pitch);
	}

	inline auto HeadingDeg() const -> decltype(FMath::RadToDeg(Heading))
	{
		return FMath::RadToDeg(Heading);
	}

	inline auto RollDeg() const -> decltype(FMath::RadToDeg(Roll))
	{
		return FMath::RadToDeg(Roll);
	}
};