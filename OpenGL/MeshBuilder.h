#pragma once

#include "Mesh.h"

class MeshBuilder
{
public:
	MeshBuilder();
	~MeshBuilder();

	static Mesh* CubeMesh();
};