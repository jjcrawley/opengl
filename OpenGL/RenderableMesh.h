#pragma once
#include "Material.h"
#include "Mesh.h"
#include "SceneObject.h"
#include "Texture.h"

class RenderableMesh : public SceneObject
{
	typedef SceneObject Super;

	unsigned int m_vertexArrayObject;
	unsigned int m_indexBuffer;
	unsigned int m_vertexBuffer;
	unsigned int m_uvBuffer;
	unsigned int m_normalBuffer;
		
	unsigned int m_textureHandle;

protected:
	
	Mesh* m_mesh;
	Material* m_material;
	//Texture* m_texture;

	MaterialPropertyList* m_propertyList;

public:
		
	virtual void InitialiseComponent(GameEntity* entity) override;
	
	void SetMesh(Mesh* mesh);
	void SetMaterial(Material* material);
	virtual void Render(const Matrix& viewProjection);
	virtual void Destroy() override;
	virtual void OnEnable() override;
	virtual void OnDisable() override;
	
	void SetTexture(Texture* texture)
	{
		//m_texture = texture;
	}

	const MaterialPropertyList* GetPropertyList() const
	{
		return m_propertyList;
	}

	void SetPropertyList(MaterialPropertyList* list)
	{
		m_propertyList = list;
	}
};