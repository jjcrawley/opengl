#include "Matrix.h"
#include "Debugger.h"
#include "MyMath.h"

const Matrix Matrix::Identity(Vector4(1, 0, 0, 0), Vector4(0, 1, 0, 0), Vector4(0, 0, 1, 0), Vector4(0, 0, 0, 1));

Matrix::Matrix(Vector4 x, Vector4 y, Vector4 z, Vector4 w)
{
	M[0][0] = x.X;
	M[0][1] = x.Y;
	M[0][2] = x.Z;
	M[0][3] = x.W;

	M[1][0] = y.X;
	M[1][1] = y.Y;
	M[1][2] = y.Z;
	M[1][3] = y.W;

	M[2][0] = z.X;
	M[2][1] = z.Y;
	M[2][2] = z.Z;
	M[2][3] = z.W;

	M[3][0] = w.X;
	M[3][1] = w.Y;
	M[3][2] = w.Z;
	M[3][3] = w.W;
}

Matrix Matrix::operator*(const Matrix& Mat) const
{
	Matrix result;
	
	result.M[0][0] = M[0][0] * Mat.M[0][0] + M[0][1] * Mat.M[1][0] + M[0][2] * Mat.M[2][0] + M[0][3] * Mat.M[3][0];
	result.M[0][1] = M[0][0] * Mat.M[0][1] + M[0][1] * Mat.M[1][1] + M[0][2] * Mat.M[2][1] + M[0][3] * Mat.M[3][1];
	result.M[0][2] = M[0][0] * Mat.M[0][2] + M[0][1] * Mat.M[1][2] + M[0][2] * Mat.M[2][2] + M[0][3] * Mat.M[3][2];
	result.M[0][3] = M[0][0] * Mat.M[0][3] + M[0][1] * Mat.M[1][3] + M[0][2] * Mat.M[2][3] + M[0][3] * Mat.M[3][3];

	result.M[1][0] = M[1][0] * Mat.M[0][0] + M[1][1] * Mat.M[1][0] + M[1][2] * Mat.M[2][0] + M[1][3] * Mat.M[3][0];
	result.M[1][1] = M[1][0] * Mat.M[0][1] + M[1][1] * Mat.M[1][1] + M[1][2] * Mat.M[2][1] + M[1][3] * Mat.M[3][1];
	result.M[1][2] = M[1][0] * Mat.M[0][2] + M[1][1] * Mat.M[1][2] + M[1][2] * Mat.M[2][2] + M[1][3] * Mat.M[3][2];
	result.M[1][3] = M[1][0] * Mat.M[0][3] + M[1][1] * Mat.M[1][3] + M[1][2] * Mat.M[2][3] + M[1][3] * Mat.M[3][3];

	result.M[2][0] = M[2][0] * Mat.M[0][0] + M[2][1] * Mat.M[1][0] + M[2][2] * Mat.M[2][0] + M[2][3] * Mat.M[3][0];
	result.M[2][1] = M[2][0] * Mat.M[0][1] + M[2][1] * Mat.M[1][1] + M[2][2] * Mat.M[2][1] + M[2][3] * Mat.M[3][1];
	result.M[2][2] = M[2][0] * Mat.M[0][2] + M[2][1] * Mat.M[1][2] + M[2][2] * Mat.M[2][2] + M[2][3] * Mat.M[3][2];
	result.M[2][3] = M[2][0] * Mat.M[0][3] + M[2][1] * Mat.M[1][3] + M[2][2] * Mat.M[2][3] + M[2][3] * Mat.M[3][3];

	result.M[3][0] = M[3][0] * Mat.M[0][0] + M[3][1] * Mat.M[1][0] + M[3][2] * Mat.M[2][0] + M[3][3] * Mat.M[3][0];
	result.M[3][1] = M[3][0] * Mat.M[0][1] + M[3][1] * Mat.M[1][1] + M[3][2] * Mat.M[2][1] + M[3][3] * Mat.M[3][1];
	result.M[3][2] = M[3][0] * Mat.M[0][2] + M[3][1] * Mat.M[1][2] + M[3][2] * Mat.M[2][2] + M[3][3] * Mat.M[3][2];
	result.M[3][3] = M[3][0] * Mat.M[0][3] + M[3][1] * Mat.M[1][3] + M[3][2] * Mat.M[2][3] + M[3][3] * Mat.M[3][3];
	
	return result;
}

Matrix& Matrix::operator*=(const Matrix& Mat)
{
	return *this = *this * Mat;
}

Matrix Matrix::operator*(float scale) const
{
	Matrix result;

	for (int i = 0; i < 4; ++i)
	{
		for (int j = 0; j < 4; ++j)
		{
			result.M[i][j] = M[i][j] * scale;
		}
	}

	return result;
}

Matrix& Matrix::operator*=(float scale)
{
	return *this = *this * scale;
}

Matrix Matrix::operator/(float scale) const
{
	Matrix result;

	float oneOverScale = 1 / scale;

	for (int i = 0; i < 4; ++i)
	{
		for (int j = 0; j < 4; ++j)
		{
			result.M[i][j] = M[i][j] * oneOverScale;
		}
	}

	return result;
}

Matrix& Matrix::operator/=(float scale)
{
	float oneOverScale = 1 / scale;

	for (int i = 0; i < 4; ++i)
	{
		for (int j = 0; j < 4; ++j)
		{
			M[i][j] *= scale;
		}
	}

	return *this;
}

Matrix Matrix::operator+(const Matrix& Mat) const
{
	Matrix result;

	for (int i = 0; i < 4; ++i)
	{
		for (int j = 0; j < 4; ++j)
		{
			result.M[i][j] = M[i][j] + Mat.M[i][j];
		}
	}

	return result;
}

Matrix Matrix::operator-(const Matrix& Mat) const
{
	Matrix result;

	for (int i = 0; i < 4; ++i)
	{
		for (int j = 0; j < 4; ++j)
		{
			result.M[i][j] = M[i][j] - Mat.M[i][j];
		}
	}	

	return result;
}

Matrix& Matrix::operator+=(const Matrix& Mat)
{
	return *this = *this + Mat;
}

Matrix& Matrix::operator-=(const Matrix& Mat)
{
	return *this = *this - Mat;
}

Matrix Matrix::GetTranspose() const
{
	Matrix result;

	result.M[0][0] = M[0][0];
	result.M[0][1] = M[1][0];
	result.M[0][2] = M[2][0];
	result.M[0][3] = M[3][0];

	result.M[1][0] = M[0][1];
	result.M[1][1] = M[1][1];
	result.M[1][2] = M[2][1];
	result.M[1][3] = M[3][1];

	result.M[2][0] = M[0][2];
	result.M[2][1] = M[1][2];
	result.M[2][2] = M[2][2];
	result.M[2][3] = M[3][2];

	result.M[3][0] = M[0][3];
	result.M[3][1] = M[1][3];
	result.M[3][2] = M[2][3];
	result.M[3][3] = M[3][3];

	return result;
}

void Matrix::PrintString() const
{
	std::cout << '{' << M[0][0] << ", " << M[0][1] << ", " << M[0][2] << ", " << M[0][3] << '}' << std::endl;
	std::cout << '{' << M[1][0] << ", " << M[1][1] << ", " << M[1][2] << ", " << M[1][3] << '}' << std::endl;
	std::cout << '{' << M[2][0] << ", " << M[2][1] << ", " << M[2][2] << ", " << M[2][3] << '}' << std::endl;
	std::cout << '{' << M[3][0] << ", " << M[3][1] << ", " << M[3][2] << ", " << M[3][3] << '}' << std::endl;
}

void Matrix::SetIdentity()
{
	M[0][0] = 1.0f;	M[1][0] = 0.0f;	M[2][0] = 0.0f;	M[3][0] = 0.0f;
	M[0][1] = 0.0f;	M[1][1] = 1.0f;	M[2][1] = 0.0f;	M[3][1] = 0.0f;
	M[0][2] = 0.0f;	M[1][2] = 0.0f;	M[2][2] = 1.0f;	M[3][2] = 0.0f;
	M[0][3] = 0.0f;	M[1][3] = 0.0f;	M[2][3] = 0.0f;	M[3][3] = 1.0f;
}

float Matrix::Determinant() const
{
	float result;

	float temp1 = M[0][0] * (M[1][1] * (M[2][2] * M[3][3] - M[2][3] * M[3][2]) + M[1][2] * (M[2][3] * M[3][1] - M[2][1] * M[3][3]) + M[1][3] * (M[2][1] * M[3][2] - M[2][2] * M[3][1]));
	float temp2 = M[0][1] * (M[1][0] * (M[2][2] * M[3][3] - M[2][3] * M[3][2]) + M[1][2] * (M[2][3] * M[3][0] - M[2][0] * M[3][3]) + M[1][3] * (M[2][0] * M[3][2] - M[2][2] * M[3][0]));
	float temp3 = M[0][2] * (M[1][0] * (M[2][1] * M[3][3] - M[2][3] * M[3][1]) + M[1][1] * (M[2][3] * M[3][0] - M[2][0] * M[3][3]) + M[1][3] * (M[2][0] * M[3][1] - M[2][1] * M[3][0]));
	float temp4 = M[0][3] * (M[1][0] * (M[2][1] * M[3][2] - M[2][2] * M[3][1]) + M[1][1] * (M[2][2] * M[3][0] - M[2][0] * M[3][2]) + M[1][2] * (M[2][0] * M[3][1] - M[2][1] * M[3][0]));

	result = temp1 - temp2 + temp3 - temp4;

	return result;
}

float Matrix::RotDeterminant() const
{	
	return M[0][0] * (M[1][1] * M[2][2] - M[1][2] * M[2][1]) - 
		M[0][1] * (M[1][0] * M[2][2] - M[1][2] * M[2][0]) + 
		M[0][2] * (M[1][0] * M[2][1] - M[1][1] * M[2][0]);
}

Matrix Matrix::Inverse() const
{
	float det = Determinant();

	if (det != 0.0f)
	{
		float oneOverDet = 1.0f / det;

		Matrix result;

		result.M[0][0] = oneOverDet * (M[1][1] * M[2][2] * M[3][3] + M[1][2] * M[2][3] * M[3][1] + M[1][3] * M[2][1] * M[3][2] - M[1][1] * M[2][3] * M[3][2] - M[1][2] * M[2][1] * M[3][3] - M[1][3] * M[2][2] * M[3][1]);
		result.M[0][1] = oneOverDet * (M[0][1] * M[2][3] * M[3][2] + M[0][2] * M[2][1] * M[3][3] + M[0][3] * M[2][2] * M[3][1] - M[0][1] * M[2][2] * M[3][3] - M[0][2] * M[2][3] * M[3][1] - M[0][3] * M[2][1] * M[3][2]); 
		result.M[0][2] = oneOverDet * (M[0][1] * M[1][2] * M[3][3] + M[0][2] * M[1][3] * M[3][1] + M[0][3] * M[1][1] * M[3][2] - M[0][1] * M[1][3] * M[3][2] - M[0][2] * M[1][1] * M[3][3] - M[0][3] * M[1][2] * M[3][1]); 
		result.M[0][3] = oneOverDet * (M[0][1] * M[2][3] * M[2][2] + M[0][2] * M[1][1] * M[2][3] + M[0][3] * M[1][2] * M[2][1] - M[0][1] * M[1][2] * M[2][3] - M[0][2] * M[1][3] * M[2][1] - M[0][3] * M[1][1] * M[2][2]);
		
		result.M[1][0] = oneOverDet * (M[1][0] * M[2][3] * M[3][2] + M[1][2] * M[2][0] * M[3][3] + M[1][3] * M[2][2] * M[3][0] - M[1][0] * M[2][2] * M[3][3] - M[1][2] * M[2][3] * M[3][0] - M[1][3] * M[2][0] * M[3][2]); 	
		result.M[1][1] = oneOverDet * (M[0][0] * M[2][2] * M[3][3] + M[0][2] * M[2][3] * M[3][0] + M[0][3] * M[2][0] * M[3][2] - M[0][0] * M[2][3] * M[3][2] - M[0][2] * M[2][0] * M[3][3] - M[0][3] * M[2][2] * M[3][0]);		
		result.M[1][2] = oneOverDet * (M[0][0] * M[1][3] * M[3][2] + M[0][2] * M[1][0] * M[3][3] + M[0][3] * M[1][2] * M[3][0] - M[0][0] * M[1][2] * M[3][3] - M[0][2] * M[1][3] * M[3][0] - M[0][3] * M[1][0] * M[3][2]);		
		result.M[1][3] = oneOverDet * (M[0][0] * M[1][2] * M[2][3] + M[0][2] * M[1][3] * M[2][0] + M[0][3] * M[1][0] * M[2][2] - M[0][0] * M[1][3] * M[2][2] - M[0][2] * M[1][0] * M[2][3] - M[0][3] * M[1][2] * M[2][0]);

		result.M[2][0] = oneOverDet * (M[1][0] * M[2][1] * M[3][3] + M[1][1] * M[2][3] * M[3][0] + M[1][3] * M[2][0] * M[3][1] - M[1][0] * M[2][3] * M[3][1] - M[1][1] * M[2][0] * M[3][3] - M[1][3] * M[2][1] * M[3][0]); 
		result.M[2][1] = oneOverDet * (M[0][0] * M[2][3] * M[3][1] + M[0][1] * M[2][0] * M[3][3] + M[0][3] * M[2][1] * M[3][0] - M[0][0] * M[2][1] * M[3][3] - M[0][1] * M[2][3] * M[3][0] - M[0][3] * M[2][0] * M[3][1]); 
		result.M[2][2] = oneOverDet * (M[0][0] * M[1][1] * M[3][3] + M[0][1] * M[1][3] * M[3][0] + M[0][3] * M[1][0] * M[3][1] - M[0][0] * M[1][3] * M[3][1] - M[0][1] * M[1][0] * M[3][3] - M[0][3] * M[1][1] * M[3][0]); 
		result.M[2][3] = oneOverDet * (M[0][0] * M[1][3] * M[2][1] + M[0][1] * M[1][0] * M[2][3] + M[0][3] * M[1][1] * M[2][0] - M[0][0] * M[1][1] * M[2][3] - M[0][1] * M[1][3] * M[2][0] - M[0][3] * M[1][0] * M[2][1]);

		result.M[3][0] = oneOverDet * (M[1][0] * M[2][2] * M[3][1] + M[1][1] * M[2][0] * M[3][2] + M[1][2] * M[2][1] * M[3][0] - M[1][0] * M[2][1] * M[3][2] - M[1][1] * M[2][2] * M[3][0] - M[1][2] * M[2][0] * M[3][1]);
		result.M[3][1] = oneOverDet * (M[0][0] * M[2][1] * M[3][2] + M[0][1] * M[2][2] * M[3][0] + M[0][2] * M[2][0] * M[3][1] - M[0][0] * M[2][2] * M[3][1] - M[0][1] * M[2][0] * M[3][2] - M[0][2] * M[2][1] * M[3][0]);
		result.M[3][2] = oneOverDet * (M[0][0] * M[1][2] * M[3][1] + M[0][1] * M[1][0] * M[3][2] + M[0][2] * M[1][1] * M[3][0] - M[0][0] * M[1][1] * M[3][2] - M[0][1] * M[1][2] * M[3][0] - M[0][2] * M[1][0] * M[3][1]); 
		result.M[3][3] = oneOverDet * (M[0][0] * M[1][1] * M[2][2] + M[0][1] * M[1][2] * M[2][0] + M[0][2] * M[1][0] * M[2][1] - M[0][0] * M[1][2] * M[2][1] - M[0][1] * M[1][0] * M[2][2] - M[0][2] * M[1][1] * M[2][0]);
		
		return result;
	}

	return Matrix::Identity;
}

bool Matrix::IsOrthogonal() const
{
	Vector3 r1(M[0][0], M[0][1], M[0][2]);
	Vector3 r2(M[1][0], M[1][1], M[1][2]);
	Vector3 r3(M[2][0], M[2][1], M[2][2]);

	float dotProduct = abs(1.0f - r1.Dot(r1));

	if (dotProduct >= 1e-4f)
	{		
		return false;
	}

	dotProduct = abs(r1.Dot(r2));

	/*if (dotProduct >= 1e-4f)
	{
		return false;
	}*/

	dotProduct = abs(r1.Dot(r3));

	/*if (dotProduct >= 1e-4f)
	{
		return false;
	}*/
	
	dotProduct = abs(1.0f - r2.Dot(r2));

	/*if (dotProduct >= 1e-4f)
	{
		return false;
	}*/

	dotProduct = abs(r2.Dot(r3));

	/*if (dotProduct >= 1e-4f)
	{
		return false;
	}*/
	
	dotProduct = abs(1.0f - r3.Dot(r3));

	/*if (dotProduct >= 1e-4f)
	{
		return false;
	}*/			
	
	return true;
}

Vector3 Matrix::XAxis() const
{
	return Vector3(M[0][0], M[0][1], M[0][2]);
}

Vector3 Matrix::YAxis() const
{
	return Vector3(M[1][0], M[1][1], M[1][2]);
}

Vector3 Matrix::ZAxis() const
{
	return Vector3(M[2][0], M[2][1], M[2][2]);
}

Vector3 Matrix::Translation() const
{
	return Vector3(M[3][0], M[3][1], M[3][2]);
}

Vector3 Matrix::GetScale() const
{	
	return Vector3(
		XAxis().Length(), 
		YAxis().Length(), 
		ZAxis().Length());
}

Vector3 Matrix::RemoveScale()
{
	Vector3 scale(1.0f, 1.0f, 1.0f);

	float length = XAxis().SquaredLength();
	
	if (abs(1.0f - length) < 0.0001f)
	{
		scale.X = sqrt(length);

		const float scaleFactor = 1.0f / scale.X;

		M[0][0] *= scaleFactor;
		M[0][1] *= scaleFactor;
		M[0][2] *= scaleFactor;
	}

	length = YAxis().SquaredLength();

	if (abs(1.0f - length) < 0.0001f)
	{
		scale.Y = sqrt(length);

		const float scaleFactor = 1.0f / scale.Y;

		M[1][0] *= scaleFactor;
		M[1][1] *= scaleFactor;
		M[1][2] *= scaleFactor;
	}
	
	length = ZAxis().SquaredLength();

	if (abs(1.0f - length) < 0.0001f)
	{
		scale.Z = sqrt(length);

		const float scaleFactor = 1.0f / scale.Z;

		M[2][0] *= scaleFactor;
		M[2][1] *= scaleFactor;
		M[2][2] *= scaleFactor;
	}

	return scale;
}

void Matrix::SetXAxis(const Vector3& axis)
{
	M[0][0] = axis.X;
	M[0][1] = axis.Y;
	M[0][2] = axis.Z;
}

void Matrix::SetYAxis(const Vector3& axis)
{
	M[1][0] = axis.X;
	M[1][1] = axis.Y;
	M[1][2] = axis.Z;
}

void Matrix::SetZAxis(const Vector3& axis)
{
	M[2][0] = axis.X;
	M[2][1] = axis.Y;
	M[2][2] = axis.Z;
}

// The decomposition of the matrix can be used to recreate the matrix, this is done using the following procedure:
//	
//	SkewAndScale matrix
// [scale.X 0        0		   0]	[1      0      0 0]  
// [0       scale.Y  0		   0] * [skew.X 1      0 0] *  RotationMatrix * TranslationMatrix
// [0	    0	     scale.Z   0]	[skew.Y skew.Z 1 0]
// [0       0        0         1]	[0      0      0 1]
//
// Note, the proper matrix concatenation would require scaleMatrix * skewMatrix * rotationMatrix * translatonMatrix
// For this to work, the skew would need to be tweaked like so: skew.X /= scale.Y, skew.Y /= scale.Z, skew.Z /= scale.Z. 
// This would allow the scaleMatrix to bring the skew back to the original value 
void Matrix::Decompose(Vector3* scale, Vector3* skew, Quaternion* rotation, Vector3* translation) const
{
	// Decomposition is done using QR decomposition. This form of decomposition would yield a Q and an R matrix that when multipled together, recreate the original matrix.
	// The QR decomposition is namely used for extracting rotation, scale and skew.
	// The Q matrix represents the combination of scale and skew:
	// [scale.X  0		  0		 ]
	// [xy skew  scale.Y  0		 ]
	// [xz skew  yz skew  scale.Z]
	//
	// The rotation is extracted from the R matrix, we treat it as a quaternion rotation matrix:
	// [R1  R2  R3]
	// [R4  R5  R6]
	// [R7  R8  R9]
	Vector3 localScale = Vector3::Zero;
	Vector3 localSkew = Vector3::Zero;
	Vector3 localTranslation = Vector3::Zero;
	Quaternion localRotation = Quaternion::Identity;
	
	Vector3 u1, u2, u3;
	Vector3 e1, e2, e3;
	Vector3 x = XAxis(), y = YAxis(), z = ZAxis();
	
	// QR decomposition involves creating an orthnormal basis to extract our rotation
	// We create the orthonormal basis using Gran Schmidt orthogonalisation
	u1 = x;
	localScale.X = u1.Length();   // The scale is the length of the vector

	if (localScale.X == 0.0f)
	{
		e1 = Vector3::Zero;
	}
	else
	{
		e1 = u1 / localScale.X;
	}
		
	u2 = y - e1 * (y.Dot(e1));
	localScale.Y = u2.Length();

	if (localScale.Y == 0.0f)
	{
		e2 = Vector3::Zero;
	}
	else
	{
		e2 = u2 / localScale.Y;
		localSkew.X = y.Dot(e1) / localScale.Y;
	}
	
	// Force the up vector to be orthogonal to the x axis
	/*if (abs(e1.Dot(e2)) >= 1e-6f)
	{
		Quaternion rightRotation = e1.ToQuaternion(AxisType::Right);
		e2 = rightRotation.Up();
	}*/
		
	u3 = z - e1 * (z.Dot(e1)) - e2 * (z.Dot(e2));
	localScale.Z = u3.Length();
	
	if (localScale.Z == 0.0f)
	{
		e3 = Vector3::Zero;
	}
	else
	{
		e3 = u3 / localScale.Z;
				
		localSkew.Y = z.Dot(e1) / localScale.Z;
		localSkew.Z = z.Dot(e2) / localScale.Z;
	}	
		
	/*if (abs(e1.Dot(e3)) > 1e-6f)
	{	
		e3 = e1.Cross(e2);		
	}*/
		
	// We check the scale instead of the axis, as if the scale is zero, then that axis is also zero
	if (localScale.X == 0.0f)
	{
		if (localScale.Y == 0.0f)
		{
			if (localScale.Z != 0.0f)
			{
				// This will happen if the matrix has a scale of 0 in the x and y axis.
				// To take a stab at solving you have to find a quaternion that rotates the forward vector to the current z axis
				// Use that quaternion to calculate a new x and y basis vectors

				Quaternion forwardRotation = e3.ToQuaternion();
				e2 = forwardRotation.Up();
				e1 = forwardRotation.Right();
			}
			// If the localScale is 0 in x,y and z we don't need to do anything, it'll magically work... the amount of rotations would be infinite if we tried to find a rotation. It'll come out as identity
		}
		// Got scale in Y, check for scale in z
		else if(localScale.Z == 0.0f)
		{
			// Got 0 scale in x and z
			// Find quaternion that rotates up to current y vector
			// Use the quaternion to find new x and z basis vectors
			
			Quaternion upRotation = e2.ToQuaternion(AxisType::Up);
			e1 = upRotation.Right();
			e3 = upRotation.Forward();
		}
		else
		{
			// We only have a scale of 0 in the x axis, need to use Y cross Z to produce the x axis vector
			e1 = e2.Cross(e3);
		}
	}
	// Never have to check x again
	else if (localScale.Y == 0.0f)
	{
		if (localScale.Z == 0.0f)
		{
			//Got a scale in the X axis, but 0 in y and z
			//Find a quaternion that rotates the right vector to the current x axis
			Quaternion upRotation = e1.ToQuaternion(AxisType::Right);
			e2 = upRotation.Up();
			e3 = upRotation.Forward();
		}
		else
		{
			//Only got 0 scale in y, cross x with z to get y
			e2 = e1.Cross(e3);
		}
	}
	// Got scale in x and y
	else if(localScale.Z == 0.0f)
	{
		//Use x cross y to get new z axis
		e3 = e1.Cross(e2);
	}

	Matrix rotationMat = Matrix(Vector4(e1), Vector4(e2), Vector4(e3), Vector4(0.0f, 0.0f, 0.0f, 1.0f));		
		
	if (rotationMat.RotDeterminant() <= -0.99f)
	{
		rotationMat.SetXAxis(-e1);
		rotationMat.SetYAxis(-e2);
		rotationMat.SetZAxis(-e3);

		localScale = -localScale;
	}

	localRotation = Quaternion(rotationMat);
	localTranslation = Translation();

	if (scale)
	{
		*scale = localScale;
	}
	
	if (skew)
	{
		*skew = localSkew;
	}
	
	if (rotation)
	{
		*rotation = localRotation;
	}
	
	if (translation)
	{
		*translation = localTranslation;
	}
}

std::ostream& operator<<(std::ostream& out, const Matrix& M)
{
	M.PrintString();

	return out;
}

bool operator==(const Matrix& matrix, const Matrix& right)
{
	for (int i = 0; i < 4; ++i)
	{
		for (int j = 0; j < 4; ++j)
		{
			if (matrix.M[i][j] != right.M[i][j])
			{
				return false;
			}
		}
	}

	return true;
}

bool operator!=(const Matrix & matrix, const Matrix & right)
{
	return !(matrix == right);
}