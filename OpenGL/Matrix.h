#pragma once

#include "Mathfwd.h"

struct Matrix
{	
public:

	static const Matrix Identity;

	float M[4][4] = { 0, 0 };

	Matrix() {}
	Matrix(Vector4 x, Vector4 y, Vector4 z, Vector4 w);
	
	Matrix operator* (const Matrix& M) const;
	Matrix& operator*= (const Matrix& M);	
	Matrix operator* (float scale) const;
	Matrix& operator*= (float scale);
	
	Matrix operator/ (float scale) const;
	Matrix& operator/= (float scale);

	Matrix operator+ (const Matrix& M) const;
	Matrix operator- (const Matrix& M) const;
	Matrix& operator+= (const Matrix& M);
	Matrix& operator-= (const Matrix& M);
		
	Matrix GetTranspose() const;

	void PrintString() const;

	void SetIdentity();
	float Determinant() const;
	float RotDeterminant() const;
	Matrix Inverse() const;
	bool IsOrthogonal() const;

	Vector3 XAxis() const;
	Vector3 YAxis() const;
	Vector3 ZAxis() const;
	Vector3 Translation() const;
	
	Vector3 GetScale() const;
	Vector3 RemoveScale();

	void SetXAxis(const Vector3& axis);
	void SetYAxis(const Vector3& axis);
	void SetZAxis(const Vector3& axis);
	void Decompose(Vector3* scale, Vector3* skew, Quaternion* rotation, Vector3* translation) const;

	friend bool operator==(const Matrix& matrix, const Matrix& right);
	friend bool operator!=(const Matrix& matrix, const Matrix& right);
};