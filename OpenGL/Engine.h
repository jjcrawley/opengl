#pragma once
#include "Singleton.h"
#include "InputManager.h"
#include "ResourceManager.h"
#include "WindowManager.h"
#include "RendererManager.h"
#include "LightingEngine.h"
#include "Array.h"
#include "GameEntity.h"

//Need to see about debugger class, will be used in place of the printf stuff

class Engine : public Singleton<Engine>
{
	ResourceManager* m_resourceManager;
	InputManager* m_inputManager;
	WindowManager* m_windowManager;
	RendererManager* m_rendererManager;
	LightingEngine* m_lightingEngine;

	float m_deltaTime = 0.0f;
	double m_startTime = 0;
	double m_currentTime = 0;
	double m_previousTime = 0;
	double m_tickFrequency = 0;

	bool b_shutdownEngine;
	
	Array<GameEntity*> m_updateEntities;

	void StartFrame();
	void EndFrame();
public:
	Engine() {}
	~Engine() {}
	bool Initialise();
	
	void Cleanup();
	void UpdateEngine();
	void Render();
	void MainLoop();
	
	void Shutdown();	

	bool ShouldShutdown();	
		
	template<typename T = GameEntity>
	T* CreateEntity()
	{
		T* entity = new T();

		m_updateEntities.Add(entity);
		entity->OnCreate();

		return entity;
	}

	void DeleteEntity(GameEntity* entity);
};