#pragma once

#include "Material.h"
#include "Singleton.h"
#include "Array.h"
#include "Light.h"

#define MAX_LIGHTS 12

class LightingEngine : public Singleton<LightingEngine>
{	
	unsigned int m_lightingBuffer;

	Array<Light*> m_activeLights;
		
	Colour m_ambientColour;
	
	__declspec(align(16)) LightingData m_lightingData[MAX_LIGHTS];	

	//bool b_lightingStateDirty;
	
public:
	
	LightingEngine() 
	{
		m_activeLights.Reserve(MAX_LIGHTS);
		//b_lightingStateDirty = true;
	}

	void SetupMaterial(Material* material);
	void UpdateLighting();
		
	void RegisterLight(Light* light)
	{
		if (light && m_activeLights.Size() < MAX_LIGHTS)
		{
			m_activeLights.Add(light);
			//b_lightingStateDirty = true;
		}
	}

	void DeRegisterLight(Light* light)
	{
		if (light)
		{
			int size = m_activeLights.Size();

			for (int i = 0; i < size; ++i)
			{
				if (m_activeLights[i] == light)
				{
					m_activeLights.RemoveAt(i, false);
					//b_lightingStateDirty = true;
					return;
				}
			}			
		}
	}

	void TidyUp();
	void Initialise();
	
	void SetGlobalAmbient(const Colour& Ambient)
	{
		m_ambientColour = Ambient;
		//b_lightingStateDirty = true;
	}

	Colour GetAmbient()
	{
		return m_ambientColour;
	}
};