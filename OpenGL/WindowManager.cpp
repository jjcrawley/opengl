#include "WindowManager.h"

#include "Engine.h"
#include "InputManager.h"

#include <GLFW/glfw3.h>
#include <stdio.h>
#include <iostream>

template<typename T> T* Singleton<T>::s_pointer = nullptr;

void glfwMouseMoveCallback(GLFWwindow* window, double x, double y)
{
	InputManager::Get().OnMouseMove(x, y);
}

void glfwKeyPressCallback(GLFWwindow* window, int key, int code, int action, int mods)
{
	InputManager::Get().OnKeyPress(key, action, mods);
}

void ErrorCallback(int error, const char* desc)
{
	fprintf(stderr, "Error: %s\n", desc);
}

void glfwScrollCallback(GLFWwindow* window, double x, double y)
{
	InputManager::Get().OnScroll(x, y);
}

void glfwWindowCloseCallback(GLFWwindow* window)
{
	Engine::Get().Shutdown();
}

void PollEvents()
{
	glfwPollEvents();
}

bool WindowManager::InitialiseWindow()
{
	WindowManager* windowManager = new WindowManager();

	glfwSetErrorCallback(ErrorCallback);

	if (!glfwInit())
	{
		fprintf(stderr, "Couldn't initialise GLFW\n");
		return false;
	}

	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);

	GLFWwindow* window = glfwCreateWindow(1024, 768, "OpenGL", NULL, NULL);

	if (window == nullptr)
	{
		fprintf(stderr, "There was an error in the glfw window, couldn't open it.");
		glfwTerminate();

		std::system("pause");

		return false;
	}

	glfwMakeContextCurrent(window);

	windowManager->m_window = window;

	glfwSetCursorPosCallback(window, glfwMouseMoveCallback);
	glfwSetKeyCallback(window, glfwKeyPressCallback);
	glfwSetScrollCallback(window, glfwScrollCallback);
	glfwSetWindowCloseCallback(window, glfwWindowCloseCallback);
	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);

	return true;
}

void WindowManager::Cleanup()
{
	glfwDestroyWindow(m_window);
}

void WindowManager::SwapBuffers()
{	
	glfwSwapBuffers(m_window);
}

int WindowManager::GetKey(int keyCode)
{
	return glfwGetKey(m_window, keyCode);
}

void WindowManager::SetMouseHidden(bool hidden)
{
	if (hidden)
	{
		glfwSetInputMode(m_window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	}
	else
	{
		glfwSetInputMode(m_window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
	}
}

void WindowManager::GetCursorPosition(double& x, double& y)
{
	glfwGetCursorPos(m_window, &x, &y);
}