#include <GL/glew.h>
#include "RenderableMesh.h"
#include "RendererManager.h"

void RenderableMesh::InitialiseComponent(GameEntity* entity)
{
	Super::InitialiseComponent(entity);

	unsigned int buffers[4];

	glGenBuffers(4, buffers);
		
	glGenVertexArrays(1, &m_vertexArrayObject);
	glBindVertexArray(m_vertexArrayObject);

	m_vertexBuffer = buffers[0];
	glBindBuffer(GL_ARRAY_BUFFER, m_vertexBuffer);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*) 0);
	glEnableVertexAttribArray(0);
	
	m_uvBuffer = buffers[1];
	glBindBuffer(GL_ARRAY_BUFFER, m_uvBuffer);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (void*) 0);
	glEnableVertexAttribArray(1);

	m_normalBuffer = buffers[2];
	glBindBuffer(GL_ARRAY_BUFFER, m_normalBuffer);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, (void*) 0);
	glEnableVertexAttribArray(2);

	m_indexBuffer = buffers[3];
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_indexBuffer);	

	m_propertyList = nullptr;
}

void RenderableMesh::SetMesh(Mesh* mesh)
{
	if (mesh == m_mesh)
	{
		return;
	}

	if (mesh == nullptr)
	{
		m_mesh = nullptr;
		SetEnabled(false);
		return;
	}

	m_mesh = mesh;

	glBindVertexArray(m_vertexArrayObject);

	glBindBuffer(GL_ARRAY_BUFFER, m_vertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Vector3) * m_mesh->Vertices().Size(), m_mesh->Vertices().GetData(), GL_STATIC_DRAW);
	
	glBindBuffer(GL_ARRAY_BUFFER, m_uvBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Vector2) * m_mesh->UVs().Size(), m_mesh->UVs().GetData(), GL_STATIC_DRAW);
	
	glBindBuffer(GL_ARRAY_BUFFER, m_normalBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Vector3) * m_mesh->Normals().Size(), m_mesh->Normals().GetData(), GL_STATIC_DRAW);
	
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_indexBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * m_mesh->Indices().Size(), m_mesh->Indices().GetData(), GL_STATIC_DRAW);
}

void RenderableMesh::SetMaterial(Material* material)
{
	if (m_material == material)
	{
		return;
	}
	else if (material == nullptr)
	{
		SetEnabled(false);
		m_material = nullptr;
		return;
	}

	m_material = material;
	//m_textureHandle = m_material->GetVariableHandle("textureSampler");
}

void RenderableMesh::Render(const Matrix& viewProjection)
{			
	Matrix world = GetWorldTransform().GetMatrix();

	Matrix MVP = world * viewProjection;
		
	RendererManager::Get().UpdateMatrixBuffer(MVP, world);
	
	/*if (m_texture != nullptr)
	{
		m_material->SetTexture(m_textureHandle, m_texture);
	}*/

	m_material->ApplyMaterial();

	if (m_propertyList)
	{
		m_propertyList->ApplyProperties();
	}
	else
	{
		m_material->GetPropertyList()->ApplyProperties();
	}

	glBindVertexArray(m_vertexArrayObject);
	glDrawElements(GL_TRIANGLES, m_mesh->Indices().Size(), GL_UNSIGNED_INT, nullptr);	
}

void RenderableMesh::Destroy()
{
	unsigned int buffers[4] = { m_vertexBuffer, m_uvBuffer, m_normalBuffer, m_indexBuffer };
	
	glDeleteBuffers(4, buffers);
	glDeleteVertexArrays(1, &m_vertexArrayObject);
	m_mesh = nullptr;
	m_material = nullptr;	
}

void RenderableMesh::OnEnable()
{
	if (m_mesh != nullptr && m_material != nullptr)
	{
		RendererManager::Get().RegisterRenderer(this);
	}
}

void RenderableMesh::OnDisable()
{
	if (m_mesh != nullptr && m_material != nullptr)
	{
		RendererManager::Get().DeregisterRenderer(this);
	}
}