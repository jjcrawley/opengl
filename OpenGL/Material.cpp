#include "Material.h"
#include "Math.h"
#include "LightingEngine.h"
#include "RendererManager.h"

void Material::PopulateParameterList(unsigned int program)
{
}

Material::~Material()
{
	if (m_program != 0)
	{
		Release();
	}
}

void Material::Initialise(unsigned int program)
{	
	if (m_program || program == -1)
	{
		return;
	}

	m_program = program;

	m_propertiesList = ResourceManager::Get().CreateResource<MaterialPropertyList>();

	m_propertiesList->InitialisePropertyList(this, m_program);

	RendererManager::Get().SetupMaterial(this);
}

unsigned int Material::Program() const
{
	return m_program;
}

void Material::SetInt(const char* var, int value)
{
	/*MaterialProperty* variable = GetVariable(var);

	if (IfCheck(variable, TEXT("The requested variable cannot be found")))
	{
		glUniform1i(variable->m_location, value);
	}*/

	//glUniform1i(glGetUniformLocation(m_program, var), value);
}

void Material::SetFloat(const char* var, float value)
{
	/*MaterialProperty* variable = GetVariable(var);

	if(IfCheck(variable, TEXT("The requested variable cannot be found")))
	{
		glUniform1f(variable->m_location, value);
	}*/
}

void Material::SetFloat(MatVariable handle, float value)
{
	/*MaterialProperty* var = GetVariable(handle);

	if (IfCheck(var, TEXT("The requested variable cannot be found")))
	{
		glUniform1f(var->m_location, value);
	}*/
}

void Material::SetBool(const char* var, bool value)
{
	/*MaterialProperty* variable = GetVariable(var);

	if (IfCheck(variable, TEXT("The requested variable cannot be found")))
	{
		glUniform1i(variable->m_location, value);
	}*/
}

void Material::SetTexture(MatVariable handle, Texture* texture)
{
	/*MaterialProperty* variable = GetVariable(handle);

	if (IfCheck(variable, TEXT("The requested variable couldn't be found")))
	{
		glBindTexture(GL_TEXTURE_2D, texture->GetTextureID());
	}*/
}

void Material::SetVector(const char* var, const Vector3& vector)
{
	/*MaterialProperty* variable = GetVariable(var);

	if (IfCheck(variable, TEXT("The requested variable couldn't be found")))
	{
		glUniform3fv(variable->m_location, 1, (float*)&vector);
	}*/
}

void Material::SetVector(MatVariable handle, const Vector3& vector)
{
	/*MaterialProperty* variable = GetVariable(handle);

	if (IfCheck(variable, TEXT("The requested variable couldn't be found")))
	{
		glUniform3fv(variable->m_location, 1, (float*) &vector);
	}*/
}

void Material::SetVector(const char* var, const Vector4& vector)
{
	/*MaterialProperty* variable = GetVariable(var);

	if (IfCheck(variable, TEXT("The requested variable couldn't be found")))
	{
		glUniform4fv(variable->m_location, 1, &vector.X);
	}*/
}

void Material::SetVector(MatVariable handle, const Vector4& vector)
{
	/*MaterialProperty* variable = GetVariable(handle);

	if (IfCheck(variable, TEXT("The requested variable couldn't be found")))
	{
		glUniform4fv(variable->m_location, 1, &vector.X);
	}*/
}

void Material::SetColour(const char* var, const Colour& colour)
{
	/*MaterialProperty* variable = GetVariable(var);

	if (IfCheck(variable, TEXT("The requested variable couldn't be found")))
	{
		glUniform3fv(variable->m_location, 1, (float*) &colour);
	}*/
}

void Material::SetColour(MatVariable handle, const Colour& colour)
{
	/*MaterialProperty* variable = GetVariable(handle);

	if (IfCheck(variable, TEXT("The requested variable couldn't be found")))
	{
		glUniform3fv(variable->m_location, 1, (float*) &colour);
	}*/
}

MatVariable Material::GetVariableHandle(const char* var) const
{	
	int size = m_properties.Size();

	for (int i = 0; i < size; ++i)
	{
		const MaterialProperty& current = m_properties[i];
		
		if (current.m_name == var)
		{
			return current.m_handle;
		}
	}

	return -1;
}

void Material::ApplyMaterial()
{
	glUseProgram(m_program);
}

void Material::Release()
{
	glDeleteProgram(m_program);
	m_program = 0;
	m_properties.Clear();
}

MaterialPropertyList* Material::GetPropertyListCopy() const
{
	return m_propertiesList->CreateCopy();
}

Material* Material::LoadMaterial(const TCHAR* vertexShader, const TCHAR* fragmentShader)
{
	Material* pointer = ResourceManager::Get().CreateResource<Material>();

	unsigned int program = ShaderHelpers::LoadShader(vertexShader, fragmentShader);

	// If there was a problem with the shader load, then we load in the defaults
	if (!program)	
	{
		program = ShaderHelpers::LoadShader(TEXT("vertexShaderDefault.vs"), TEXT("pixelShaderBasic.ps"));
	}
	
	pointer->Initialise(program);
	
	LightingEngine::Get().SetupMaterial(pointer);
	
	return pointer;
}