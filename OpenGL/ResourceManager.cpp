#include "ResourceManager.h"
#include "TextureHelpers.h"

ResourceManager* Singleton<ResourceManager>::s_pointer = nullptr;

void ResourceManager::RegisterResource(BaseResource* resource)
{	
	Assert(resource, TEXT("Trying to register a null resoure, unnecessary"));

	ResourceHandle handle;

	unsigned short index;

	if (m_freeList.IsEmpty())
	{
		m_resources.Add(resource);
		index = m_resources.Size() - 1;
	}
	else
	{
		index = m_freeList[m_freeList.Size() - 1];
		
		m_freeList.PopBack();

		m_resources[index] = resource;
	}
	
	resource->SetID(handle);
}

void ResourceManager::ReleaseResource(unsigned int id)
{
	ResourceHandle handle(id);

	unsigned short index = handle.GetIndex();

	if (m_resources.InRange(index))
	{
		BaseResource* resource = m_resources[index];

		resource->Release();
		delete resource;

		m_freeList.Add(index);
	}
}

void ResourceManager::ReleaseResource(BaseResource* resource)
{
	if (resource)
	{
		ReleaseResource(resource->GetID());
	}
}

void ResourceManager::ReleaseAll()
{
	int size = m_resources.Size();

	for (int i = 0; i < size; ++i)
	{
		BaseResource* resource = m_resources[i];

		resource->Release();
		delete resource;
	}
}