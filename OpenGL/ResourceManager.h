#pragma once
#include "Singleton.h"
#include <string>
#include "Array.h"
#include <type_traits>
#include "BaseResource.h"

class ResourceManager : public Singleton<ResourceManager>
{		
	Array<BaseResource*> m_resources;
	
	Array<int> m_freeList;

	void RegisterResource(BaseResource* resource);

public:
		
	template<typename T>
	T* GetResource(unsigned int id)
	{
		ResourceHandle handle(id);

		unsigned short index = handle.GetIndex();

		if (m_resources.InRange(index))
		{
			return m_resourceHandles[index];
		}

		return nullptr;
	}

	template<typename T>
	std::enable_if_t<std::is_base_of<BaseResource, T>::value, T*> CreateResource()
	{
		T* pointer = new T();

		RegisterResource(static_cast<BaseResource*>(pointer));

		return pointer;
	}

	void ReleaseResource(unsigned int id);
	void ReleaseResource(BaseResource* resource);

	void ReleaseAll();
};