#pragma once
#include "Sorting.h"
#include "ContainerHelpers.h"

template<int childrenCount>
class MultiHeap
{
	static_assert(childrenCount > 1, "Parents nodes require more than one child");

public:
	static inline int GetParent(int child)
	{
		return (child - 1) / childrenCount;
	}

	// Get the left most child
	static inline int GetChild(int parent)
	{
		return (parent * 2) + 1;
	}

	template<typename T, typename Predicate>
	static void SiftUp(T values[], int child, const Predicate& pred)
	{		
		int parent = MultiHeap<childrenCount>::GetParent(child);

		// Keep swapping child with parent whilst the child is greater than the parent
		while (pred(values[parent], values[child]))
		{
			Swap(values[parent], values[child]);

			child = parent;
			parent = GetParent(child);
		}
	}

	template<typename T, typename Predicate>
	void SiftDown(T values[], int parent, int count, const Predicate& pred)
	{
		int child = MultiHeap<childrenCount>::GetChild(parent);

		// While we have a left child
		while (child < count)
		{
			// Calculate the index of the right most child
			// Use a min to prevent it from going out of the range of the array
			int rightChild = FMath::Min(child + childrenCount - 1, count - 1);

			// Loop through each child and grab the largest one
			for (int currentChild = child + 1; currentChild <= rightChild; ++currentChild)
			{
				if (pred(values[child], values[currentChild]))
				{
					child = currentChild;
				}
			}

			if (pred(values[parent], values[child]))
			{
				Swap(values[child], values[parent]);
			}

			// Check the next parent
			parent = child;
			child = MultiHeap<childrenCount>::GetChild(parent);
		}
	}

	template<typename T, typename Predicate = LessThan<T>>
	static void HeapInsert(T values[], int& count, const T& value, const Predicate& pred = Predicate())
	{		
		++count;

		int childIndex = count - 1;

		Construct<T>(values + childIndex, value);
				
		MultiHeap<childrenCount>::SiftUp(values, childIndex, predicate);
	}

	template<Dereference, typename T, typename Predicate = LessThan<T>>
	static void HeapInsert(T values[], int& count, const T& value, const Predicate& pred = Predicate())
	{
		DereferencePredicate<Predicate> predicate(pred);
				
		MultiHeap<childrenCount>::HeapInsert(values, count, value, predicate);
	}

	template<typename T, typename Predicate = LessThan<T>>
	static void Heapify(T values[], int count, const Predicate& pred = Predicate())
	{		
		int parent = MultiHeap<childrenCount>::GetParent(count - 1);
				
		while (parent >= 0)
		{
			MultiHeap<childrenCount>::SiftDown(values, parent, count, predicate);
			--parent;
		}
	}

	template<Dereference, typename T, typename Predicate = LessThan<T>>
	static void Heapify(T values[], int count, const Predicate& pred = Predicate())
	{
		DereferencePredicate<Predicate> predicate(pred);

		MultiHeap<childrenCount>::Heapify(values, count, predicate);
	}

	template<typename T, typename Predicate>
	static void HeapSort(T values[], int count, const Predicate& pred)
	{	
		MultiHeap<childrenCount>::Heapify(values, count, predicate);

		while (--count > 0)
		{
			Swap(values[count], values[0]);

			MultiHeap<childrenCount>::SiftDown(values, 0, count, predicate);
		}
	}

	template<Dereference, typename T, typename Predicate = LessThan<T>>
	static void HeapSort(T values[], int count, const Predicate& pred = Predicate())
	{
		DereferencePredicate<Predicate> predicate(pred);
				
		MultiHeap<childrenCount>::HeapSort(values, count, predicate);
	}

	template<typename T>
	static void HeapSort(T values[], int count)
	{
		MultiHeap<childrenCount>::HeapSort(values, count, LessThan<T>());
	}
};

class Heap
{
public:
	static inline int GetParent(int child)
	{
		return (child - 1) / 2;
	}

	// Retrieves the left most child of a parent
	static inline int GetChild(int parent)
	{
		return (parent * 2) + 1;
	}

	template<typename T, typename Predicate>
	static void SiftUp(T values[], int child, const Predicate& pred)
	{
		int parent = Heap::GetParent(child);

		// While our parent is less than our child
		while (pred(values[parent], values[child]))
		{
			Swap(values[parent], values[child]);

			child = parent;
			parent = GetParent(child);
		}
	}
	
	template<typename T, typename Predicate>
	static void SiftDown(T values[], int parent, int count, const Predicate& pred)
	{
		int child = Heap::GetChild(parent);

		// While we have a left child
		while (child < count)
		{
			// The right child of this node is always going to be one index greater than the left
			int rightChild = child + 1;

			// If we have a right child check the right value, if greater use that index instead of the left
			if (rightChild < count && pred(values[child], values[rightChild]))
			{
				child = rightChild;
			}

			if (pred(values[parent], values[child]))
			{
				Swap(values[parent], values[child]);
			}

			parent = child;
			child = Heap::GetChild(parent);
		}
	}

	template<typename T, typename Predicate = LessThan<T>>
	static void Heapify(T values[], int count, const Predicate& pred = Predicate())
	{
		// We use -2 as it's akin to ((count - 1) - 1) / 2
		// (count - 1) gives us the end index. It then becomes (endIndex - 1) / 2, which is the formula used to grab the parent
		int parent = Heap::GetParent(count - 1);
				
		while (parent >= 0)
		{
			Heap::SiftDown(values, parent, count, pred);
			--parent;
		}
	}

	template<Dereference, typename T, typename Predicate = LessThan<T>>
	static void Heapify(T values[], int count, const Predicate& pred = Predicate())
	{
		DereferencePredicate<Predicate> predicate(pred);
		
		Heap::Heapify(values, count, predicate);
	}
		
	template<typename T, typename Predicate = LessThan<T>>
	static void InsertHeap(T values[], int& count, const T& value, const Predicate& pred = Predicate())
	{
		++count;

		int childIndex = count - 1;

		Construct<T>(values + childIndex, value);
						
		Heap::SiftUp(values, childIndex, predicate);
	}

	template<Dereference, typename T, typename Predicate = LessThan<T>>
	static void InsertHeap(T values[], int& count, const T& value, const Predicate& pred = Predicate())
	{
		DereferencePredicate<Predicate> predicate(pred);

		Heap::InsertHeap(values, count, value, predicate);
	}

	template<typename T, typename Predicate>
	static void HeapSort(T values[], int count, const Predicate& pred)
	{	
		Heap::Heapify(values, count, pred);
				
		while (--count > 0)
		{
			Swap(values[count], values[0]);

			Heap::SiftDown(values, 0, count, pred);
		}
	}

	template<Dereference, typename T, typename Predicate = LessThan<T>>
	static void HeapSort(T values[], int count, const Predicate& pred = Predicate())
	{
		DereferencePredicate<Predicate> predicate(pred);
		
		Heap::HeapSort(values, count, predicate);
	}

	template<typename T>
	static void HeapSort(T values[], int count)
	{
		Heap::HeapSort(values, count, LessThan<T>());
	}
};