#pragma once

#include "Mathfwd.h"

class MatrixFactory
{
public:
	static Matrix Translate(float x, float y, float z);
	static Matrix Translate(const Vector3& v);
	static Matrix RotateAboutAxis(const Vector3& axis, float angle);
	static Matrix Scale(Vector3 scale);
	static Matrix Scale(float x, float y, float z);
	static Matrix Scale(float scale);
	static Matrix ScaleAlongAxis(const Vector3& axis, float scale);
	static Matrix OrthographicProjection(const Vector3& axis);
	static Matrix ReflectAlongAxis(const Vector3& axis);
	static Matrix Rotation(float pitch, float heading, float roll);
	static Matrix Rotation(const ERotation& angles);
	static Matrix Rotation(const Quaternion& q);
	static Matrix PerspectiveLH(float fovX, float aspect, float near, float far);
	static Matrix PerspectiveLH(float fovX, float width, float height, float near, float far);
	static Matrix PerspectiveRH(float fovX, float width, float height, float near, float far);
	static Matrix OrthographicLH(float left, float right, float top, float bottom, float near, float far);
	static Matrix OrthographicLH(float width, float height, float near, float far);
	static Matrix OrthographicLHAspect(float width, float aspect, float near, float far);
	static Matrix OrthographicRH(float width, float height, float near, float far);
	static Matrix LookToLH(const Vector3& direction, const Vector3& up, const Vector3& position);
	static Matrix LookToRH(const Vector3& direction, const Vector3& up, const Vector3& position);
	static Matrix LookAtLH(const Vector3& target, const Vector3& up, const Vector3& position);
	static Matrix TRSMatrix(const Vector3& scale, const Quaternion& rotation, const Vector3& translation);
	static Matrix TRSkewSMatrix(const Vector3& scale, const Vector3& skew, const Quaternion& rotation, const Vector3& translation);
};