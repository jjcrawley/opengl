#pragma once
#include "Array.h"
#include "Component.h"
#include "MyMath.h"

// Properly support parenting
class SceneObject : public Component
{		
	SceneObject* m_parent;
	Array<SceneObject*> m_children;

	bool b_updateWorldTransform;
	Transform m_toWorldTransform;

	SceneObject* RemoveChildInternal(int index);

protected:
	Vector3 m_relativePosition;
	Quaternion m_relativeRotation;
	Vector3 m_relativeScale;
		
	void UpdateToWorld();
	void UpdateChildrenTransforms();
	void ConditionalUpdateToWorld();

public:
	SceneObject() : m_relativePosition(Vector3::Zero), m_relativeRotation(Quaternion::Identity), m_relativeScale(Vector3(1.0f, 1.0f, 1.0f)), b_updateWorldTransform(false)
	{}
	
	void SetRelativePosition(const Vector3& position)
	{
		m_relativePosition = position;
				
		UpdateToWorld();
	}

	Vector3 GetRelativePosition() const
	{
		return m_relativePosition;
	}

	Quaternion GetRelativeRotation() const
	{
		return m_relativeRotation;
	}

	void SetRelativeRotation(const Quaternion& rotation)
	{
		m_relativeRotation = rotation;
				
		UpdateToWorld();
	}	

	void FaceTarget(const Vector3& target)
	{
		FaceDirection(target - m_relativePosition);
	}

	void FaceDirection(const Vector3& direction)
	{
		SetRelativeRotation(direction.ToQuaternion());
	}

	void SetRelativeScale(const Vector3& scale)
	{
		m_relativeScale = scale;

		UpdateToWorld();
	}
	
	Vector3 GetRelativeScale() const
	{
		return m_relativeScale;
	}

	void SetWorldPosition(const Vector3& position);
	void SetWorldRotation(const Quaternion& rotation);
	
	const Transform& GetWorldTransform() const
	{
		return m_toWorldTransform;
	}

	Transform& GetWorldTransform()
	{
		return m_toWorldTransform;
	}

	void AddChild(SceneObject* object);	

	void RemoveChild(SceneObject* object);
	void RemoveChild(int index);	

	void OnParentChange(SceneObject* parent) {}

	SceneObject* GetChild(int index) const
	{		
		if (IfCheck(m_children.InRange(index), TEXT("Unable to retrieve child as the index is out of range")))
		{
			return m_children[index];
		}

		return nullptr;
	}

	SceneObject* GetChild(const std::string& name) const
	{
		int size = m_children.Size();

		for (int i = 0; i < size; ++i)
		{
			if (m_children[i]->SameName(name))
			{
				return m_children[i];
			}
		}

		return nullptr;
	}
};