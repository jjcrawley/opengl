#pragma once
#include "Assertions.h"

template<typename T>
class Singleton
{
	static T* s_pointer;
	
public:
	Singleton()
	{
		Assert(s_pointer == nullptr, TEXT("Tried to create another instance of a singleton"));
		s_pointer = static_cast<T*>(this);
	}

	virtual ~Singleton()
	{
		s_pointer = nullptr;
	}
	
	static T& Get()
	{
		return *s_pointer;
	}

	static T* GetPointer()
	{
		return s_pointer;
	}
};