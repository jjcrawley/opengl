#pragma once
#include "BaseResource.h"
#include <tchar.h>

enum class TextureType
{
	BMP,
	DDS,
	TGA,
	Unknown
};

class Texture : public BaseResource
{	
	static Texture* s_whiteTexture;

	unsigned int m_textureID;
	TextureType m_type;

	void Initialise(unsigned int programID, TextureType texture);

public:
	Texture() : m_textureID(0), m_type(TextureType::Unknown)
	{}

	~Texture() {}

	inline unsigned int GetTextureID() 
	{
		return m_textureID;
	}

	inline TextureType GetType()
	{
		return m_type;
	}

	virtual void Release() override;
	
	static Texture* LoadTexture(const TCHAR* filePath, TextureType type, bool flip = false);
	static Texture* WhiteTexture();
};