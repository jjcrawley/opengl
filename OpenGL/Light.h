#pragma once
#include "MyMath.h"
#include "SceneObject.h"

enum class LightType
{
	Directional,
	Point,
	SpotLight
};

__declspec(align(16)) struct LightingData
{
	Vector4 position;
	Vector4 direction;
	Colour colour;
	int type;
	float range;
	float innerSpot;
	float outerSpot;
};

class Light : public SceneObject
{
	typedef SceneObject Super;

	Colour m_lightColour;
	LightType m_type;
	float m_intensity;
	float m_range;
	float m_innerSpot;
	float m_outerSpot;

public:
	
	virtual void OnEnable() override;
	virtual void OnDisable() override;

	void SetLightColour(Colour colour)
	{
		m_lightColour = colour;
	}

	Colour GetLightColour()
	{
		return m_lightColour;
	}

	void SetLightType(LightType type)
	{
		m_type = type;
	}

	LightType GetType()
	{
		return m_type;
	}

	void SetIntensity(float intensity)
	{
		m_intensity = intensity;
	}

	float GetIntensity()
	{
		return m_intensity;
	}

	void SetRange(float range)
	{
		m_range = range;
	}

	void SetInnerSpot(float innerSpot)
	{
		m_innerSpot = innerSpot;
	}

	float GetInnerSpot()
	{
		return m_innerSpot;
	}

	void SetOuterSpot(float outerSpot)
	{
		m_outerSpot = outerSpot;
	}
		
	LightingData GetData()
	{
		LightingData data;

		Transform& worldTransform = Super::GetWorldTransform();

		data.position = Vector4(worldTransform.GetTranslation(), 1);
		data.colour = m_lightColour * m_intensity;
		data.colour.A = 1.0f;
		data.direction = Vector4(worldTransform.GetRotation().Forward());
		data.innerSpot = m_innerSpot;
		data.outerSpot = m_outerSpot;
		data.range = m_range;
		data.type = (int) m_type;

		return data;
	}
};