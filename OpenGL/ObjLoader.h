#pragma once
#include "Mesh.h"

//TODO: See about adding in base class that has some loading properties
//namely information about coordinate systems and what data to import

class ObjLoader
{
public:
	ObjLoader();
	~ObjLoader();

	static Mesh* LoadMesh(const TCHAR* fileName);
};