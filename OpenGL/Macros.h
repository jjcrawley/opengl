#pragma once

#ifdef UNICODE
#define STRING(s) L##s
#else
#define STRING(s) s
#endif

#define TEXT(s) STRING(s)