#include "Debugger.h"
#include "MyMath.h"
#include "Material.h"
#include "Array.h"
#include "MeshBuilder.h"
#include "InputManager.h"
#include "GLFWInput.h"
#include "ObjLoader.h"
#include "Texture.h"
#include "Light.h"
#include "LightingEngine.h"
#include "RenderableMesh.h"
#include "RendererManager.h"
#include "Camera.h"
#include "TestEntity.h"
#include "GameEntity.h"

#include "GL/glew.h"
#include "GLFW/glfw3.h"

#include <stdio.h>
#include <stdlib.h>
#include "Engine.h"

size_t Counter::callsToAlloc = 0;
size_t Counter::callsToDestroy = 0;

Engine* engine;

//Light* light;
//LightingEngine* lightingEngine;

DelegateHandle mouseListenerHandle;

struct MaterialProperties
{
	Vector4 Diffuse;
	Vector4 Ambient;
	Vector4 Specular;
	float Shininess;
};

void APIENTRY glDebugMessageFunction(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, const void* userParam)
{
	if (id == 131185)
	{
		return;
	}

	std::cout << "-------------------------\n";
	std::cout << "Error in OpenGL: " << id << "\n";

	switch (source)
	{
		case GL_DEBUG_SOURCE_API:
			std::cout << "Source: API";
			break;
		case GL_DEBUG_SOURCE_WINDOW_SYSTEM:
			std::cout << "Source: Windows System";
			break;
		case GL_DEBUG_SOURCE_SHADER_COMPILER:
			std::cout << "Source: Shader Compiler";
			break;
		case GL_DEBUG_SOURCE_THIRD_PARTY:
			std::cout << "Source: Third Party";
			break;
		case GL_DEBUG_SOURCE_APPLICATION:
			std::cout << "Source: Application";
			break;
		case GL_DEBUG_SOURCE_OTHER:
			std::cout << "Source: Other";
			break;
	}

	switch (type)
	{
		case GL_DEBUG_TYPE_ERROR:
			std::cout << ", Type: Error";
			break;
		case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
			std::cout << ", Type: Deprecated behaviour";
			break;
		case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
			std::cout << ", Type: Undefined behaviour";
			break;
		case GL_DEBUG_TYPE_PERFORMANCE:
			std::cout << ", Type: Performance";
			break;
		case GL_DEBUG_TYPE_PORTABILITY:
			std::cout << ", Type: Portability";
			break;
		case GL_DEBUG_TYPE_OTHER:
			std::cout << ", Type: Other";
			break;
	}

	switch (severity)
	{
		case GL_DEBUG_SEVERITY_HIGH:
			std::cout << ", Severity: High";
			break;
		case GL_DEBUG_SEVERITY_MEDIUM:
			std::cout << ", Severity: Medium";
			break;
		case GL_DEBUG_SEVERITY_LOW:
			std::cout << ", Severity: Low";
			break;
		case GL_DEBUG_SEVERITY_NOTIFICATION:
			std::cout << ", Severity: Notification";
			break;
	}	

	std::cout << "\n" << message << "\n";
}

int main(void)
{	
	if (!WindowManager::InitialiseWindow())
	{
		printf("There was an issue initialising the window.\n");

		return -1;
	}

	glewExperimental = true;

	if (glewInit() != GLEW_OK)
	{
		fprintf(stderr, "Couldn't init GLEW.");
		system("pause");
		glfwTerminate();
		return -1;
	}

	if (GLEW_ARB_debug_output)
	{
		glEnable(GL_DEBUG_OUTPUT);
		glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
		glDebugMessageCallback((GLDEBUGPROC)&glDebugMessageFunction, nullptr);
	}
		
	engine = new Engine();
			
	if (!engine->Initialise())
	{
		printf("Issue with setting up engine, aborting");
		system("pause");
		engine->Cleanup();
		glfwTerminate();
		return -1;
	}
		
	Texture* ddsTexture = Texture::LoadTexture(TEXT("Textures/uvmap.dds"), TextureType::DDS);		
	Texture* monkeyTexture = Texture::LoadTexture(TEXT("Textures/uvmap2.dds"), TextureType::DDS);

	Texture* whiteTexture = Texture::WhiteTexture();

	Material* material = Material::LoadMaterial(TEXT("vertexShader.vs"), TEXT("pixelShader.ps"));
	Material* basicMaterial = Material::LoadMaterial(TEXT("vertexShaderDefault.vs"), TEXT("pixelShaderBasic.ps"));

	material->GetPropertyList()->SetFloat("Shininess", 32.0f);
	material->GetPropertyList()->SetColour("Ambient", Colour(0.0f, 0.0f, 0.1f, 1.0f));
	material->GetPropertyList()->SetColour("Diffuse", Colour(1.0f, 1.0f, 1.0f, 1.0f));
	material->GetPropertyList()->SetColour("Specular", Colour(1.0f, 1.0f, 1.0f, 1.0f));

	//TODO: see about creating game objects, will need it to allow for future expansion
	//TODO: Keep working on resource management system
	//TODO: See about potential debugging of memory leaks, using the resource manager to output any resources that were live when the application closed
	
	//RendererManager::Get().CurrentCamera()->FaceDirection(Vector3::Forward);
		
	//material->ApplyMaterial();
		
	//MaterialProperties props;
	//props.Shininess = 32.0f;
	//props.Ambient = Vector4(1.0f, 1.0f, 1.0f, 1.0f);
	//props.Diffuse = Vector4(1.0f, 1.0f, 1.0f, 1.0f);
	//props.Specular = Vector4(1.0f, 1.0f, 1.0f, 1.0f);

	////material->SetFloat("material.Shininess", 32);
	////material->SetVector("material.Diffuse", Vector3(1.0f, 1.0f, 1.0f));
	////material->SetVector("material.Ambient", Vector3(1.0f, 1.0f, 1.0f));
	////material->SetVector("material.Specular", Vector3(1.0f, 1.0f, 1.0f));
	//
	//GLuint materialBuffer, blockIndex;
	//glGenBuffers(1, &materialBuffer);
	//glBindBuffer(GL_UNIFORM_BUFFER, materialBuffer);
	//glBufferData(GL_UNIFORM_BUFFER, 16 * 4, nullptr, GL_STATIC_DRAW);
	//	
	//blockIndex = glGetUniformBlockIndex(material->Program(), "MaterialBlock");
	//glUniformBlockBinding(material->Program(), blockIndex, 0);
	//glBindBufferBase(GL_UNIFORM_BUFFER, 0, materialBuffer);
	//
	//glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(MaterialProperties), &props.Diffuse.X);

	/*GLuint matrixBuffer;
	glGenBuffers(1, &matrixBuffer);
	glBindBuffer(GL_UNIFORM_BUFFER, matrixBuffer);
	glBufferData(GL_UNIFORM_BUFFER, sizeof(Matrix) * 2, nullptr, GL_STREAM_DRAW);
	
	blockIndex = glGetUniformBlockIndex(material->Program(), "Matrices");
	glUniformBlockBinding(material->Program(), blockIndex, 1);
	glBindBufferBase(GL_UNIFORM_BUFFER, 1, matrixBuffer);*/
	//MatVariable timerHandle = material->GetVariableHandle("timerVal");
	
	//MatVariable handle = material->GetVariableHandle("textureSampler");
	
	Mesh* sphereMesh = ObjLoader::LoadMesh(TEXT("Models/sphere.obj"));
	GameEntity* sphereEntity = new GameEntity();	
	RenderableMesh* sphereMeshRenderer = sphereEntity->AddComponent<RenderableMesh>();
	sphereEntity->SetRoot(sphereMeshRenderer);
	//sphereMeshRenderer->SetTexture(whiteTexture);
	sphereMeshRenderer->SetMesh(sphereMesh);
	sphereMeshRenderer->SetMaterial(material);
	sphereMeshRenderer->SetRelativePosition(Vector3(-2.0f, 0.0f, 0.0f));
	sphereMeshRenderer->SetEnabled(true);

	MaterialPropertyList* propertyList = material->GetPropertyListCopy();
	propertyList->SetTexture("textureSampler", whiteTexture);
	propertyList->SetColour("Diffuse", Colour(1.0f, 0.1f, 0.1f, 1.0f));
	sphereMeshRenderer->SetPropertyList(propertyList);
		
	Mesh* cubeMesh = ObjLoader::LoadMesh(TEXT("Models/cube.obj"));
	GameEntity* cubeMeshEntity = engine->CreateEntity();
	RenderableMesh* cubeMeshRenderer = cubeMeshEntity->AddComponent<RenderableMesh>();
	cubeMeshEntity->SetRoot(cubeMeshRenderer);
	//cubeMeshRenderer->SetTexture(ddsTexture);
	cubeMeshRenderer->SetMesh(cubeMesh);
	cubeMeshRenderer->SetMaterial(material);	
	cubeMeshRenderer->SetRelativePosition(Vector3(2.0f, 0.0f, 0.0f));
	cubeMeshRenderer->SetEnabled(true);
	
	propertyList = material->GetPropertyListCopy();
	propertyList->SetTexture("textureSampler", ddsTexture);
	propertyList->SetColour("Diffuse", Colour(0.2f, 0.2f, 0.2f, 1.0f));
	cubeMeshRenderer->SetPropertyList(propertyList);

	Mesh* monkeyFace = ObjLoader::LoadMesh(TEXT("Models/suzanne.obj"));
	GameEntity* monkeyEntity = engine->CreateEntity();
	RenderableMesh* monkeyRenderer = monkeyEntity->AddComponent<RenderableMesh>();
	monkeyEntity->SetRoot(monkeyRenderer);
	//monkeyRenderer->SetTexture(monkeyTexture);
	monkeyRenderer->SetMesh(monkeyFace);
	monkeyRenderer->SetMaterial(material);
	monkeyRenderer->SetRelativePosition(Vector3(0.0f, 0.0f, 5.0f));
	monkeyRenderer->SetEnabled(true);

	propertyList = material->GetPropertyListCopy();
	propertyList->SetTexture("textureSampler", monkeyTexture);
	monkeyRenderer->SetPropertyList(propertyList);

	TestEntity* entity = engine->CreateEntity<TestEntity>();
	RenderableMesh* meshRenderer = entity->AddComponent<RenderableMesh>();
	//meshRenderer->SetTexture(whiteTexture);
	meshRenderer->SetMesh(sphereMesh);
	meshRenderer->SetRelativeScale(Vector3(0.2f, 0.2f, 0.2f));
	meshRenderer->SetMaterial(basicMaterial);
	Light* light = entity->GetComponent<Light>(0);
	light->AddChild(meshRenderer);
	meshRenderer->SetEnabled(true);
	
	while (!engine->ShouldShutdown())
	{		
		engine->MainLoop();		
	}
	
	//glDeleteBuffers(1, &materialBuffer);

	engine->Cleanup();	
		
	glfwTerminate();

	//printf("Number of calls to allocate: %d\nNumber of calls to deallocate: %d\n", Counter::callsToAlloc, Counter::callsToDestroy);

	//system("pause");

	return 0;
}