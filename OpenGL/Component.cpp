#include "Component.h"
#include "GameEntity.h"

void Component::InitialiseComponent(GameEntity* entity)
{
	m_owner = entity;
}

void Component::Destroy()
{
	m_owner = nullptr;
}