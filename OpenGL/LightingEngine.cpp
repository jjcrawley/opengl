#include "LightingEngine.h"
#include <GL/glew.h>

LightingEngine* Singleton<LightingEngine>::s_pointer = nullptr;

void LightingEngine::SetupMaterial(Material* material)
{
	GLuint blockIndex = glGetUniformBlockIndex(material->Program(), "LightBlock");

	if (blockIndex != GL_INVALID_INDEX)
	{
		material->ApplyMaterial();
		glBindBuffer(GL_UNIFORM_BUFFER, m_lightingBuffer);
		glUniformBlockBinding(material->Program(), blockIndex, 2);
		glBindBufferBase(GL_UNIFORM_BUFFER, 2, m_lightingBuffer);		
	}
}

void LightingEngine::UpdateLighting()
{
	glBindBuffer(GL_UNIFORM_BUFFER, m_lightingBuffer);

	glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(Colour), &m_ambientColour);

	int size = m_activeLights.Size();

	for (int i = 0; i < size; ++i)
	{
		Light* current = m_activeLights[i];

		m_lightingData[i] = current->GetData();
	}

	glBufferSubData(GL_UNIFORM_BUFFER, sizeof(Colour), sizeof(int), &size);

	if (size)
	{
		glBufferSubData(GL_UNIFORM_BUFFER, sizeof(Vector4) * 2, sizeof(LightingData) * size, m_lightingData);
	}
}

void LightingEngine::TidyUp()
{	
	m_activeLights.Clear();

	glDeleteBuffers(1, &m_lightingBuffer);
}

void LightingEngine::Initialise()
{
	glGenBuffers(1, &m_lightingBuffer);

	glGenBuffers(1, &m_lightingBuffer);
	glBindBuffer(GL_UNIFORM_BUFFER, m_lightingBuffer);
	glBufferData(GL_UNIFORM_BUFFER, sizeof(Vector4) * 2 + sizeof(m_lightingData), nullptr, GL_DYNAMIC_DRAW);	

	SetGlobalAmbient(Colour(0.005f, 0.005f, 0.005f, 1.0f));
}