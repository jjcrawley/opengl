#pragma once
#include <string>
#include "Assertions.h"

class Object
{
protected:
	std::string m_name;

public:
	Object();
	virtual ~Object();
		
	bool SameName(const std::string& name)
	{
		return m_name == name;
	}

	void SetName(const std::string& name)
	{
		m_name = name;
	}

	std::string GetName()
	{
		return m_name;
	}

	virtual void Destroy() {}
};