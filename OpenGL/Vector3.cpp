#include "MyMath.h"
#include "Debugger.h"
#include <math.h>

const Vector3 Vector3::Zero(0.0f, 0.0f, 0.0f);
const Vector3 Vector3::OneVector(1.0f, 1.0f, 1.0f);
const Vector3 Vector3::Up(0.0f, 1.0f, 0.0f);
const Vector3 Vector3::Down(0.0f, -1.0f, 0.0f);
const Vector3 Vector3::Forward(0.0f, 0.0f, 1.0f);
const Vector3 Vector3::Back(0.0f, 0.0f, -1.0f);
const Vector3 Vector3::Right(1.0f, 0.0f, 0.0f);
const Vector3 Vector3::Left(-1.0f, 0.0f, 0.0f);

void Vector3::Set(float x, float y, float z)
{
	X = x;
	Y = y;
	Z = z;
}

Vector3 Vector3::Normalise() const
{
	float squareSum = SquaredLength();

	if (abs(1.0f - squareSum) < 1e-4f)
	{
		return *this;
	}
	else if (squareSum < 1e-8f)
	{
		return Vector3::Zero;
	}
		
	const float oneOverLength = 1.0f / sqrt(squareSum);
		
	return Vector3(X * oneOverLength, Y * oneOverLength, Z * oneOverLength);
}

bool Vector3::IsNormalised() const
{	
	return abs(1.0f - SquaredLength()) < 0.001f;
}

bool Vector3::IsZero() const
{
	return X == 0.0f && Y == 0.0f && Z == 0.0f;
}

bool Vector3::IsUniform() const
{
	return (X == Y) && (X == Z) && (Y == Z);
}

float Vector3::Length() const
{
	return sqrtf(SquaredLength());
}

float Vector3::SquaredLength() const
{
	return X * X + Y * Y + Z * Z;
}

float Vector3::Dot(const Vector3& right) const
{
	return X * right.X + Y * right.Y + Z * right.Z;
}

Vector3 Vector3::ProjectOnto(const Vector3& v) const
{	
	Assert(v.IsZero(), TEXT("v is a zero vector, which should never happen"));

	// Alternative way to calculate: v.Normalise() * v.Normalise().Dot(*this)
		
	return v * ((*this).Dot(v) / v.Dot(v));
}

Vector3 Vector3::Perpendicular(const Vector3& right) const
{	
	return *this - ProjectOnto(right);
}

Vector3 Vector3::Cross(const Vector3& crossWith) const
{
	return Vector3(Y * crossWith.Z - Z * crossWith.Y, 
					Z * crossWith.X - X * crossWith.Z,
					X * crossWith.Y - Y * crossWith.X);
}

Vector3 Vector3::operator+(const Vector3& right) const
{	
	return Vector3(X + right.X, Y + right.Y, Z + right.Z);
}

Vector3 Vector3::operator-(const Vector3& right) const
{
	return Vector3(X - right.X, Y - right.Y, Z - right.Z);
}

Vector3& Vector3::operator+=(const Vector3& right)
{	
	X += right.X;
	Y += right.Y;
	Z += right.Z;

	return *this;
}

Vector3& Vector3::operator-=(const Vector3& right)
{
	X -= right.X;
	Y -= right.Y;
	Z -= right.Z;

	return *this;
}

Vector3 Vector3::operator*(const Vector3& r) const
{
	return Vector3(X * r.X, Y * r.Y, Z * r.Z);
}

Vector3 Vector3::operator/(const Vector3& r) const
{
	return Vector3(X / r.X, Y / r.Y, Z / r.Z);
}

Vector3& Vector3::operator*=(const float scale)
{
	X *= scale;
	Y *= scale;
	Z *= scale;

	return *this;
}

Vector3& Vector3::operator/=(const float scale)
{
	const float oneOverScale = 1.0f / scale;

	X *= oneOverScale;
	Y *= oneOverScale;
	Z *= oneOverScale;

	return *this;
}

inline Vector3 Vector3::operator*(const float scale) const
{
	return Vector3(X * scale, Y * scale, Z * scale);
}

Vector3 Vector3::operator/(const float scale) const
{
	const float oneOverScale = 1.0f / scale;

	return Vector3(X * oneOverScale, Y * oneOverScale, Z * oneOverScale);
}

Vector3 Vector3::operator-() const
{
	return Vector3(-X, -Y, -Z);
}

ERotation Vector3::ToRotation() const
{
	ERotation rotation;

	FMath::CartesianToSphericalUnit(*this, rotation.Heading, rotation.Pitch);
	rotation.Roll = 0;

	return rotation;
}

// This method will do quaternion math treating this vector as the forward vector
Quaternion Vector3::ToQuaternion() const
{
	float heading, pitch;

	FMath::CartesianToSphericalUnit(*this, heading, pitch);

	const float cp = cos(pitch * 0.5f);
	const float sp = sin(pitch * 0.5f);

	const float ch = cos(heading * 0.5f);
	const float sh = sin(heading * 0.5f);
	
	return Quaternion(
		ch * cp,
		ch * sp,
		sh * cp,
		-sh * sp		
		);
}

// This version of ToQuaternion will treat this vector as a vector representing the axis passed in.
// A quaternion will then be created that represents the rotation.
// Important note: You do lose one degree of freedom regardless of the axis you use.
// When used as a forward vector you lose roll, for an up vector you'll lose heading and for a right vector you'll lose pitch
Quaternion Vector3::ToQuaternion(AxisType axis) const
{
	Quaternion result(Quaternion::Identity);

	float heading, pitch, roll;

	float cr, ch, sr, sh, sp, cp;

	switch (axis)
	{
		case AxisType::Right:
						
			roll = asin(Y);

			heading = abs(roll) < HALF_PI ? atan2(-Z, X) : 0;

			cr = cos(roll * 0.5f);
			sr = sin(roll * 0.5f);

			ch = cos(heading * 0.5f);
			sh = sin(heading * 0.5f);

			result = Quaternion(
				ch * cr,
				sh * sr,
				sh * cr,
				ch * sr
				);
			
			break;
		case AxisType::Up:
						
			roll = asin(-X);

			pitch = abs(roll) < HALF_PI ? atan2(Z, Y) : 0;

			cr = cos(roll * 0.5f);
			sr = sin(roll * 0.5f);

			cp = cos(pitch * 0.5f);
			sp = sin(pitch * 0.5f);

			result = Quaternion(
				cp * cr,
				sp * cr,
				-sp * sr,
				cp * sr
				);

			break;
		case AxisType::Forward:
			result = ToQuaternion();
			break;
	}

	return result;
}

Vector3 Vector3::RotateVector(const Vector3& axis, float angle, const Vector3& v)
{
	// Equation for this is:
	// cos(angle) * (v - (v.Dot(nAxis) * nAxis) + sin(angle) * (nAxis.Cross(v)) + (v.Dot(nAxis) * nAxis)

	Vector3 result;
	Vector3 nAxis = axis.Normalise();
	
	Vector3 vProjected = v.ProjectOnto(axis);
		
	result = (v - vProjected) * cos(angle) + (nAxis.Cross(v)) * sin(angle) + vProjected;
	
	return result;
}

Vector3 Vector3::Reflect(const Vector3& v, const Vector3& normal)
{
	Vector3 norm = normal.Normalise();
	
	return v - norm * 2.0f * v.Dot(norm);
}

std::ostream& operator<<(std::ostream& out, const Vector3& v)
{
	return out << "[" << v.X << ", " << v.Y << ", " << v.Z << "]";
}