#pragma once
#include "FMath.h"
#include "AlgorithmHelpers.h"

namespace Sort
{
	template<typename T, typename Predicate = LessThan<T>>
	bool IsSorted(T values[], int count, const Predicate& pred = Predicate())
	{
		for (int i = 1; i < count; ++i)
		{
			if (pred(values[i], values[i - 1]))
			{
				return false;
			}
		}

		return true;
	}

	template<typename T, typename Predicate>
	void InsertionSort(T values[], int count, const Predicate& pred)
	{
		// Loop until we reach the end of array
		for (int i = 0; i < count; ++i)
		{
			T current = values[i];

			int j = i - 1;

			// Loop backwards from the current position
			// While looping backwards, move each visited element forward by 1 index until we find the correct position for the current value
			while (j >= 0 && pred(current, values[j]))
			{
				values[j + 1] = values[j];
				--j;
			}

			// Slot the value into its correct position
			values[j + 1] = current;
		}
	}

	template<Dereference, typename T, typename Predicate = LessThan<T>>
	void InsertionSort(T values[], int count, const Predicate& pred = Predicate())
	{
		DereferencePredicate<Predicate> predicate(pred);
				
		Sort::InsertionSort(values, count, predicate);
	}
		
	template<typename T>
	void InsertionSort(T values[], int count)
	{
		Sort::InsertionSort(values, count, LessThan<T>());
	}

	template<typename T, typename Predicate>
	void SelectionSort(T values[], int count, const Predicate& pred)
	{		
		for (int i = 0; i < count; ++i)
		{
			int smallest = i;

			// Loop over the non-sorted part of the array to find the index of the smallest value
			for (int j = i + 1; j < count; ++j)
			{
				if (pred(values[j], values[smallest]))
				{
					smallest = j;
				}
			}

			// Swap our smallest value with the current index in the array
			Swap(values[i], values[smallest]);
		}
	}

	template<Dereference, typename T, typename Predicate = LessThan<T>>
	void SelectionSort(T values[], int count, const Predicate& pred = Predicate())
	{
		DereferencePredicate<Predicate> predicate(pred);

		Sort::SelectionSort(values, count, predicate);
	}
		
	template<typename T>
	void SelectionSort(T values[], int count)
	{
		Sort::SelectionSort(values, count, LessThan<T>());
	}

	template<typename T, typename Predicate>
	void BubbleSort(T values[], int count, const Predicate& pred)
	{		
		// Modified version of bubble sort, reduces the number of swaps that take place
		for (int iteration = 0; iteration < count; ++iteration)
		{
			// the value to swap
			T toSwap = values[0];

			int j = 1;

			// Loop till we hit our (size - CurrentIteration)
			while (j < count - iteration)
			{
				// If our integer is less than our current value, we move the integer back one
				if (pred(values[j], toSwap))
				{
					values[j - 1] = values[j];
				}
				else
				{
					// Otherwise, our current value is placed at our previous index and we store the current value
					values[j - 1] = toSwap;
					toSwap = values[j];
				}

				++j;
			}

			// Place our current value in the correct position
			values[j - 1] = toSwap;
		}
	}

	template<Dereference, typename T, typename Predicate = LessThan<T>>
	void BubbleSort(T values[], int count, const Predicate& pred = Predicate())
	{
		DereferencePredicate<Predicate> predicate(pred);

		Sort::BubbleSort(values, count, predicate);
	}

	template<typename T>
	void BubbleSort(T values[], int count)
	{
		Sort::BubbleSort(values, count, LessThan<T>());
	}

	template<typename T, typename Predicate>
	int HoarePartition(int start, int end, T values[], const Predicate& pred)
	{		
		// Select the pivot
		T pivot = values[start];

		int lower = start - 1;
		int higher = end + 1;

		// Keep looping until we break out of the loop ourselves
		while (true)
		{
			// Checks if the lower value is less than the pivot, it is left alone if it is
			while (pred(values[++lower], pivot))
			{}

			// Checks whether the higher value is greater than the pivot, left alone if it is
			while (pred(pivot, values[--higher]))
			{}

			// if our higher and lower invert or overlap, we can leave
			if (lower >= higher)
			{
				return higher;
			}

			// Swap our higher and lower value
			Swap(values[lower], values[higher]);
		}
	}	

	template<typename T, typename Predicate>
	void QuickSort(int start, int end, T values[], const Predicate& pred)
	{
		int* partitionStack = new int[end - start + 1];

		int stackIndex = -1;

		partitionStack[++stackIndex] = start;
		partitionStack[++stackIndex] = end;

		while (stackIndex > 0)
		{
			end = partitionStack[stackIndex--];
			start = partitionStack[stackIndex--];

			int leftEnd = HoarePartition(start, end, values, pred);

			// Push the right partition if we have more than one element in it
			if (leftEnd + 1 < end)
			{
				partitionStack[++stackIndex] = leftEnd + 1;
				partitionStack[++stackIndex] = end;
			}

			// Push the left partition if we have more than one element in it
			if (start < leftEnd)
			{
				partitionStack[++stackIndex] = start;
				partitionStack[++stackIndex] = leftEnd;
			}
		}

		delete[] partitionStack;
	}
	
	template<typename T, typename Predicate>
	void QuickSort(T start[], int count, const Predicate& pred)
	{
		if (count == 0)
		{
			return;
		}
				
		Sort::QuickSort(0, count - 1, start, pred);
	}

	template<Dereference, typename T, typename Predicate = LessThan<T>>
	void QuickSort(T start[], int count, const Predicate& pred = Predicate())
	{
		DereferencePredicate<Predicate> predicate(pred);
				
		Sort::QuickSort(start, count, predicate);
	}

	template<typename T>
	void QuickSort(T start[], int count)
	{
		Sort::QuickSort(start, count, LessThan<T>());
	}

	template<typename T, typename Predicate>
	void MergeInPlace(int start, int mid, int end, T values[], const Predicate& pred)
	{		
		int leftPos = start;
		int leftEnd = mid;

		int rightPos = mid + 1;

		// Keep looping while both partitions have elements to examine
		while (leftPos <= leftEnd && rightPos <= end)
		{
			if (pred(values[rightPos], values[leftPos]))
			{
				// We need to move the value from the right partition into its correct place	

				// We first store the rightValue that we are slotting in, we do this to prevent the shuffle from destroying it
				// We increment rightPos to move the starting position of the right partition forward, as we've essentially removed it from the right partition			
				T rightValue = values[rightPos++];

				int j = leftEnd;

				// Shuffle all elements in the left partition to the right by 1
				while (j >= leftPos)
				{
					values[j + 1] = values[j];
					--j;
				}

				// We slot the right value into its correct place, at the old left start
				values[leftPos] = rightValue;

				// Alternatively we perform a left rotation of -1 on the values from leftPos to rightPos + 1
				//RotateArray(values + leftPos, rightPos - leftPos + 1, -1);
				//++rightPos;

				// We then move the left partition indices forward to reflect the change			
				++leftPos;
				++leftEnd;
			}
			else
			{
				++leftPos;
			}
		}

		// Once one of the partitions becomes empty the elements are sorted
	}

	template<typename T, typename Predicate>
	void MergeSort(int start, int end, T values[], const Predicate& pred)
	{
		int size = end - start;

		int mid;
		int partitionEnd;

		// The bottom-up approach involves treating the incoming array as a group of subsets of one element, then combining those subsets together until the array is sorted
		// The partition size starts out as 1 then is multiplied by 2 after each iteration. The sequence will be: 1, 2, 4, 8, 16, etc.
		for (int partitionSize = 1; partitionSize < size; partitionSize *= 2)
		{
			// Our current partition starting index, we combine 2 partitions into one with each iteration
			for (int partition = start; partition < end; partition += 2 * partitionSize)
			{
				// We grab the back of the left partition, aka mid.
				mid = partition + partitionSize - 1;

				// Also grab the end of the right partition, min prevents it from going out of range
				partitionEnd = FMath::Min(mid + partitionSize, end);

				// Combine the left partition with the right partition
				// Left partition is from partition -> mid and right partition is from mid+1 -> partitionEnd
				Sort::MergeInPlace(partition, mid, partitionEnd, values, pred);
			}
		}
	}

	template<Dereference, typename T, typename Predicate = LessThan<T>>
	void MergeSort(T values[], int count, const Predicate& pred = Predicate())
	{
		DereferencePredicate<Predicate> predicate(pred);

		Sort::MergeSort(0, count - 1, values, predicate);
	}

	template<typename T>
	void MergeSort(T values[], int count)
	{
		Sort::MergeSort(0, count - 1, values, LessThan<T>());
	}

	template<typename T, typename Predicate>
	void ShellSort(T values[], int count, const Predicate& pred)
	{
		// Shell sort is a derivation of insertion sort.
		// It uses a gap value to create a subset of the data and performs insertion sort on that data	
		// For example, array {2, 3, 6, 1, 4, 9} with a gap value of 2 will have the subset: {2, 6, 4}, we then perform insertion sort on the subset

		// Here, we get the largest gap value and work backwards, reducing the size of the gap with each iteration
		//for (int gap = count - 1; gap > 0; gap /= 2)

		// Alternatively, we can have a smaller gap and increase it, in this case we use a formula from Wikipedia: 2 ^ k + 1
		// We don't need to remember k as we can infer it from the starting gap value, 3.
		// The original sequence starts with 1 but that would be a regular insertion sort, defeating the purpose of using shell sort 
		int gap = 3;
				
		while (gap < count)
		{
			// Check each value that is part of this set, ie: all values that are gap indexes away from one another
			for (int i = gap; i < count; i += gap)
			{
				T current = values[i];

				int j = i - gap;

				// Shuffle the elements backwards in the subset
				while (j >= 0 && pred(current, values[j]))
				{
					values[j + gap] = values[j];
					j -= gap;
				}

				// Slot in the value to its correct position
				values[j + gap] = current;
			}

			// Calculate the new gap value, this math means we don't have to remember k
			// This should be commented out if we want to start with the largest gap value
			gap = (gap - 1) * 2 + 1;
		}

		Sort::InsertionSort(values, count, pred);
	}

	template<Dereference, typename T, typename Predicate = LessThan<T>>
	void ShellSort(T values[], int count, const Predicate& pred = Predicate())
	{
		DereferencePredicate<Predicate> predicate(pred);

		Sort::ShellSort(values, count, predicate);
	}

	template<typename T>
	void ShellSort(T values[], int count)
	{
		Sort::ShellSort(values, count, LessThan<T>());
	}

	template<typename T, typename Predicate>
	void CombSort(T values[], int count, const Predicate& pred)
	{
		int gap = count - 1;

		bool swapped = false;
				
		// We keep looping if we have a gap > 1 or we made a swap
		// With a gap size of 1 it's just a bubble sort
		while (gap > 1 || swapped)
		{
			// Same as dividing by 1.3, but keeps it as an int
			gap = (gap * 10) / 13;

			gap = FMath::Max(gap, 1);

			swapped = false;

			for (int i = gap; i < count; ++i)
			{
				if (pred(values[i], values[i - gap]))
				{
					Swap(values[i - gap], values[i]);
					swapped = true;
				}
			}
		}
	}

	template<Dereference, typename T, typename Predicate = LessThan<T>>
	void CombSort(T values[], int count, const Predicate& pred = Predicate())
	{
		DereferencePredicate<Predicate> predicate(pred);

		Sort::CombSort(values, count, predicate);
	}

	template<typename T>
	void CombSort(T values[], int count)
	{
		Sort::CombSort(values, count, LessThan<T>());
	}

	template<typename T, typename Predicate>
	void IntroSort(T values[], int start, int end, const Predicate& pred, int depth)
	{
		int count = end - start + 1;

		if (count < 16)
		{
			Sort::InsertionSort(&values[start], count, pred);
			return;
		}
		
		if (depth == 0)
		{
			Heap::HeapSort(values + start, count, pred);
			return;
		}

		int pivot = Sort::HoarePartition(start, end, values, pred);

		Sort::IntroSort(values, start, pivot, pred, depth - 1);
		Sort::IntroSort(values, pivot + 1, end, pred, depth - 1);
	}

	template<typename T, typename Predicate>
	void IntroSort(T values[], int count, const Predicate& pred)
	{		
		int maxDepth = (int) FMath::Log((float)count) * 2;

		IntroSort(values, 0, count - 1, pred, maxDepth);
	}

	template<Dereference, typename T, typename Predicate>
	void IntroSort(T values[], int count, const Predicate& pred)
	{
		DereferencePredicate<Predicate> predicate(pred);

		IntroSort(values, count, predicate);
	}
};