#include "MaterialPropertyList.h"
#include "GL/glew.h"
#include "ResourceManager.h"

void MaterialPropertyList::InitialisePropertyList(Material* owner, unsigned int program)
{
	if (m_owningMaterial)
	{
		return;
	}

	m_owningMaterial = owner;

	char name[256];

	int uniformCount = 0;

	//const unsigned int materialBlockIndex = glGetProgramResourceIndex(program, GL_UNIFORM_BLOCK, "MaterialBlock");

	glGetProgramInterfaceiv(program, GL_UNIFORM, GL_ACTIVE_RESOURCES, &uniformCount);

	const GLenum uniformDetails[] = { GL_BLOCK_INDEX, GL_TYPE, GL_NAME_LENGTH, GL_LOCATION };
	const GLenum materialBlockUniformDetails[] = {GL_ACTIVE_VARIABLES};
	GLint uniformValues[4];
	
	for (int i = 0; i < uniformCount; ++i)
	{
		glGetProgramResourceiv(program, GL_UNIFORM, i, 4, uniformDetails, 4, nullptr, uniformValues);

		// Retrieve information about a uniform if it is in the material block, or if it is outside of any uniform block 
		if (uniformValues[0] != -1)
		{
			continue;
		}

		union
		{
			struct
			{
				unsigned short Index;
				unsigned short Location;
			};

			unsigned int ID;
		};

		BaseMaterialProperty* newProperty;

		switch (uniformValues[1])
		{
			case GL_FLOAT:
			{
				FloatProperty floatProperty;

				m_floatProperties.Add(floatProperty);
				Index = (unsigned short) m_floatProperties.Size();

				newProperty = &m_floatProperties.Back();
			}
			break;
			case GL_INT:
			{
				IntProperty intProperty;

				m_intProperties.Add(intProperty);
				Index = (unsigned short) m_intProperties.Size();

				newProperty = &m_intProperties.Back();
			}
			break;
			case GL_FLOAT_VEC3:
			{
				Vector3Property vector3Property;

				m_vector3Properties.Add(vector3Property);
				Index = (unsigned short) m_vector3Properties.Size();

				newProperty = &m_vector3Properties.Back();
			}
			break;
			case GL_FLOAT_VEC4:
			{
				VectorProperty vector4Property;

				m_vectorProperties.Add(vector4Property);
				Index = (unsigned short) m_vectorProperties.Size();

				newProperty = &m_vectorProperties.Back();
			}
			break;
			case GL_SAMPLER_2D:
			{
				TextureProperty textureProperty;

				m_textureProperties.Add(textureProperty);
				Index = (unsigned short) m_textureProperties.Size();

				newProperty = &m_textureProperties.Back();
			}
			break;
			default:
				continue;
		}

		newProperty->Location = uniformValues[3];

		newProperty->Name.reserve(uniformValues[2]);
		glGetProgramResourceName(program, GL_UNIFORM, i, uniformValues[2], nullptr, name);

		newProperty->Name = name;

		Location = (unsigned short) newProperty->Location;

		newProperty->PropHandle = ID;
	}
}

MatVariable MaterialPropertyList::GetVariableHandleDefault(const char* name) const
{
	int size = m_vectorProperties.Size();

	int i;

	for (i = 0; i < size; ++i)
	{
		if (m_vectorProperties[i].Name == name)
		{
			return m_vectorProperties[i].PropHandle;
		}
	}

	size = m_textureProperties.Size();

	for (i = 0; i < size; ++i)
	{
		if (m_textureProperties[i].Name == name)
		{
			return m_textureProperties[i].PropHandle;
		}
	}

	size = m_floatProperties.Size();

	for (i = 0; i < size; ++i)
	{
		if (m_floatProperties[i].Name == name)
		{
			return m_floatProperties[i].PropHandle;
		}
	}

	size = m_vectorProperties.Size();

	for (i = 0; i < size; ++i)
	{
		if (m_vector3Properties[i].Name == name)
		{
			return m_vector3Properties[i].PropHandle;
		}
	}

	size = m_intProperties.Size();

	for (i = 0; i < size; ++i)
	{
		if (m_intProperties[i].Name == name)
		{
			return m_intProperties[i].PropHandle;
		}
	}

	return -1;
}

MatVariable MaterialPropertyList::GetVariableHandle(const char* var, PropertyType hint) const
{
	if (hint == PropertyType::Unknown)
	{
		return GetVariableHandleDefault(var);
	}

	int size;
	int i;

	switch (hint)
	{
		case PropertyType::Float:
			size = m_floatProperties.Size();

			for (i = 0; i < size; ++i)
			{
				if (m_floatProperties[i].Name == var)
				{
					return m_floatProperties[i].PropHandle;
				}
			}
			break;
		case PropertyType::Int:
			size = m_intProperties.Size();

			for (i = 0; i < size; ++i)
			{
				if (m_intProperties[i].Name == var)
				{
					return m_intProperties[i].PropHandle;
				}
			}
			break;
		case PropertyType::Texture:
			size = m_textureProperties.Size();

			for (i = 0; i < size; ++i)
			{
				if (m_textureProperties[i].Name == var)
				{
					return m_textureProperties[i].PropHandle;
				}
			}
			break;
		case PropertyType::Vector:
			size = m_vectorProperties.Size();

			for (i = 0; i < size; ++i)
			{
				if (m_vectorProperties[i].Name == var)
				{
					return m_vectorProperties[i].PropHandle;
				}
			}
			break;
		case PropertyType::Vector3:
			size = m_vector3Properties.Size();

			for (i = 0; i < size; ++i)
			{
				if (m_vector3Properties[i].Name == var)
				{
					return m_vector3Properties[i].PropHandle;
				}
			}
			break;
		case PropertyType::Unknown:
			break;
	}

	return 0;
}

void MaterialPropertyList::ApplyProperties()
{
	int size;

	size = m_vector3Properties.Size();

	// Update all the vector3 properties of the material
	while (size--)
	{
		Vector3Property* current = &m_vector3Properties[size];

		glUniform3fv(current->Location, 1, &current->Value.X);
	}

	size = m_vectorProperties.Size();

	// Update all the vector4 uniforms
	while (size--)
	{
		VectorProperty* current = &m_vectorProperties[size];

		glUniform4fv(current->Location, 1, &current->Value.X);
	}

	size = m_intProperties.Size();

	// Update all the int properties
	while (size--)
	{
		IntProperty* current = &m_intProperties[size];

		glUniform1i(current->Location, current->Value);
	}

	size = m_floatProperties.Size();

	// Update all float properties
	while (size--)
	{
		FloatProperty* current = &m_floatProperties[size];

		glUniform1f(current->Location, current->Value);
	}

	size = m_textureProperties.Size();

	int currentTexture = 0;

	// Update all the texture properties of the material
	// Needs to be handled slightly differently to regular uniforms, as the location is not a texture but a sampler.
	for (int i = 0; i < size; ++i)
	{
		glActiveTexture(GL_TEXTURE0 + currentTexture);
		
		if (m_textureProperties[i].Texture)
		{
			glBindTexture(GL_TEXTURE_2D, m_textureProperties[i].Texture->GetTextureID());

			glUniform1i(m_textureProperties[i].Location, currentTexture++);
		}
	}
}

MaterialPropertyList* MaterialPropertyList::CreateCopy() const
{
	MaterialPropertyList* copy = ResourceManager::Get().CreateResource<MaterialPropertyList>();

	copy->m_floatProperties = m_floatProperties;
	copy->m_intProperties = m_intProperties;
	copy->m_textureProperties = m_textureProperties;
	copy->m_vector3Properties = m_vector3Properties;
	copy->m_vectorProperties = m_vectorProperties;
	copy->m_owningMaterial = m_owningMaterial;

	return copy;
}

struct HandleHelper
{
	union
	{
		struct
		{
			unsigned short Index;
			unsigned short Location;
		};

		unsigned int ID;
	};

	HandleHelper(unsigned int id) : ID(id)
	{
		Index -= (unsigned short)1;
	}
};

void MaterialPropertyList::SetFloat(MatVariable handle, float value)
{
	HandleHelper helper(handle);

	if (m_floatProperties.InRange(helper.Index))
	{
		if (m_floatProperties[helper.Index].PropHandle == handle)
		{
			m_floatProperties[helper.Index].Value = value;
		}
	}
}

void MaterialPropertyList::SetFloat(const char* name, float value)
{
	MatVariable handle = GetVariableHandle(name, PropertyType::Float);
	
	if (handle != 0)
	{
		SetFloat(handle, value);
	}
}

void MaterialPropertyList::SetBool(MatVariable handle, bool value)
{
	HandleHelper helper(handle);

	if (m_intProperties.InRange(helper.Index))
	{
		if (m_intProperties[helper.Index].PropHandle == handle)
		{
			m_intProperties[helper.Index].Value = value;
		}
	}
}

void MaterialPropertyList::SetBool(const char* name, bool value)
{
	MatVariable handle = GetVariableHandle(name, PropertyType::Int);
		
	if (handle != 0)
	{
		SetBool(handle, value);
	}
}

void MaterialPropertyList::SetInt(MatVariable handle, int value)
{
	HandleHelper helper(handle);

	if (m_intProperties.InRange(helper.Index))
	{
		if (m_intProperties[helper.Index].PropHandle == handle)
		{
			m_intProperties[helper.Index].Value = value;
		}
	}
}

void MaterialPropertyList::SetInt(const char* name, int value)
{
	MatVariable handle = GetVariableHandle(name, PropertyType::Int);

	if (handle != 0)
	{
		SetInt(handle, value);
	}
}

void MaterialPropertyList::SetTexture(MatVariable handle, Texture* texture)
{
	HandleHelper helper(handle);

	if (m_textureProperties.InRange(helper.Index))
	{
		if (m_textureProperties[helper.Index].PropHandle == handle)
		{
			m_textureProperties[helper.Index].Texture = texture;
		}
	}
}

void MaterialPropertyList::SetTexture(const char* name, Texture* texture)
{
	MatVariable handle = GetVariableHandle(name, PropertyType::Texture);

	if (handle != 0)
	{
		SetTexture(handle, texture);
	}
}

void MaterialPropertyList::SetVector(MatVariable handle, const Vector3& vector)
{
	HandleHelper helper(handle);

	if (m_vector3Properties.InRange(helper.Index))
	{
		if (m_vector3Properties[helper.Index].PropHandle == handle)
		{
			m_vector3Properties[helper.Index].Value = vector;
		}
	}
}

void MaterialPropertyList::SetVector(const char* name, const Vector3& vector)
{
	MatVariable handle = GetVariableHandle(name, PropertyType::Vector3);

	if (handle != 0)
	{
		SetVector(handle, vector);
	}
}

void MaterialPropertyList::SetVector(MatVariable handle, const Vector4& vector)
{
	HandleHelper helper(handle);

	if (m_vectorProperties.InRange(helper.Index))
	{
		if (m_vectorProperties[helper.Index].PropHandle == handle)
		{
			m_vectorProperties[helper.Index].Value = vector;
		}
	}
}

void MaterialPropertyList::SetVector(const char* name, const Vector4& vector)
{
	MatVariable handle = GetVariableHandle(name, PropertyType::Vector);

	if (handle != 0)
	{
		SetVector(handle, vector);
	}
}

void MaterialPropertyList::SetColour(MatVariable handle, const Colour& colour)
{
	HandleHelper helper(handle);

	if (m_vectorProperties.InRange(helper.Index))
	{
		if (m_vectorProperties[helper.Index].PropHandle == handle)
		{
			m_vectorProperties[helper.Index].Value = Vector4(colour.R, colour.G, colour.B, colour.A);
		}
	}
}

void MaterialPropertyList::SetColour(const char* name, const Colour& colour)
{
	MatVariable handle = GetVariableHandle(name, PropertyType::Vector);

	if (handle != 0)
	{
		SetColour(handle, colour);
	}
}