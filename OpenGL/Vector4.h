#pragma once

#include "Mathfwd.h"

struct Vector4
{
	float X;
	float Y;
	float Z;
	float W;

	static const Vector4 ZeroVector;

public:
	Vector4();
	Vector4(float x, float y, float z, float w = 0);
	Vector4(Vector3 vec, float w = 0);

	Vector4 operator*(const Matrix& m) const;
	operator Vector3() const;
};