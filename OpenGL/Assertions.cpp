#include "Assertions.h"
#include "InputManager.h"

#pragma push_macro("TEXT")

#undef TEXT
#include <Windows.h>

#pragma pop_macro("TEXT")

#include <string>

#ifdef UNICODE
#define PRINTF(size, ...) swprintf_s<size>(__VA_ARGS__)
#else
#define PRINTF(size, buffer, ...) sprintf_s<size>(buffer, __VA_ARGS__)
#endif

void Debug::LogAssert(const TCHAR* message, int line, const TCHAR* function, const TCHAR* file)
{
	TCHAR mesg[1024];

	PRINTF(1024, mesg, TEXT("%s \nLine: %d \nFunction: %s \nFile: %s"), message, line, function, file);
		
	int choice = MessageBox(nullptr, mesg, TEXT("Assertion Failed"), MB_ICONERROR | MB_ABORTRETRYIGNORE);

	if (choice == 3)
	{
		exit(3);
	}
	else if (choice == 4)
	{
		//GInputManager.HideMouse(false);
		__debugbreak();
	}
}

void Debug::LogCheck(const TCHAR* message, int line, const TCHAR* function, const TCHAR* file)
{
	TCHAR mesg[1024];

	PRINTF(1024, mesg, TEXT("%s \nLine: %d \nFunction: %s, \nFile: %s\n"), message, line, function, file);

	wprintf(mesg);
}

bool Debug::LogCheck(bool condition, const TCHAR* message, int line, const TCHAR* function, const TCHAR* file)
{
	if (condition)
	{
		LogCheck(message, line, function, file);
	}

	return false;
}