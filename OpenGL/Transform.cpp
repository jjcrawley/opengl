#include "Transform.h"
#include <cmath>
#include "Debugger.h"

Vector3 Transform::InverseScale() const
{
	const float tolerance = 1e-6f;
	
	Vector3 scale;

	if (abs(m_scale.X) < tolerance)
	{
		scale.X = 0.0f;
	}
	else
	{
		scale.X = 1.0f / m_scale.X;
	}

	if(abs(m_scale.Y) < tolerance)
	{
		scale.Y = 0.0f;
	}
	else
	{
		scale.Y = 1.0f / m_scale.Y;
	}

	if (abs(m_scale.Z) < tolerance)
	{
		scale.Z = 0.0f;
	}
	else
	{
		scale.Z = 1.0f / m_scale.Z;
	}

	return scale;
}

Matrix Transform::GetMatrix() const
{
	Matrix result;

	// May need to check for a uniform scale too
	if (!HasSkew())
	{		
		result = MatrixFactory::TRSMatrix(m_scale, m_rotation, m_translation);
	}
	else
	{
		result = MatrixFactory::TRSkewSMatrix(m_scale, m_skew, m_rotation, m_translation);
	}
	
	return result;
}

Matrix Transform::GetUnScaledMatrix() const
{
	Matrix result;

	if (!HasSkew())
	{
		result = MatrixFactory::TRSMatrix(Vector3(1.0f, 1.0f, 1.0f), m_rotation, m_translation);
	}
	else
	{
		result = MatrixFactory::TRSkewSMatrix(Vector3(1.0f, 1.0f, 1.0f), m_skew, m_rotation, m_translation);
	}

	return result;
}

Transform Transform::Inverse() const
{
	Transform result;
		
	// If we have uniform scale then we can set the values directly, else we need to set it from the matrix.	
	if(HasUniformScale())
	{
		result.m_scale = InverseScale();
		result.m_rotation = m_rotation.Inverse();
		result.m_translation = result.m_rotation.RotateVector(-m_translation) * result.m_scale;
	}
	else
	{
		// The matrix is necessary as the order of multiplication needs to be preserved
		// In other words: M = (S * Skew * R * T).Inverse, is not the same as N = (S.Inverse * Skew.Inverse * R.Inverse * T.Inverse). 
		// For N to be the correct inverse, you would have to apply the matrices in reverse order IE: N = T.Inverse * R.Inverse * Skew.Inverse * S.Inverse
		// S = scaleMatrix, Skew = skewMatrix, R = rotationMatrix, T = translationMatrix
		// So to keep the math sane, a new scale, rotation, skew and translation needs to be calculated

		Matrix matrix = GetMatrix();
		matrix = matrix.Inverse();

		if (matrix != Matrix::Identity)
		{
			result.SetFromMatrix(matrix);
		}		
	}

	return result;
}

void Transform::SetFromMatrix(const Matrix& matrix)
{
	matrix.Decompose(&m_scale, &m_skew, &m_rotation, &m_translation);
}

Transform Transform::operator*(const Transform& transform) const
{
	Transform result;

	if (transform.HasUniformScale() && !transform.HasSkew() && !HasSkew())
	{
		result.m_scale = m_scale * transform.m_scale;
		result.m_rotation = transform.m_rotation * m_rotation;

		result.m_translation = transform.m_rotation * (transform.m_scale * m_translation) + transform.m_translation;
	}
	else
	{
		Matrix mat = GetMatrix() * transform.GetMatrix();

		result.SetFromMatrix(mat);
	}

	return result;
}

Transform& Transform::operator*=(const Transform& transform)
{
	if (transform.HasUniformScale() && !transform.HasSkew() && !HasSkew())
	{
		m_scale = m_scale * transform.m_scale;
		m_rotation = transform.m_rotation * m_rotation;

		m_translation = transform.m_rotation * (transform.m_scale * m_translation) + transform.m_translation;
	}
	else
	{
		Matrix mat = GetMatrix() * transform.GetMatrix();

		SetFromMatrix(mat);
	}

	return *this;
}

bool Transform::HasUniformScale() const
{
	const float tolerance = 1e-8f;
	
	return abs(abs(m_scale.X) - abs(m_scale.Y)) < tolerance &&
		abs(abs(m_scale.X) - abs(m_scale.Z)) < tolerance &&
		abs(abs(m_scale.Y) - abs(m_scale.Z)) < tolerance;
}

bool Transform::HasSkew() const
{
	const float tolerance = 1e-6f;

	return !(abs(m_skew.X) < tolerance && abs(m_skew.Y) < tolerance && abs(m_skew.Z) < tolerance);
}

Vector3 Transform::TransformDirection(const Vector3& vector) const
{
	Vector3 result;

	if (!HasSkew())
	{
		result = m_rotation * (vector * m_scale);
	}
	else
	{
		// Special case for handling skew
		// You can find the math by the following matrix multiplication:
		// 
		//				[m_scale.X	0			0		 ]	 [1			0		  0]
		// [x, y, z] *	[0      	m_scale.Y	0		 ] * [m_skew.X  1		  0]	
		//				[0          0	    	m_scale.Z]	 [m_skew.Y  m_skew.Z  1]
		//
		result.X = vector.X * m_scale.X + vector.Y * m_skew.X * m_scale.Y + vector.Z * m_skew.Y * m_scale.Z;
		result.Y = vector.Y * m_scale.Y + vector.Z * m_skew.Z * m_scale.Z;
		result.Z = vector.Z * m_scale.Z;

		result = (m_rotation * result);
	}

	return result;
}

Vector3 Transform::TransformPosition(const Vector3& vector) const
{	
	return TransformDirection(vector) + m_translation;
}

Vector4 Transform::TransformVector(const Vector4& vector) const
{
	Vector4 result;

	if (vector.W == 0.0f)
	{
		result = Vector4(TransformDirection(Vector3(vector.X, vector.Y, vector.Z)), 0.0f);
	}
	else if(vector.W == 1.0f)
	{
		result = Vector4(TransformPosition(Vector3(vector.X, vector.Y, vector.Z)), 1.0f);
	}

	return result;
}

Vector3 Transform::InverseTransformDirection(const Vector3& vector) const
{
	Vector3 result;

	Vector3 inverseScale = InverseScale();

	if(HasSkew())
	{		
		result = m_rotation.UnRotateVector(vector);

		// Special case for handling skew
		// You can find the math by the following matrix multiplication:
		// 
		//				[1								  0			 0]	  [1 / m_scale.X  0				 0			  ]
		// [x, y, z] *	[-m_skew.X						  1			 0] * [0			  1 / m_scale.Y	 0			  ]	
		//				[m_skew.X * m_skew.Z - m_skew.Y   -m_skew.Z  1]	  [0			  0				 1 / m_scale.Z]
		//

		result.X = (vector.X - vector.Y * m_skew.X + vector.Z * (m_skew.X * m_skew.Z - m_skew.Y)) * inverseScale.X;
		result.Y = (vector.Y - vector.Z * m_skew.Z) * inverseScale.Y;
		result.Z = vector.Z * inverseScale.Z;
	}
	else
	{		
		result =  m_rotation.UnRotateVector(vector) * inverseScale;
	}

	return result;
}

Vector3 Transform::InverseTransformPosition(const Vector3& vector) const
{
	return InverseTransformDirection(vector - m_translation);
}

Vector4 Transform::InverseTransformVector(const Vector4& vector) const
{
	if (vector.W == 0.0f)
	{
		return Vector4(InverseTransformDirection(vector), 0.0f);
	}
	else if (vector.W == 1.0f)
	{
		return Vector4(InverseTransformPosition(vector), 1.0f);
	}

	return Vector4();
}

Quaternion Transform::RotateQuat(const Quaternion& quaternion) const
{
	return m_rotation * quaternion;
}

Quaternion Transform::UnRotateQuat(const Quaternion& quaternion) const
{
	return m_rotation.Inverse() * quaternion;
}

std::ostream& operator<<(std::ostream& stream, const Transform& transform)
{
	stream << "Translation: " << transform.GetTranslation() << "\n";
	stream << "Rotation: " << transform.GetRotation() << "\n";
	stream << "Skew: " << transform.GetSkew() << "\n";
	stream << "Scale: " << transform.GetScale();	

	return stream;
}