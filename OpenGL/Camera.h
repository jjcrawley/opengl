#pragma once
#include "Mathfwd.h"
#include "RendererManager.h"
#include "SceneObject.h"

enum class CameraType
{
	Orthographic,
	Perspective
};

class Camera : public SceneObject
{
	float m_near;
	float m_far;

	float m_aspectRatio;
	float m_fov;

	float m_orthoWidth;

	Matrix m_projectionMat;
	Matrix m_viewMat;

	bool b_updateProjection;
	CameraType m_type;
public:
	Camera() : b_updateProjection(true),
		m_near(0.1f),
		m_far(100.0f),
		m_orthoWidth(10.0f),
		m_aspectRatio(1.33333f),
		m_fov(FMath::DegToRad(57.9f)),
		m_type(CameraType::Perspective)
	{}
		
	void SetFOV(float fov)
	{
		if (m_fov == fov)
		{
			return;
		}

		m_fov = fov;
		b_updateProjection = true;
	}

	float GetFOV() const
	{
		return m_fov;
	}

	void SetAspect(float width, float height)
	{
		SetAspect(width / height);
	}

	void SetAspect(float aspect)
	{
		if (m_aspectRatio == aspect)
		{
			return;
		}

		m_aspectRatio = aspect;
		b_updateProjection = true;
	}

	float GetAspect() const
	{
		return m_aspectRatio;
	}
	
	void SetNear(float near)
	{
		if (near == m_near)
		{
			return;
		}

		m_near = near;
		b_updateProjection = true;
	}

	float GetNear() const
	{
		return m_near;
	}

	void SetFar(float far)
	{
		if (m_far == far)
		{
			return;
		}

		m_far = far;
		b_updateProjection = true;
	}

	float GetFar() const
	{
		return m_far;
	}

	void SetType(CameraType type)
	{
		if (m_type == type)
		{
			return;
		}

		m_type = type;
		b_updateProjection = true;
	}

	CameraType GetType() const
	{
		return m_type;
	}

	void SetOrthWidth(float width)
	{
		if (width == m_orthoWidth)
		{
			return;
		}

		m_orthoWidth = width;

		if (m_type == CameraType::Orthographic)
		{
			b_updateProjection = true;
		}
	}

	float GetOrthoWidth() const
	{
		return m_orthoWidth;
	}

	void PrepCamera();

	Matrix GetProjectionMatrix() const;
	Matrix GetViewMatrix() const;
	Matrix GetViewProjectionMatrix() const;
};