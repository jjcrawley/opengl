#include "MatrixFactory.h"
#include "MyMath.h"
#include <cmath>

Matrix MatrixFactory::Translate(float x, float y, float z)
{
	return Matrix(Vector4(1.0f, 0.0f, 0.0f, 0.0f), 
		Vector4(0.0f, 1.0f, 0.0f, 0.0f), 
		Vector4(0.0f, 0.0f, 1.0f, 0.0f), 
		Vector4(x, y, z, 1.0f));
}

Matrix MatrixFactory::Translate(const Vector3& v)
{
	return Translate(v.X, v.Y, v.Z);
}

Matrix MatrixFactory::RotateAboutAxis(const Vector3& axis, float angle)
{
	Matrix result;

	float cosAngle = cos(angle);
	float oneMinusCos = 1.0f - cosAngle;
	float sinAngle = sin(angle);

	result.M[0][0] = axis.X * axis.X * oneMinusCos + cosAngle;
	result.M[0][1] = axis.X * axis.Y * oneMinusCos + axis.Z * sinAngle;
	result.M[0][2] = axis.X * axis.Z * oneMinusCos - axis.Y * sinAngle;
	result.M[0][3] = 0.0f;

	result.M[1][0] = axis.X * axis.Y * oneMinusCos - axis.Z * sinAngle;
	result.M[1][1] = axis.Y * axis.Y * oneMinusCos + cosAngle;
	result.M[1][2] = axis.Z * axis.Y * oneMinusCos + axis.X * sinAngle;
	result.M[1][3] = 0.0f;

	result.M[2][0] = axis.X * axis.Z * oneMinusCos + axis.Y * sinAngle;
	result.M[2][1] = axis.Y * axis.Z * oneMinusCos - axis.X * sinAngle;
	result.M[2][2] = axis.Z * axis.Z * oneMinusCos + cosAngle;
	result.M[2][3] = 0.0f;

	result.M[3][0] = 0.0f;
	result.M[3][1] = 0.0f;
	result.M[3][2] = 0.0f;
	result.M[3][3] = 1.0f;

	return result;
}

Matrix MatrixFactory::Scale(Vector3 scale)
{
	return Scale(scale.X, scale.Y, scale.Z);
}

Matrix MatrixFactory::Scale(float x, float y, float z)
{
	return Matrix(
	{ x, 0.0f, 0.0f, 0.0f },
	{ 0.0f, y, 0.0f, 0.0f },
	{ 0.0f, 0.0f, z, 0.0f },
	{ 0.0f, 0.0f, 0.0f, 1.0f });
}

Matrix MatrixFactory::Scale(float scale)
{
	return Scale(scale, scale, scale);
}

Matrix MatrixFactory::ScaleAlongAxis(const Vector3& axis, float scale)
{
	Matrix result;

	float scaleMinusOne = scale - 1.0f;

	Vector3 nAxis = axis.Normalise();

	result.M[0][0] = 1.0f + scaleMinusOne * nAxis.X * nAxis.X;
	result.M[0][1] = scaleMinusOne * nAxis.X * nAxis.Y;
	result.M[0][2] = scaleMinusOne * nAxis.X * nAxis.Z;
	result.M[0][3] = 0.0f;

	result.M[1][0] = scaleMinusOne * nAxis.X * nAxis.Y;
	result.M[1][1] = 1.0f + scaleMinusOne * nAxis.Y * nAxis.Y;
	result.M[1][2] = scaleMinusOne * nAxis.Y * nAxis.Z;
	result.M[1][3] = 0.0f;

	result.M[2][0] = scaleMinusOne * nAxis.Z * nAxis.X;
	result.M[2][1] = scaleMinusOne * nAxis.Z * nAxis.Y;
	result.M[2][2] = 1.0f + scaleMinusOne * nAxis.Z * nAxis.Z;
	result.M[2][3] = 0.0f;

	result.M[3][0] = 0.0f;
	result.M[3][1] = 0.0f;
	result.M[3][2] = 0.0f;
	result.M[3][3] = 1.0f;

	// Calculate vPar;
	// Calculate vPerp;
	// Scale the vPar using scale
	// Calculate the new v

	return result;
}

Matrix MatrixFactory::OrthographicProjection(const Vector3& axis)
{
	Matrix result;

	Vector3 nAxis = axis.Normalise();

	result.M[0][0] = 1.0f - nAxis.X * nAxis.X;
	result.M[0][1] = -nAxis.X * nAxis.Y;
	result.M[0][2] = -nAxis.X * nAxis.Z;
	result.M[0][3] = 0.0f;

	result.M[1][0] = -nAxis.X * nAxis.Y;
	result.M[1][1] = 1.0f - nAxis.Y * nAxis.Y;
	result.M[1][2] = -nAxis.Z * nAxis.Y;
	result.M[1][3] = 0.0f;

	result.M[2][0] = -nAxis.X * nAxis.Z;
	result.M[2][1] = -nAxis.Y * nAxis.Z;
	result.M[2][2] = 1.0f - nAxis.Z * nAxis.Z;
	result.M[2][3] = 0.0f;

	result.M[3][0] = 0.0f;
	result.M[3][1] = 0.0f;
	result.M[3][2] = 0.0f;
	result.M[3][3] = 1.0f;

	return result;
}

Matrix MatrixFactory::ReflectAlongAxis(const Vector3& axis)
{
	Matrix result;

	Vector3 nAxis = axis.Normalise();

	result.M[0][0] = 1.0f - 2.0f * nAxis.X * nAxis.X;
	result.M[0][1] = -2.0f * nAxis.X * nAxis.Y;
	result.M[0][2] = -2.0f * nAxis.X * nAxis.Z;
	result.M[0][3] = 0.0f;

	result.M[1][0] = -2.0f * nAxis.Y * nAxis.X;
	result.M[1][1] = 1.0f - 2.0f * nAxis.Y * nAxis.Y;
	result.M[1][2] = -2.0f * nAxis.Y * nAxis.Z;
	result.M[1][3] = 0.0f;

	result.M[2][0] = -2.0f * nAxis.X * nAxis.Z;
	result.M[2][1] = -2.0f * nAxis.Y * nAxis.Z;
	result.M[2][2] = 1.0f - 2.0f * nAxis.Z * nAxis.Z;
	result.M[2][3] = 0.0f;

	result.M[3][0] = 0.0f;
	result.M[3][1] = 0.0f;
	result.M[3][2] = 0.0f;
	result.M[3][3] = 1.0f;

	return result;
}

Matrix MatrixFactory::Rotation(float pitch, float heading, float roll)
{
	return Rotation(ERotation(pitch, heading, roll));
}

Matrix MatrixFactory::Rotation(const ERotation& angles)
{
	Matrix result;

	const float ch = cos(angles.Heading);
	const float cp = cos(angles.Pitch);
	const float cb = cos(angles.Roll);

	const float sh = sin(angles.Heading);
	const float sp = sin(angles.Pitch);
	const float sb = sin(angles.Roll);

	result.M[0][0] = ch * cb + sh * sp * sb;
	result.M[0][1] = sb * cp;
	result.M[0][2] = ch * sp * sb - sh * cb;
	result.M[0][3] = 0.0f;

	result.M[1][0] = sh * sp * cb - ch * sb;
	result.M[1][1] = cb * cp;
	result.M[1][2] = sb * sh + ch * sp * cb;
	result.M[1][3] = 0.0f;

	result.M[2][0] = sh * cp;
	result.M[2][1] = -sp;
	result.M[2][2] = ch * cp;
	result.M[2][3] = 0.0f;

	result.M[3][0] = 0.0f;
	result.M[3][1] = 0.0f;
	result.M[3][2] = 0.0f;
	result.M[3][3] = 1.0f;

	return result;
}

Matrix MatrixFactory::Rotation(const Quaternion& q)
{
	Matrix result;

	result.M[0][0] = 1.0f - 2.0f * (q.Y * q.Y + q.Z * q.Z);
	result.M[0][1] = 2.0f * (q.X * q.Y + q.W * q.Z);
	result.M[0][2] = 2.0f * (q.X * q.Z - q.W * q.Y);
	result.M[0][3] = 0.0f;

	result.M[1][0] = 2.0f * (q.X * q.Y - q.W * q.Z);
	result.M[1][1] = 1.0f - 2.0f * (q.X * q.X + q.Z * q.Z);
	result.M[1][2] = 2.0f * (q.Y * q.Z + q.W * q.X);
	result.M[1][3] = 0.0f;

	result.M[2][0] = 2.0f * (q.X * q.Z + q.W * q.Y);
	result.M[2][1] = 2.0f * (q.Y * q.Z - q.W * q.X);
	result.M[2][2] = 1.0f - 2.0f * (q.X * q.X + q.Y * q.Y);
	result.M[2][3] = 0.0f;

	result.M[3][0] = 0.0f;
	result.M[3][1] = 0.0f;
	result.M[3][2] = 0.0f;
	result.M[3][3] = 1.0f;

	return result;
}

Matrix MatrixFactory::PerspectiveLH(float fovX, float aspect, float near, float far)
{
	const float zoomX = 1.0f / tan(fovX * 0.5f);
	const float zoomY = zoomX * aspect;

	const float farMinusNear = far - near;

	const float m22 = (far + near) / farMinusNear;
	const float m32 = -2.0f * near * far / farMinusNear;

	return Matrix(
		Vector4(zoomX, 0.0f, 0.0f, 0.0f),
		Vector4(0.0f, zoomY, 0.0f, 0.0f),
		Vector4(0.0f, 0.0f, m22, 1.0f),
		Vector4(0.0f, 0.0f, m32, 0.0f));
}

Matrix MatrixFactory::PerspectiveLH(float fovX, float width, float height, float near, float far)
{
	const float zoomX = 1.0f / tan(fovX * 0.5f);
	const float zoomY = zoomX * width / height;
	
	const float farMinusNear = far - near;

	const float m22 = (far + near) / farMinusNear;
	const float m32 = -2.0f * near * far / farMinusNear;

	return Matrix(
		Vector4(zoomX, 0.0f, 0.0f, 0.0f), 
		Vector4(0.0f, zoomY, 0.0f, 0.0f), 
		Vector4(0.0f, 0.0f, m22, 1.0f),
		Vector4(0.0f, 0.0f, m32, 0.0f));
}

Matrix MatrixFactory::PerspectiveRH(float fovX, float width, float height, float near, float far)
{
	const float zoomX = 1.0f / tan(fovX * 0.5f);
	const float zoomY = zoomX * width / height;

	const float farMinusNear = far - near;

	const float m22 = -(far + near) / farMinusNear;
	const float m32 = -2.0f * near * far / farMinusNear;

	return Matrix(
		Vector4(zoomX, 0.0f, 0.0f, 0.0f),
		Vector4(0.0f, zoomY, 0.0f, 0.0f),
		Vector4(0.0f, 0.0f, m22, -1.0f),
		Vector4(0.0f, 0.0f, m32, 0.0f));
}

Matrix MatrixFactory::OrthographicLH(float width, float height, float near, float far)
{
	const float invWidth = 2.0f / width;
	const float invHeight = 2.0f / height;
	const float invFMinusN = 1.0f / (far - near);

	return Matrix(
		Vector4(invWidth, 0.0f, 0.0f, 0.0f), 
		Vector4(0.0f, invHeight, 0.0f, 0.0f), 
		Vector4(0.0f, 0.0f, 2.0f * invFMinusN, 0.0f), 
		Vector4(0.0f, 0.0f, (far + near) * invFMinusN, 1.0f));
}

Matrix MatrixFactory::OrthographicLHAspect(float width, float aspect, float near, float far)
{
	const float invWidth = 2.0f / width;
	const float invHeight = (2.0f / width) / aspect;
	const float invFMinusN = 1.0f / (far - near);

	return Matrix(
		Vector4(invWidth, 0.0f, 0.0f, 0.0f),
		Vector4(0.0f, invHeight, 0.0f, 0.0f),
		Vector4(0.0f, 0.0f, 2.0f * invFMinusN, 0.0f),
		Vector4(0.0f, 0.0f, -(far + near) * invFMinusN, 1.0f));
}

Matrix MatrixFactory::OrthographicLH(float left, float right, float top, float bottom, float near, float far)
{
	const float invWidth = 1.0f / (right - left);
	const float invHeight = 1.0f / (top - bottom);
	const float invFMinusN = 1.0f / (far - near);

	return Matrix(
		Vector4(2.0f * invWidth, 0.0f, 0.0f, -(right + left) * invWidth),
		Vector4(0.0f, 2.0f * invHeight, 0.0f, -(top + bottom) * invHeight),
		Vector4(0.0f, 0.0f, 2.0f * invFMinusN, 0.0f),
		Vector4(0.0f, 0.0f, -(far + near) * invFMinusN, 1.0f));
}

Matrix MatrixFactory::OrthographicRH(float width, float height, float near, float far)
{
	const float invWidth = 2.0f / width;
	const float invHeight = 2.0f / height;
	const float invFMinusN = 1.0f / (far - near);

	return Matrix(
		Vector4(invWidth, 0.0f, 0.0f, 0.0f),
		Vector4(0.0f, invHeight, 0.0f, 0.0f),
		Vector4(0.0f, 0.0f, -2.0f * invFMinusN, 0.0f),
		Vector4(0.0f, 0.0f, -(far + near) * invFMinusN, 1.0f));
}

Matrix MatrixFactory::LookToLH(const Vector3& direction, const Vector3& up, const Vector3& position)
{
	Vector3 ZAxis = direction.Normalise();
	Vector3 XAxis = (up.Cross(ZAxis)).Normalise();
	Vector3 YAxis = ZAxis.Cross(XAxis);

	Matrix result;

	result.M[0][0] = XAxis.X;
	result.M[0][1] = YAxis.X;
	result.M[0][2] = ZAxis.X;
	result.M[0][3] = 0.0f;
	
	result.M[1][0] = XAxis.Y;
	result.M[1][1] = YAxis.Y;
	result.M[1][2] = ZAxis.Y;
	result.M[1][3] = 0.0f;

	result.M[2][0] = XAxis.Z;
	result.M[2][1] = YAxis.Z;
	result.M[2][2] = ZAxis.Z;
	result.M[2][3] = 0.0f;

	result.M[3][0] = -position.Dot(XAxis);
	result.M[3][1] = -position.Dot(YAxis);
	result.M[3][2] = -position.Dot(ZAxis);
	result.M[3][3] = 1.0f;
	
	return result;
}

Matrix MatrixFactory::LookToRH(const Vector3& direction, const Vector3& up, const Vector3& position)
{
	Vector3 ZAxis = direction.Normalise();
	Vector3 XAxis = (ZAxis.Cross(up)).Normalise();
	Vector3 YAxis = XAxis.Cross(ZAxis);

	Matrix result;

	result.M[0][0] = XAxis.X;
	result.M[0][1] = YAxis.X;
	result.M[0][2] = ZAxis.X;
	result.M[0][3] = 0.0f;

	result.M[1][0] = XAxis.Y;
	result.M[1][1] = YAxis.Y;
	result.M[1][2] = ZAxis.Y;
	result.M[1][3] = 0.0f;

	result.M[2][0] = -XAxis.Z;
	result.M[2][1] = -YAxis.Z;
	result.M[2][2] = -ZAxis.Z;
	result.M[2][3] = 0.0f;

	result.M[3][0] = -position.Dot(XAxis);
	result.M[3][1] = -position.Dot(YAxis);
	result.M[3][2] = -position.Dot(ZAxis);
	result.M[3][3] = 1.0f;

	return result;
}

Matrix MatrixFactory::LookAtLH(const Vector3& target, const Vector3& up, const Vector3& position)
{
	return LookToLH(target - position, up, position);
}

Matrix MatrixFactory::TRSMatrix(const Vector3& scale, const Quaternion& rotation, const Vector3& translation)
{
	Matrix result;

	result.M[0][0] = (1.0f - 2.0f * (rotation.Y * rotation.Y + rotation.Z * rotation.Z)) * scale.X;
	result.M[0][1] = 2.0f * (rotation.X * rotation.Y + rotation.W * rotation.Z) * scale.X;
	result.M[0][2] = 2.0f * (rotation.X * rotation.Z - rotation.W * rotation.Y) * scale.X;
	result.M[0][3] = 0.0f;

	result.M[1][0] = 2.0f * (rotation.X * rotation.Y - rotation.W * rotation.Z) * scale.Y;
	result.M[1][1] = (1.0f - 2.0f * (rotation.X * rotation.X + rotation.Z * rotation.Z)) * scale.Y;
	result.M[1][2] = 2.0f * (rotation.Y * rotation.Z + rotation.W * rotation.X) * scale.Y;
	result.M[1][3] = 0.0f;

	result.M[2][0] = 2.0f * (rotation.X * rotation.Z + rotation.W * rotation.Y) * scale.Z;
	result.M[2][1] = 2.0f * (rotation.Y * rotation.Z - rotation.W * rotation.X) * scale.Z;
	result.M[2][2] = (1.0f - 2.0f * (rotation.X * rotation.X + rotation.Y * rotation.Y)) * scale.Z;
	result.M[2][3] = 0.0f;

	result.M[3][0] = translation.X;
	result.M[3][1] = translation.Y;
	result.M[3][2] = translation.Z;
	result.M[3][3] = 1.0f;

	return result;
}

Matrix MatrixFactory::TRSkewSMatrix(const Vector3& scale, const Vector3& skew, const Quaternion& rotation, const Vector3& translation)
{
	// The following math is akin to some matrix math:
	// Scale							Skew					 Rotation          Translation
	// [scale.X  0        0        0]	[1 		 0		 0  0]	 [R1  R2  R3  0]   [1  0  0  0]
	// [0        scale.Y  0        0] *	[skew.X  1		 0	0] * [R4  R5  R6  0] * [0  1  0  0]
	// [0        0        scale.Z  0]	[skew.Y  skew.Z  1	0]	 [R7  R8  R9  0]   [0  0  1  0]
	// [0        0        0        1]	[0		 0		 0	1]	 [0   0   0   1]   [x  y  z  1]
	// Note, the skew is predivided by abs(scale)	

	const float R1 = (1.0f - 2.0f * (rotation.Y * rotation.Y + rotation.Z * rotation.Z));
	const float R2 = 2.0f * (rotation.X * rotation.Y + rotation.W * rotation.Z);
	const float R3 = 2.0f * (rotation.X * rotation.Z - rotation.W * rotation.Y);
	
	const float R4 = 2.0f * (rotation.X * rotation.Y - rotation.W * rotation.Z);
	const float R5 = (1.0f - 2.0f * (rotation.X * rotation.X + rotation.Z * rotation.Z));
	const float R6 = 2.0f * (rotation.Y * rotation.Z + rotation.W * rotation.X);
	
	const float R7 = 2.0f * (rotation.X * rotation.Z + rotation.W * rotation.Y);
	const float R8 = 2.0f * (rotation.Y * rotation.Z - rotation.W * rotation.X);
	const float R9 = (1.0f - 2.0f * (rotation.X * rotation.X + rotation.Y * rotation.Y));
	
	Matrix result;

	//Matrix scaleMat = MatrixFactory::Scale(scale);

	//Matrix R = Matrix::Identity;

	//scaleMat.M[1][0] = skew.X * scale.Y;
	//scaleMat.M[2][0] = skew.Y * scale.Z;
	//scaleMat.M[2][1] = skew.Z * scale.Z;

	//result = scaleMat * MatrixFactory::Rotation(rotation) * MatrixFactory::Translate(translation);

	result.M[0][0] = scale.X * R1;
	result.M[0][1] = scale.X * R2;
	result.M[0][2] = scale.X * R3;
	result.M[0][3] = 0.0f;

	result.M[1][0] = scale.Y * (skew.X * R1 + R4);
	result.M[1][1] = scale.Y * (skew.X * R2 + R5);
	result.M[1][2] = scale.Y * (skew.X * R3 + R6);
	result.M[1][3] = 0.0f;

	result.M[2][0] = scale.Z * (skew.Y * R1 + skew.Z * R4 + R7);
	result.M[2][1] = scale.Z * (skew.Y * R2 + skew.Z * R5 + R8);
	result.M[2][2] = scale.Z * (skew.Y * R3 + skew.Z * R6 + R9);
	result.M[2][3] = 0.0f;

	result.M[3][0] = translation.X;
	result.M[3][1] = translation.Y;
	result.M[3][2] = translation.Z;
	result.M[3][3] = 1.0f;

	return result;
}