#include "ObjLoader.h"
#include "Debugger.h"
#include "Array.h"
#include "Math.h"
#include "ResourceManager.h"

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>

ObjLoader::ObjLoader()
{
}

ObjLoader::~ObjLoader()
{
}

void TrimBeginningString(std::wstring& string)
{
	int beginningOfString = 0;
	TCHAR current;

	do
	{
		current = string[beginningOfString];
		++beginningOfString;
	}
	while (current == ' ' && current != std::wstring::npos);	

	string = string.substr(beginningOfString - 1);
}

void GenerateNormals(const Array<Vector3>& vertices, const Array<unsigned int>& indices, Array<Vector3>& normals)
{		
	// Initialise the normals array with all zero vectors
	for (int i = 0; i < vertices.Size(); ++i)
	{
		normals.Add(Vector3::Zero);
	}
	
	Vector3 surfaceNormal;
	Vector3 edgeOne;
	Vector3 edgeTwo;
	
	for (int i = 0; i < indices.Size(); i += 3)
	{
		const unsigned int i1 = indices[i + 0];
		const unsigned int i2 = indices[i + 1];
		const unsigned int i3 = indices[i + 2];

		edgeOne = vertices[i1] - vertices[i2];
		edgeTwo = vertices[i3] - vertices[i2];

		surfaceNormal = edgeOne.Cross(edgeTwo).Normalise();

		normals[i1] += surfaceNormal;
		normals[i2] += surfaceNormal;
		normals[i3] += surfaceNormal;		
	}

	for (int i = 0; i < normals.Size(); ++i)
	{
		normals[i] = normals[i].Normalise();
	}	
}

Mesh* ObjLoader::LoadMesh(const TCHAR* fileName)
{		
	std::wifstream file;
	file.open(fileName);

	if (!file.is_open())
	{
		std::wprintf(TEXT("File couldn't be opened %s\n"), fileName);
		return nullptr;
	}
	
	Array<unsigned int> vertexIndices, uvIndices, normalIndices;
	Array<Vector3> vertices;
	Array<Vector2> uvs;
	Array<Vector3> normals;
	Array<unsigned int> meshIndices;

	int faceCount = 0;
	int vertexCount = 0;

	std::wstring line;	
	
	while (getline(file, line))
	{
		TrimBeginningString(line);
		std::wstringstream lineStream(line);
				
		if (line[0] == L'v' && line[1] == L't')
		{
			lineStream.ignore(3);

			Vector2 uv;
			
			lineStream >> uv.X >> uv.Y;

			uv.Y = (1 - uv.Y);

			uvs.Add(uv);
		}
		else if (line[0] == L'v' && line[1] == L'n')
		{
			lineStream.ignore(3);

			Vector3 normal;

			lineStream >> normal.X >> normal.Y >> normal.Z;

			normal.Z = -normal.Z;

			normals.Add(normal);
		}
		else if (line[0] == L'v')
		{
			lineStream.ignore(2);

			Vector3 vert;

			lineStream >> vert.X >> vert.Y >> vert.Z;

			vert.Z = -vert.Z;

			vertices.Add(vert);
		}
		else if (line[0] == L'f')
		{
			unsigned int v1, v2, v3, uv1, uv2, uv3, n1, n2, n3;
			unsigned int vCur, uvCur, nCur;
			bool exists = false;

			lineStream.ignore(2);

			// If the face data has uvs and normal data.
			if (uvs.Size() > 0 && normals.Size() > 0)
			{
				lineStream >> v1; lineStream.get();
				lineStream >> uv1; lineStream.get();
				lineStream >> n1; lineStream.get();

				lineStream >> v2; lineStream.get();
				lineStream >> uv2; lineStream.get();
				lineStream >> n2; lineStream.get();

				lineStream >> v3; lineStream.get();
				lineStream >> uv3; lineStream.get();
				lineStream >> n3;
								
				for (int i = 0; i < 3; ++i)
				{
					exists = false;

					switch (i)
					{
						case 0:
							vCur = v1;
							uvCur = uv1;
							nCur = n1;
							break;
						case 1:
							vCur = v2;
							uvCur = uv2;
							nCur = n2;
							break;
						case 2:
							vCur = v3;
							uvCur = uv3;
							nCur = n3;
							break;
					}

					for (int j = 0; j < vertexCount; ++j)
					{
						if (vertexIndices[j] == vCur)
						{
							if (uvIndices[j] == uvCur)
							{
								if (normalIndices[j] == nCur)
								{
									exists = true;
									meshIndices.Add(j);
									break;
								}
							}
						}
					}

					if (!exists)
					{
						vertexIndices.Add(vCur);
						uvIndices.Add(uvCur);
						normalIndices.Add(nCur);
						meshIndices.Add(vertexCount++);
					}
				}
			}
			// If we have uvs and no normals
			else if (uvs.Size() > 0)
			{
				lineStream >> v1; lineStream.get();
				lineStream >> uv1; lineStream.get();

				lineStream >> v2; lineStream.get();
				lineStream >> uv2; lineStream.get();

				lineStream >> v3; lineStream.get();
				lineStream >> uv3; lineStream.get();

				for (int i = 0; i < 3; ++i)
				{
					exists = false;

					switch (i)
					{
						case 0:
							vCur = v1;
							uvCur = uv1;
							break;
						case 1:
							vCur = v2;
							uvCur = uv2;
							break;
						case 2:
							vCur = v3;
							uvCur = uv3;
							break;
					}

					for (int j = 0; j < vertexCount; ++j)
					{
						if (vertexIndices[j] == vCur)
						{
							if (uvIndices[j] == uvCur)
							{
								exists = true;
								meshIndices.Add(j);
								break;
							}
						}
					}

					if (!exists)
					{
						vertexIndices.Add(vCur);
						uvIndices.Add(uvCur);
						meshIndices.Add(vertexCount++);
					}
				}
			}
			// If we have normals and no uvs
			else
			{
				lineStream >> v1; lineStream.get(); lineStream.get();
				lineStream >> n1; lineStream.get();

				lineStream >> v2; lineStream.get(); lineStream.get();
				lineStream >> n2; lineStream.get();

				lineStream >> v3; lineStream.get(); lineStream.get();
				lineStream >> n3;

				for (int i = 0; i < 3; ++i)
				{
					exists = false;

					switch (i)
					{
						case 0:
							vCur = v1;
							nCur = n1;
							break;
						case 1:
							vCur = v2;
							nCur = n2;
							break;
						case 2:
							vCur = v3;
							nCur = n3;
							break;
					}

					for (int j = 0; j < vertexCount; ++j)
					{
						if (vertexIndices[j] == vCur)
						{							
							if (normalIndices[j] == nCur)
							{
								exists = true;
								meshIndices.Add(j);
								break;
							}							
						}
					}

					if (!exists)
					{
						vertexIndices.Add(vCur);
						normalIndices.Add(nCur);
						meshIndices.Add(vertexCount++);
					}
				}
			}
						
			++faceCount;
		}
	}

	Array<Vector3> finalVerts;
	Array<Vector2> finalUVs;
	Array<Vector3> finalNormals;

	finalVerts.Reserve(vertexCount);
	finalUVs.Reserve(vertexCount);
	finalNormals.Reserve(vertexCount);
		
	// Got normal and uv data
	if (normals.Size() > 0 && uvs.Size() > 0)
	{
		for (int i = 0; i < vertexCount; ++i)
		{
			finalVerts.Add(vertices[vertexIndices[i] - 1]);
			finalUVs.Add(uvs[uvIndices[i] - 1]);
			finalNormals.Add(normals[normalIndices[i] - 1]);
		}
	}
	// Got uvs
	else if(uvs.Size() > 0)
	{
		for (int i = 0; i < vertexCount; ++i)
		{
			finalVerts.Add(vertices[vertexIndices[i] - 1]);
			finalUVs.Add(uvs[uvIndices[i] - 1]);
		}

		GenerateNormals(finalVerts, meshIndices, finalNormals);
	}
	// Got normals
	else
	{
		for (int i = 0; i < vertexCount; ++i)
		{
			finalVerts.Add(vertices[vertexIndices[i] - 1]);			
			finalNormals.Add(normals[normalIndices[i] - 1]);
			finalUVs.Add(Vector2(0.5f, 0.5f)); //Add some dummy data
		}
	}

	// Open file
	// Create variables, vertices, uvs, normals
	// index array, for faces	 

	file.close();
	
	Mesh* mesh = ResourceManager::Get().CreateResource<Mesh>();

	mesh->SetVertices(finalVerts);
	mesh->SetUVs(finalUVs);
	mesh->SetNormals(finalNormals);
	mesh->SetIndices(meshIndices);

	return mesh;
}