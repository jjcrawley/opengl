#include "Light.h"
#include "LightingEngine.h"

void Light::OnEnable()
{
	LightingEngine::Get().RegisterLight(this);
}

void Light::OnDisable()
{
	LightingEngine::Get().DeRegisterLight(this);
}