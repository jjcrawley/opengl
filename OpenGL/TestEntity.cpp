#include "TestEntity.h"
#include "Light.h"
#include "Engine.h"
#include "Camera.h"
#include "GLFWInput.h"
#include "Debugger.h"

void TestEntity::OnCreate()
{
	m_light = AddComponent<Light>();

	m_light->SetLightColour(Colour(1.0f, 1.0f, 1.0f, 1.0f));
	m_light->SetInnerSpot(FMath::DegToRad(10.0f));
	m_light->SetOuterSpot(FMath::DegToRad(20.0f));
	m_light->SetRange(100.0f);
	m_light->SetLightType(LightType::SpotLight);
	m_light->SetIntensity(10.0f);
	m_light->SetEnabled(true);

	m_camera = AddComponent<Camera>();
	RendererManager::Get().SetMainCamera(m_camera);
	SetRoot(m_camera);

	InputManager& inputManager = InputManager::Get();

	m_mouseHandle = inputManager.RegisterMouseMoveListener(this, &TestEntity::OnMouseMove);

	Axis* axis = &inputManager.CreateAxis(TEXT("MoveForward"));
	axis->AddBinding(GLFW_KEY_W, 1.0f);
	axis->AddBinding(GLFW_KEY_S, -1.0f);
	axis->RegisterListener(this, &TestEntity::ForwardDirection);

	axis = &inputManager.CreateAxis(TEXT("LeftRight"));
	axis->AddBinding(GLFW_KEY_A, -1.0f);
	axis->AddBinding(GLFW_KEY_D, 1.0f);
	axis->RegisterListener(this, &TestEntity::LeftAndRightMovement);

	axis = &inputManager.CreateAxis(TEXT("UpAndDown"));
	axis->AddBinding(GLFW_KEY_Q, -1.0f);
	axis->AddBinding(GLFW_KEY_E, 1.0f);
	axis->RegisterListener(this, &TestEntity::UpAndDownMovement);
	
	m_scrollHandle = inputManager.RegisterScrollListener(this, &TestEntity::LightBackwardAndForward);

	//axis = &inputManager.CreateAxis(TEXT("LightBackForward"));	
	//axis->AddBinding(GLFW_KEY_UP, 1.0f);
	//axis->AddBinding(GLFW_KEY_DOWN, -1.0f);
	//axis->RegisterListener(this, &TestEntity::LightBackwardAndForward);

	Action* action = &inputManager.CreateAction(TEXT("ToggleCamera"), GLFW_KEY_TAB, GLFW_MOD_CONTROL);
	action->AddObjectBinding(this, &TestEntity::ToggleCameraControl, ActionType::Press);

	action = &inputManager.CreateAction(TEXT("CloseEngine"), GLFW_KEY_ESCAPE, 0);
	action->AddObjectBinding(this, &TestEntity::ShutdownEngine, ActionType::Press);

	action = &inputManager.CreateAction(TEXT("DirectionalLight"), GLFW_KEY_1, 0);
	action->AddObjectBinding(this, &TestEntity::MakeDirectionalLight, ActionType::Press);

	action = &inputManager.CreateAction(TEXT("PointLight"), GLFW_KEY_2, 0);
	action->AddObjectBinding(this, &TestEntity::MakePointLight, ActionType::Press);

	action = &inputManager.CreateAction(TEXT("SpotLight"), GLFW_KEY_3, 0);
	action->AddObjectBinding(this, &TestEntity::MakeSpotLight, ActionType::Press);

	action = &inputManager.CreateAction(TEXT("Toggle"), GLFW_KEY_F, 0);
	action->AddObjectBinding(this, &TestEntity::ToggleLight, ActionType::Press);
	
	/*action = &inputManager.CreateAction(TEXT("Change Camera"), GLFW_KEY_R, 0);
	action->AddFunctorBinding(
	[]()
	{
	Camera* pointer = RendererManager::Get().CurrentCamera();

	if (pointer->GetType() == CameraType::Orthographic)
	{
	pointer->SetType(CameraType::Perspective);
	}
	else
	{
	pointer->SetType(CameraType::Orthographic);
	}
	},
	ActionType::Press);*/

	inputManager.HideMouse(true);
	SetActive(true);
		
	m_camera->SetRelativePosition(Vector3(0.0f, 0.0f, -5.0f));

	m_light->SetRelativePosition(Vector3(0.0f, 0.0f, 0.0f));

	m_camera->AddChild(m_light);
}

void TestEntity::Update(float deltaTime)
{
	if (m_mouseX || m_mouseY)
	{
		m_horizontalAngle += m_turnSpeed * deltaTime * m_mouseX;
		m_verticalAngle += m_turnSpeed * deltaTime * m_mouseY;

		Vector3 direction = FMath::SphericalToCartesianUnit(m_horizontalAngle, m_verticalAngle);

		m_camera->FaceDirection(direction);
		//m_light->FaceDirection(direction);
		
		Quaternion directionQuat = m_camera->GetRelativeRotation();

		m_right = directionQuat.Right();
		m_up = directionQuat.Up();
		m_forward = direction;
	}

	SceneObject* root = GetRoot();

	Vector3 position = root->GetRelativePosition();
	
	Vector3 movementVec = m_right * m_xMovement;
	movementVec += m_up * m_yMovement;
	movementVec += m_forward * m_zMovement;

	position += movementVec * deltaTime * m_moveSpeed;

	root->SetRelativePosition(position);

	if (m_lightBackwardAndForward)
	{
		Vector3 target = Vector3(0.0f, 0.0f, m_lightBackwardAndForward);

		target = FMath::Lerp(m_light->GetRelativePosition(), m_light->GetRelativePosition() + target, deltaTime * 4.0f);

		//float zMovement = FMath::Lerp(0.0f, m_lightBackwardAndForward, deltaTime);

		//Vector3 lightPosition = m_light->GetRelativePosition() + Vector3(m_lightLeftAndRight * deltaTime, 0.0f, zMovement) * 40.0f;

		m_light->SetRelativePosition(target);

		m_lightBackwardAndForward = 0.0f;

		//m_lightBackwardAndForward = FMath::Clamp(m_lightBackwardAndForward - deltaTime, 1.0f, 0.0f);
	}

	/*Quaternion rotationOne = m_camera->GetRelativeRotation();
	Quaternion rotationTwo = m_light->GetRelativeRotation();
	Quaternion worldRotation = m_light->GetWorldTransform().GetRotation();

	if (rotationOne != worldRotation)
	{
		std::cout << "Rotation is worked out incorrectly" << std::endl;
	}*/

	//m_light->SetRelativePosition(position);	

	m_mouseX = 0.0f;
	m_mouseY = 0.0f;
}

void TestEntity::OnMouseMove(float x, float y)
{
	m_mouseX = x;
	m_mouseY = y;
}

void TestEntity::ToggleCameraControl()
{
	InputManager& manager = InputManager::Get();

	if (manager.MouseHidden())
	{
		manager.RemoveMouseListener(m_mouseHandle);
		manager.HideMouse(false);
		m_mouseX = 0.0f;
		m_mouseY = 0.0f;
	}
	else
	{
		manager.HideMouse(true);
		m_mouseHandle = manager.RegisterMouseMoveListener(this, &TestEntity::OnMouseMove);
	}	
}

void TestEntity::ShutdownEngine()
{
	Engine::Get().Shutdown();
}

void TestEntity::ForwardDirection(float value)
{
	m_zMovement = value;
}

void TestEntity::LeftAndRightMovement(float value)
{
	m_xMovement = value;
}

void TestEntity::UpAndDownMovement(float value)
{
	m_yMovement = value;
}

void TestEntity::LightBackwardAndForward(float value)
{
	m_lightBackwardAndForward = value;	
}

void TestEntity::MakeSpotLight()
{
	m_light->SetLightType(LightType::SpotLight);
}

void TestEntity::MakeDirectionalLight()
{
	m_light->SetLightType(LightType::Directional);
}

void TestEntity::MakePointLight()
{
	m_light->SetLightType(LightType::Point);
}

void TestEntity::ToggleLight()
{
	m_light->SetEnabled(!m_light->IsEnabled());
}