#pragma once
#include "MyMath.h"

class Transform
{
	Vector3 m_translation;
	Quaternion m_rotation;
	Vector3 m_scale;

	// This is the skew of the matrix.
	// This is necessary as the translation, rotation and scale can not properly represent a transform with non-uniform scale, in some circumstances at least
	// XY skew is in X, XZ skew is in Y and YZ skew is in Z
	Vector3 m_skew;
		
public:
	
	Transform() : m_translation(Vector3::Zero), m_rotation(Quaternion::Identity), m_scale(1.0f, 1.0f, 1.0f), m_skew(Vector3::Zero)
	{}

	Transform(const Vector3& translation, const Quaternion& rotation, const Vector3& scale) : m_translation(translation), m_rotation(rotation), m_scale(scale), m_skew(Vector3::Zero)
	{}

	void SetScale(const Vector3& scale)
	{
		m_scale = scale;
	}

	Vector3 GetScale() const
	{
		return m_scale;
	}

	Vector3 InverseScale() const;

	void SetRotation(const Quaternion& rotation)
	{
		m_rotation = rotation;
	}

	Quaternion GetRotation() const
	{
		return m_rotation;
	}

	void SetTranslation(const Vector3& position)
	{
		m_translation = position;
	}

	Vector3 GetTranslation() const
	{
		return m_translation;
	}

	//
	// Use with caution, the skew is used to recreate a transformation matrix with non-uniform scale
	// It is necessary for correct transformation concatenation
	// skew.x is the xy skew, skew.y is the zx skew and skew.z is the zy skew
	// The skew is used in the final transform matrix like so:
	//
	//  SkewAndScaleMat
	// [scale.X 0	    0      ]
	// [skew.X	scale.Y 0      ]  * RotationMat * TranslationMat 
	// [skew.y  skew.z  scale.Z]
	//
	void SetSkew(const Vector3& skew)
	{
		m_skew = skew;
	}

	Vector3 GetSkew() const
	{
		return m_skew;
	}

	Matrix GetMatrix() const;	
	Matrix GetUnScaledMatrix() const;

	Transform Inverse() const;

	void SetFromMatrix(const Matrix& matrix);

	Transform operator*(const Transform& transform) const;
	Transform& operator*=(const Transform& transform);

	bool SameRotation(const Quaternion& quaternion) const
	{
		return m_rotation == quaternion;
	}

	bool HasUniformScale() const;
	bool HasSkew() const;

	Vector3 TransformDirection(const Vector3& vector) const;
	Vector3 TransformPosition(const Vector3& vector) const;

	// The vector is transformed differently depending on the value of w
	// W = 1 is a position, W = 0 is a direction
	Vector4 TransformVector(const Vector4& vector) const;

	Vector3 InverseTransformDirection(const Vector3& vector) const;
	Vector3 InverseTransformPosition(const Vector3& vector) const;
	Vector4 InverseTransformVector(const Vector4& vector) const;

	Quaternion RotateQuat(const Quaternion& quaternion) const;
	Quaternion UnRotateQuat(const Quaternion& quaternion) const;

	friend bool operator==(const Transform& left, const Transform& right)
	{
		return left.m_scale == right.m_scale && left.m_rotation == right.m_rotation && left.m_translation == right.m_translation && left.m_skew == right.m_skew;
	}

	friend bool operator!=(const Transform& left, const Transform& right)
	{
		return !(left == right);
	}
};