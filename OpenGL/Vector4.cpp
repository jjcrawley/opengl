#include "Vector4.h"
#include "MyMath.h"
#include "Debugger.h"

const Vector4 Vector4::ZeroVector(0, 0, 0, 0);

Vector4::Vector4()
{
}

Vector4::Vector4(float x, float y, float z, float w): X(x), Y(y), Z(z), W(w)
{	
}

Vector4::Vector4(Vector3 vec, float w): X(vec.X), Y(vec.Y), Z(vec.Z), W(w)
{}

Vector4 Vector4::operator*(const Matrix& m) const
{
	Vector4 result;
		
	result.X = X * m.M[0][0] + Y * m.M[1][0] + Z * m.M[2][0] + W * m.M[3][0];
	result.Y = X * m.M[0][1] + Y * m.M[1][1] + Z * m.M[2][1] + W * m.M[3][1];
	result.Z = X * m.M[0][2] + Y * m.M[1][2] + Z * m.M[2][2] + W * m.M[3][2];
	result.W = X * m.M[0][3] + Y * m.M[1][3] + Z * m.M[2][3] + W * m.M[3][3];
	
	return result;
}

Vector4::operator Vector3() const
{
	return Vector3(X, Y, Z);
}

std::ostream& operator<<(std::ostream& stream, const Vector4& v)
{
	return stream << "[" << v.X << ", " << v.Y << ", " << v.Z << ", " << v.W << "]";
}