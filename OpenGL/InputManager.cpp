#include "InputManager.h"
#include "WindowManager.h"

template<typename T> T* Singleton<T>::s_pointer = nullptr;

unsigned int DelegateHandle::s_idCounter = 1;

void InputManager::Setup()
{	
	WindowManager::Get().GetCursorPosition(m_mouseX, m_mouseY);	
}

void InputManager::Update(float deltaTime)
{
	for (int i = 0; i < m_inputAxes.Size(); ++i)
	{
		m_inputAxes[i].CheckInput(this);
	}
}

Axis& InputManager::CreateAxis(const std::wstring& name)
{
	Axis axis(name);
	
	m_inputAxes.Add(axis);

	return m_inputAxes[m_inputAxes.Size() - 1];
}

Action& InputManager::CreateAction(const std::wstring& name, int keyCode, int modifiers)
{
	int size = m_inputActions.Size();

	for (int i = 0; i < size; ++i)
	{
		if (m_inputActions[i].SameName(name))
		{
			return m_inputActions[i];
		}
	}

	Action action(name, keyCode, modifiers);
	m_inputActions.Add(action);

	return m_inputActions[size];
}

void InputManager::HideMouse(bool hide)
{
	if (hide != b_mouseHidden)
	{
		b_mouseHidden = hide;
		WindowManager::Get().SetMouseHidden(b_mouseHidden);
	}
}

void InputManager::OnMouseMove(double x, double y)
{
	int size = m_mouseListeners.Size();

	for (int i = 0; i < size; ++i)
	{
		m_mouseListeners[i].Execute(float(x - m_mouseX), float(y - m_mouseY));
	}

	m_mouseX = x;
	m_mouseY = y;
}

void InputManager::OnKeyPress(int keyCode, int action, int modifiers)
{
	ActionType type = action == GLFW_RELEASE ? ActionType::Release : ActionType::Press;

	int size = m_inputActions.Size();

	for (int i = 0; i < size; ++i)
	{
		m_inputActions[i].NotifyListeners(keyCode, modifiers, type);
	}
}

void InputManager::OnScroll(double x, double y)
{
	int size = m_scrollListeners.Size();

	for (int i = 0; i < size; ++i)
	{
		m_scrollListeners[i].Execute(float(y));
	}
}

bool InputManager::KeyDown(int keyCode)
{
	return WindowManager::Get().GetKey(keyCode) == GLFW_PRESS;
}

void Axis::CheckInput(InputManager* manager)
{
	int bindingSize = m_bindings.Size();

	int size = m_listeners.Size();

	for (int i = 0; i < bindingSize; ++i)
	{
		AxisBinding& binding = m_bindings[i];

		if (manager->KeyDown(binding.Key))
		{
			for (int i = 0; i < size; ++i)
			{
				m_listeners[i].Execute(binding.Scale);
			}			

			return;
		}
	}

	for (int i = 0; i < size; ++i)
	{
		AxisBinding& binding = m_bindings[i];

		m_listeners[i].Execute(0.0f);
	}
}

void Action::RemoveBinding(DelegateHandle handle, ActionType type)
{
	if (type == ActionType::Press)
	{
		for (int i = 0; i < m_pressListeners.Size(); ++i)
		{
			if (m_pressListeners[i].SameHandle(handle))
			{
				m_pressListeners.RemoveAt(i);
				return;
			}
		}
	}
	else if (type == ActionType::Release)
	{
		for (int i = 0; i < m_pressListeners.Size(); ++i)
		{
			if (m_releaseListeners[i].SameHandle(handle))
			{
				m_releaseListeners.RemoveAt(i);
				return;
			}
		}
	}
}

void Action::NotifyListeners(int keyCode, int modifiers, ActionType type)
{
	if (keyCode == m_keyCode && modifiers == m_modifiers)
	{
		int size;

		if (type == ActionType::Press && !b_fired)
		{
			b_fired = true;
			size = m_pressListeners.Size();

			for (int i = 0; i < size; ++i)
			{
				m_pressListeners[i].Execute();
			}
		}
		else if (type == ActionType::Release && b_fired)
		{
			b_fired = false;
			size = m_releaseListeners.Size();

			for (int i = 0; i < size; ++i)
			{
				m_releaseListeners[i].Execute();
			}
		}
	}
}