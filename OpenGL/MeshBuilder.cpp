#include "MeshBuilder.h"
#include "Vector3.h"
#include "ResourceManager.h"

MeshBuilder::MeshBuilder()
{
}

MeshBuilder::~MeshBuilder()
{
}

Mesh* MeshBuilder::CubeMesh()
{
	Mesh* Cubemesh = ResourceManager::Get().CreateResource<Mesh>();

	const Array<Vector3> vertices = 
	{
		Vector3(-1.0f, -1.0f, 1.0f),  //triangle one
		Vector3(-1.0f, -1.0f, -1.0f),
		Vector3(-1.0f, 1.0f, -1.0f),
		Vector3(1.0f, 1.0f, 1.0f),	   //triangle two
		Vector3(-1.0f, -1.0f, 1.0f),
		Vector3(-1.0f, 1.0f, 1.0f),
		Vector3(1.0f,-1.0f, -1.0f),	  //triangle three
		Vector3(-1.0f,-1.0f, 1.0f),
		Vector3(1.0f,-1.0f, 1.0f),
		Vector3(1.0f, 1.0f, 1.0f),	  //triangle four
		Vector3(1.0f,-1.0f, 1.0f),
		Vector3(-1.0f,-1.0f, 1.0f),
		Vector3(-1.0f,-1.0f, 1.0f),	  //triangle five
		Vector3(-1.0f, 1.0f, -1.0f),
		Vector3(-1.0f, 1.0f, 1.0f),
		Vector3(1.0f,-1.0f, -1.0f),	  //triangle six
		Vector3(-1.0f,-1.0f, -1.0f),
		Vector3(-1.0f,-1.0f, 1.0f),
		Vector3(-1.0f, 1.0f, -1.0f),	  //triangle seven
		Vector3(-1.0f,-1.0f, -1.0f),
		Vector3(1.0f,-1.0f, -1.0f),
		Vector3(1.0f, 1.0f, -1.0f),	  //triangle eight
		Vector3(1.0f,-1.0f, 1.0f),
		Vector3(1.0f, 1.0f, 1.0f),
		Vector3(1.0f,-1.0f, 1.0f),	  //triangle nine
		Vector3(1.0f, 1.0f, -1.0f),
		Vector3(1.0f,-1.0f, -1.0f),
		Vector3(1.0f, 1.0f, -1.0f),	  //triangle ten
		Vector3(1.0f, 1.0f, 1.0f),
		Vector3(-1.0f, 1.0f, 1.0f),
		Vector3(1.0f, 1.0f, -1.0f),	  //triangle eleven
		Vector3(-1.0f, 1.0f, 1.0f),
		Vector3(-1.0f, 1.0f, -1.0f),
		Vector3(1.0f, 1.0f, -1.0f),    //triangle twelve
		Vector3(-1.0f, 1.0f, -1.0f),
		Vector3(1.0f,-1.0f, -1.0f),
	};

	const Array<Vector3> normals =
	{
		Vector3(-1.0f, 0.0f, 0.0f),		//Triangle 1
		Vector3(-1.0f, 0.0f, 0.0f),
		Vector3(-1.0f, 0.0f, 0.0f),
		Vector3(0.0f, 0.0f, 1.0f),		//Triangle 2
		Vector3(0.0f, 0.0f, 1.0f),
		Vector3(0.0f, 0.0f, 1.0f),
		Vector3(0.0f, -1.0f, 0.0f),		//Triangle 3
		Vector3(0.0f, -1.0f, 0.0f),
		Vector3(0.0f, -1.0f, 0.0f),
		Vector3(0.0f, 0.0f, 1.0f),		//Triangle 4
		Vector3(0.0f, 0.0f, 1.0f),
		Vector3(0.0f, 0.0f, 1.0f),
		Vector3(-1.0f, 0.0f, 0.0f),		//Triangle 5
		Vector3(-1.0f, 0.0f, 0.0f),
		Vector3(-1.0f, 0.0f, 0.0f),
		Vector3(0.0f, -1.0f, 0.0f),		//Triangle 6
		Vector3(0.0f, -1.0f, 0.0f),
		Vector3(0.0f, -1.0f, 0.0f),
		Vector3(0.0f, 0.0f, -1.0f),		//Triangle 7
		Vector3(0.0f, 0.0f, -1.0f),
		Vector3(0.0f, 0.0f, -1.0f),
		Vector3(1.0f, 0.0f, 0.0f),		//Triangle 8
		Vector3(1.0f, 0.0f, 0.0f),
		Vector3(1.0f, 0.0f, 0.0f),
		Vector3(1.0f, 0.0f, 0.0f),		//Triangle 9
		Vector3(1.0f, 0.0f, 0.0f),
		Vector3(1.0f, 0.0f, 0.0f),
		Vector3(0.0f, 1.0f, 0.0f),		//Triangle 10
		Vector3(0.0f, 1.0f, 0.0f),
		Vector3(0.0f, 1.0f, 0.0f),
		Vector3(0.0f, 1.0f, 0.0f),		//Triangle 11
		Vector3(0.0f, 1.0f, 0.0f),
		Vector3(0.0f, 1.0f, 0.0f),
		Vector3(0.0f, 0.0f, -1.0f),		//Triangle 12
		Vector3(0.0f, 0.0f, -1.0f),
		Vector3(0.0f, 0.0f, -1.0f),
	};

	const Array<Vector2> uvData =
	{
		Vector2(0.000059f, 1.0f - 0.000004f),
		Vector2(0.000103f, 1.0f - 0.336048f),
		Vector2(0.335973f, 1.0f - 0.335903f),
		Vector2(1.000023f, 1.0f - 0.000013f),
		Vector2(0.667979f, 1.0f - 0.335851f),
		Vector2(0.999958f, 1.0f - 0.336064f),
		Vector2(0.667979f, 1.0f - 0.335851f),
		Vector2(0.336024f, 1.0f - 0.671877f),
		Vector2(0.667969f, 1.0f - 0.671889f),
		Vector2(1.000023f, 1.0f - 0.000013f),
		Vector2(0.668104f, 1.0f - 0.000013f),
		Vector2(0.667979f, 1.0f - 0.335851f),
		Vector2(0.000059f, 1.0f - 0.000004f),
		Vector2(0.335973f, 1.0f - 0.335903f),
		Vector2(0.336098f, 1.0f - 0.000071f),
		Vector2(0.667979f, 1.0f - 0.335851f),
		Vector2(0.335973f, 1.0f - 0.335903f),
		Vector2(0.336024f, 1.0f - 0.671877f),
		Vector2(1.000004f, 1.0f - 0.671847f),
		Vector2(0.999958f, 1.0f - 0.336064f),
		Vector2(0.667979f, 1.0f - 0.335851f),
		Vector2(0.668104f, 1.0f - 0.000013f),
		Vector2(0.335973f, 1.0f - 0.335903f),
		Vector2(0.667979f, 1.0f - 0.335851f),
		Vector2(0.335973f, 1.0f - 0.335903f),
		Vector2(0.668104f, 1.0f - 0.000013f),
		Vector2(0.336098f, 1.0f - 0.000071f),
		Vector2(0.000103f, 1.0f - 0.336048f),
		Vector2(0.000004f, 1.0f - 0.671870f),
		Vector2(0.336024f, 1.0f - 0.671877f),
		Vector2(0.000103f, 1.0f - 0.336048f),
		Vector2(0.336024f, 1.0f - 0.671877f),
		Vector2(0.335973f, 1.0f - 0.335903f),
		Vector2(0.667969f, 1.0f - 0.671889f),
		Vector2(1.000004f, 1.0f - 0.671847f),
		Vector2(0.667979f, 1.0f - 0.335851f)
	};

	const Array<unsigned int> indices =
	{
		0,1,2,
		3,4,5,
		6,7,8,
		9,10,11,
		12,13,14,
		15,16,17,
		18,19,20,
		21,22,23,
		24,25,26,
		27,28,29,
		30,31,32,
		33,34,35
	};	

	Cubemesh->SetVertices(vertices);
	Cubemesh->SetUVs(uvData);
	Cubemesh->SetIndices(indices);
	Cubemesh->SetNormals(normals);

	return Cubemesh;
}