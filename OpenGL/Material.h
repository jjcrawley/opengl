#pragma once
#include "MaterialPropertyList.h"
#include "ShaderHelpers.h"
#include "ResourceManager.h"

class Material : public BaseResource
{
	enum class PropertyType
	{
		Float,
		Int,
		Texture,
		Bool,
		Vector3,
		Vector4,
		Unknown
	};

	struct MaterialProperty
	{
		MatVariable m_handle;
		std::string m_name;
		unsigned int m_location;
		PropertyType m_type;
		
		MaterialProperty() : 
			m_name(""), 
			m_location(0), 
			m_type(PropertyType::Unknown)
		{}
	};
		
	unsigned int m_program;
	
	Array<MaterialProperty> m_properties;

	inline MaterialProperty* GetVariable(const char* name)
	{
		int size = m_properties.Size();

		while (size--)
		{
			if (name == m_properties[size].m_name)
			{
				return &m_properties[size];
			}
		}

		return nullptr;
	}

	inline MaterialProperty* GetVariable(MatVariable handle)
	{
		union
		{
			struct
			{
				unsigned short index;
				unsigned short location;
			};

			unsigned int ID;
		};

		ID = handle;

		if (m_properties.InRange(index))
		{
			MaterialProperty& var = m_properties[index];

			if (handle == var.m_handle)
			{
				return &var;
			}
		}

		return nullptr;
	}

	MaterialPropertyList* m_propertiesList;

	void PopulateParameterList(unsigned int program);
	
public:	
		
	Material() : m_program(0) {}
	~Material();
	unsigned int Program() const;
	void SetInt(const char* var, int value);
	void SetFloat(const char* var, float value);
	void SetFloat(MatVariable handle, float value);
	void SetBool(const char* var, bool value);
	void SetTexture(MatVariable handle, Texture* texture);
	void SetVector(const char* var, const Vector3& vector);
	void SetVector(MatVariable handle, const Vector3& vector);
	void SetVector(const char* var, const Vector4& vector);
	void SetVector(MatVariable handle, const Vector4& vector);
	void SetColour(const char* var, const Colour& colour);
	void SetColour(MatVariable handle, const Colour& colour);
	MatVariable GetVariableHandle(const char* var) const;
	void ApplyMaterial();
	virtual void Release() override;
		
	const MaterialPropertyList* GetPropertyList() const
	{
		return m_propertiesList;
	}

	MaterialPropertyList* GetPropertyList()
	{
		return m_propertiesList;
	}

	MaterialPropertyList* GetPropertyListCopy() const;	
		
	static Material* LoadMaterial(const TCHAR* vertexShader, const TCHAR* fragmentShader);

private:
	void Initialise(unsigned int program);
};