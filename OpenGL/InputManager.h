#pragma once

#include <string>
#include "Array.h"
#include "Delegate.h"
#include "Singleton.h"
#include "BaseResource.h"
#include "GLFWInput.h"

//TODO: keep working on this class
//TODO: See about extracting input processing functionality to independent listeners, IInputHandlers that can be plugged into the input system, removing the dependencies on the input manager

class InputManager;

typedef Delegate<void, float> AxisFunction;
typedef Delegate<void, float, float> MouseFunction;
typedef Delegate<void> ActionFunction;

struct AxisBinding
{
	int Key;
	float Scale;

	AxisBinding() : Key(0), Scale(0.0f)
	{}

	AxisBinding(int key, float scale) : Key(key), Scale(scale)
	{}
};

// An Axis is an object that stores an array of function listeners and axis bindings.
// Each axis is evaluated once every frame and the listeners will be called with different parameters depending on the key that is held.
class Axis
{
private:
	std::wstring m_name;	
	Array<AxisFunction> m_listeners;
	Array<AxisBinding> m_bindings;
public:
	Axis() {}

	Axis(const std::wstring& name) : m_name(name)
	{}

	Axis(const Axis& axis) : m_name(axis.m_name), m_listeners(axis.m_listeners), m_bindings(axis.m_bindings)
	{}

	template<typename Class>
	DelegateHandle RegisterListener(Class* object, typename MemFPtr<Class, void, float>::Func function)
	{
		AxisFunction listener;
		listener.BindObject(object, function);
		m_listeners.Add(listener);
						
		return listener.GetHandle();
	}

	DelegateHandle RegisterListener(typename FPtr<void, float>::Func function)
	{
		AxisFunction listener;
		listener.BindFunction(function);
		m_listeners.Add(listener);
		
		return listener.GetHandle();
	}

	template<typename Functor>
	DelegateHandle RegisterListener(Functor functor)
	{
		AxisFunction function;
		function.BindFunctor(functor);

		m_listeners.Add(function);

		return function.GetHandle();
	}

	template<typename Class>
	void RemoveListener(Class* object, typename MemFPtr<Class, void, float>::Func function)
	{
		for (int i = 0; i < m_listeners.Size(); ++i)
		{
			if (m_listeners[i].SameFunction(object, function))
			{
				m_listeners.RemoveAt(i);
				return;
			}
		}
	}
	
	void RemoveListener(typename FPtr<void, float>::Func function)
	{
		for (int i = 0; i < m_listeners.Size(); ++i)
		{
			if (m_listeners[i].SameFunction(function))
			{
				m_listeners.RemoveAt(i);
				return;
			}
		}	
	}

	void RemoveListener(DelegateHandle handle)
	{
		for (int i = 0; i < m_listeners.Size(); ++i)
		{
			if (m_listeners[i].SameHandle(handle))
			{
				m_bindings.RemoveAt(i);
				break;
			}
		}
	}

	void AddBinding(int key, float scale)
	{
		int size = m_bindings.Size();

		bool isDuplicate = false;

		for (int i = 0; i < size; ++i)
		{
			const AxisBinding& binding = m_bindings[i];

			if (binding.Key == key)
			{
				isDuplicate = true;
				break;
			}
		}

		if (!isDuplicate)
		{
			m_bindings.Add(AxisBinding(key, scale));
		}
	}

	void RemoveBinding(int key)
	{
		for (int i = m_bindings.Size() - 1; i >= 0; --i)
		{
			const AxisBinding& binding = m_bindings[i];

			if (binding.Key == key)
			{
				m_bindings.RemoveAt(i);
				break;
			}
		}
	}

	void CheckInput(InputManager* manager);	

	inline bool SameName(const std::wstring& name) const
	{
		return m_name == name;
	}
};

enum class ActionType
{
	Release,
	Press
};

// An action is an object similar to the axis, except the listeners are only invoked when a particular input event is processed.
// The two primary events are when a key is pressed and when a key is released
class Action
{
	std::wstring m_name;
	Array<ActionFunction> m_pressListeners;
	Array<ActionFunction> m_releaseListeners;
	int m_keyCode;
	int m_modifiers;

	bool b_fired = false;
public:
	Action() : m_name(L""), m_keyCode(0), m_modifiers(0)
	{}

	Action(const std::wstring& name, int keyCode, int modifiers) : m_name(name), m_keyCode(keyCode), m_modifiers(modifiers)
	{}

	Action(const Action& action) : m_name(action.m_name), 
		m_pressListeners(action.m_pressListeners), 
		m_releaseListeners(action.m_releaseListeners),
		m_keyCode(action.m_keyCode),
		m_modifiers(action.m_modifiers)
	{}	

	void AddBinding(ActionFunction function, ActionType type)
	{			
		switch (type)
		{
			case ActionType::Press:
				m_pressListeners.Add(function);
				break;
			case ActionType::Release:
				m_releaseListeners.Add(function);
				break;
		}
	}

	template<typename Object>
	DelegateHandle AddObjectBinding(Object* object, typename ActionFunction::MemberFunction<Object> function, ActionType type)
	{
		ActionFunction func;
		func.BindObject(object, function);
		
		AddBinding(func, type);

		return func.GetHandle();
	}

	DelegateHandle AddFunctionBinding(typename ActionFunction::CFunction function, ActionType type)
	{
		ActionFunction func;
		func.BindFunction(function);

		AddBinding(func, type);

		return func.GetHandle();
	}

	template<typename Functor>
	DelegateHandle AddFunctorBinding(Functor functor, ActionType type)
	{
		ActionFunction function;
		function.BindFunctor(functor);

		AddBinding(function, type);

		return function.GetHandle();
	}

	void RemoveBinding(DelegateHandle handle, ActionType type);	

	void NotifyListeners(int keyCode, int modifiers, ActionType type);

	inline bool SameName(const std::wstring& name)
	{
		return m_name == name;
	}
};

class InputManager : public Singleton<InputManager>
{
private:
	Array<Axis> m_inputAxes;
	Array<Action> m_inputActions;
public:
	
	void Setup();
	void Update(float deltaTime);
	
	Axis& CreateAxis(const std::wstring& name);	

	Axis* GetAxis(const std::wstring& name)
	{
		int size = m_inputAxes.Size();

		for (int i = 0; i < size; ++i)
		{
			if (m_inputAxes[i].SameName(name))
			{
				return &m_inputAxes[i];
			}
		}

		return nullptr;
	}

	Action& CreateAction(const std::wstring& name, int keyCode, int modifiers);
	
	Action* GetAction(const std::wstring& name)
	{
		int size = m_inputActions.Size();

		for (int i = 0; i < size; ++i)
		{
			if (m_inputActions[i].SameName(name))
			{
				return &m_inputActions[i];
			}
		}

		return nullptr;
	}
	
	// Stuff for handling mouse movement and listeners
private:
	double m_mouseX, m_mouseY;	
	bool b_mouseHidden = false;
	Array<MouseFunction> m_mouseListeners;
	Array<AxisFunction> m_scrollListeners;
public:
	template<typename Class>
	DelegateHandle RegisterMouseMoveListener(Class* object, MouseFunction::MemberFunction<Class> func)
	{
		MouseFunction function;
		function.BindObject(object, func);

		m_mouseListeners.Add(function);

		return function.GetHandle();
	}
	
	DelegateHandle RegisterMouseMoveListener(MouseFunction::CFunction func)
	{
		MouseFunction function;
		function.BindFunction(func);
		m_mouseListeners.Add(function);
				
		return function.GetHandle();
	}

	void RemoveMouseListener(MouseFunction::CFunction func)
	{
		int size = m_mouseListeners.Size();

		for (int i = 0; i < size; ++i)
		{
			if (m_mouseListeners[i].SameFunction(func))
			{
				m_mouseListeners.RemoveAt(i);
				return;
			}
		}
	}

	void RemoveMouseListener(DelegateHandle handle)
	{
		int size = m_mouseListeners.Size();

		for (int i = 0; i < size; ++i)
		{
			if (m_mouseListeners[i].SameHandle(handle))
			{
				m_mouseListeners.RemoveAt(i);
				return;
			}
		}
	}

	template<typename Class>
	DelegateHandle RegisterScrollListener(Class* object, AxisFunction::MemberFunction<Class> function)
	{
		AxisFunction func;
		func.BindObject(object, function);

		m_scrollListeners.Add(func);
		return func.GetHandle();
	}

	DelegateHandle RegisterScrollListener(AxisFunction::CFunction function)
	{
		AxisFunction func;
		func.BindFunction(function);

		m_scrollListeners.Add(func);
		return func.GetHandle();
	}

	void RemoveScrollListener(DelegateHandle handle)
	{
		int size = m_scrollListeners.Size();

		for (int i = 0; i < size; ++i)
		{
			if (m_scrollListeners[i].SameHandle(handle))
			{
				m_scrollListeners.RemoveAt(i);
				return;
			}
		}
	}

	void HideMouse(bool hide);

	inline bool MouseHidden() const
	{
		return b_mouseHidden;
	}

	// Input wrapper methods for interfacing with the input sub systems, GLFW input handling in this case

	void OnMouseMove(double x, double y);	
	void OnKeyPress(int keyCode, int action, int modifiers);
	void OnScroll(double x, double y);
	bool KeyDown(int keyCode);
};

#define GInputManager InputManager::Get()