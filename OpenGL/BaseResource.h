#pragma once

#include "Object.h"
#include "HandleHelpers.h"

class ResourceManager;
class BaseResource;

typedef Handle<BaseResource> ResourceHandle;

class BaseResource : public Object
{
	ResourceHandle m_handle;

	friend class ResourceManager;

	void SetID(ResourceHandle handle)
	{
		m_handle = handle;
	}

public:

	BaseResource() : m_handle(0)
	{}

	virtual ~BaseResource()
	{
#ifndef _DEBUG
		m_handle = ResourceHandle(0);
#endif
	}

	virtual void Release() = 0;

	unsigned int GetID() const
	{
		return m_handle.GetID();
	}
};