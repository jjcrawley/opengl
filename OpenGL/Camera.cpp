#include "Camera.h"

Matrix Camera::GetViewMatrix() const
{
	return m_viewMat;
}

Matrix Camera::GetViewProjectionMatrix() const
{
	return m_viewMat * m_projectionMat;
}

void Camera::PrepCamera()
{
	if (b_updateProjection)
	{
		if (m_type == CameraType::Perspective)
		{
			m_projectionMat = MatrixFactory::PerspectiveLH(m_fov, m_aspectRatio, m_near, m_far);
		}
		else
		{
			m_projectionMat = MatrixFactory::OrthographicLHAspect(m_orthoWidth, m_aspectRatio, m_near, m_far);
		}	
				
		b_updateProjection = false;
	}
	
	m_viewMat = MatrixFactory::LookToLH(m_relativeRotation.Forward(), m_relativeRotation.Up(), m_relativePosition);	
}

Matrix Camera::GetProjectionMatrix() const
{
	return m_projectionMat;
}