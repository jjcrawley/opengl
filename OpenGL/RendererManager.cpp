#include "RendererManager.h"
#include "Material.h"
#include "RenderableMesh.h"
#include "Camera.h"

RendererManager* Singleton<RendererManager>::s_pointer = nullptr;
const GLuint RendererManager::s_matrixBufferIndex = 1;
const GLuint RendererManager::s_cameraBufferIndex = 3;

RendererManager::RendererManager()
{
}

RendererManager::~RendererManager()
{
}

void RendererManager::Initialise()
{
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);

	glDepthFunc(GL_LESS);

	GLuint buffers[2];

	glGenBuffers(2, buffers);

	m_matrixBuffer = buffers[0];
	glBindBuffer(GL_UNIFORM_BUFFER, m_matrixBuffer);
	glBufferData(GL_UNIFORM_BUFFER, sizeof(Matrix) * 2, nullptr, GL_DYNAMIC_DRAW);

	m_cameraBuffer = buffers[1];
	glBindBuffer(GL_UNIFORM_BUFFER, m_cameraBuffer);
	glBufferData(GL_UNIFORM_BUFFER, sizeof(Vector4), nullptr, GL_DYNAMIC_DRAW);

	//m_mainCamera = new Camera();
	//m_mainCamera->SetAspect(1024.0f, 768.0f);
	//m_mainCamera->SetFar(100.0f);
	//m_mainCamera->SetNear(0.1f);
	//m_mainCamera->SetFOV(FMath::DegToRad(57.9f));

	//m_perspectiveMatrix = MatrixFactory::PerspectiveLH(FMath::DegToRad(57.9f), 1024.0f, 768.0f, 0.1f, 100.0f);
}

void RendererManager::RegisterRenderer(RenderableMesh* renderer)
{
	if (renderer)
	{
		m_renderers.Add(renderer);
	}
}

void RendererManager::DeregisterRenderer(RenderableMesh* renderer)
{
	if (!renderer)
	{
		return;
	}

	int size = m_renderers.Size();

	for (int i = 0; i < size; ++i)
	{
		if (renderer == m_renderers[i])
		{
			m_renderers.RemoveAt(i, false);
			return;
		}
	}
}

void RendererManager::SetupMaterial(Material* material)
{
	GLuint blockIndex;

	blockIndex = glGetUniformBlockIndex(material->Program(), "Matrices");

	if (blockIndex != GL_INVALID_INDEX)
	{
		glUniformBlockBinding(material->Program(), blockIndex, s_matrixBufferIndex);
		glBindBufferBase(GL_UNIFORM_BUFFER, s_matrixBufferIndex, m_matrixBuffer);
	}

	blockIndex = glGetUniformBlockIndex(material->Program(), "CameraBlock");

	if (blockIndex != GL_INVALID_INDEX)
	{
		glUniformBlockBinding(material->Program(), blockIndex, s_cameraBufferIndex);
		glBindBufferBase(GL_UNIFORM_BUFFER, s_cameraBufferIndex, m_cameraBuffer);
	}
}

void RendererManager::SetMainCamera(Camera* camera)
{
	if (camera)
	{
		m_mainCamera = camera;
	}
}

void RendererManager::UpdateMatrixBuffer(const Matrix& MVP, const Matrix& world)
{
	glBindBuffer(GL_UNIFORM_BUFFER, m_matrixBuffer);
	glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(Matrix), &MVP);
	glBufferSubData(GL_UNIFORM_BUFFER, sizeof(Matrix), sizeof(Matrix), &world);	
}

void RendererManager::Render()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	if (m_mainCamera)
	{
		m_mainCamera->PrepCamera();

		Matrix viewProjection = m_mainCamera->GetViewProjectionMatrix();

		Vector3 cameraPosition = m_mainCamera->GetRelativePosition();

		glBindBuffer(GL_UNIFORM_BUFFER, m_cameraBuffer);
		glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(Vector3), &cameraPosition);

		int size = m_renderers.Size();

		for (int i = 0; i < size; ++i)
		{			
			m_renderers[i]->Render(viewProjection);			
		}
	}
}

void RendererManager::TidyUp()
{
	glDeleteBuffers(1, &m_matrixBuffer);

	m_renderers.Clear();

	//delete m_mainCamera;
}

Camera* RendererManager::CurrentCamera()
{
	return m_mainCamera;
}