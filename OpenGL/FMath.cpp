#include "FMath.h"
#include "Vector3.h"
#include "Vector2.h"
#include "Quaternion.h"

using namespace std;

// A utility method for checking whether a point, X, is visible from point, Pos,
// using an FOV angle and vector Direction that specifies the direction the point is facing
bool FMath::InFOV(float FOV, const Vector3& Pos, const Vector3& Direction, const Vector3& X)
{
	float cosTheta;
	float cosFov = cosf(FOV / 2.0f);
	Vector3 NormPos = Direction.Normalise();
	Vector3 toX = X - Pos;		
		
	toX = toX.Normalise();
		
	cosTheta = toX.Dot(NormPos);
		
	if (cosFov < cosTheta)
	{
		return true;
	}
	
	return false;
}

void FMath::PolarToCartesian2D(float Angle, float Radius, Vector2& Result)
{
	if (Radius == 0)
	{
		Result.X = 0;
		Result.Y = 0;
		return;
	}

	Result = Vector2();

	Result.X = Radius * cosf(Angle);
	Result.Y = Radius * sinf(Angle);
}

void FMath::CartesianToPolar2D(const Vector2& Coord, float& Angle, float& Radius)
{
	Radius = Coord.X * Coord.X + Coord.Y * Coord.Y;
	
	if(Radius > 0)
	{
		Radius = sqrtf(Radius);
		Angle = atan2f(Coord.Y, Coord.X);
	}
	else
	{
		Radius = 0;
		Angle = 0;
	}
}

void FMath::CylindricalToCartesian(float Angle, float Radius, float Height, Vector3& Result)
{
	Result = Vector3();

	Result.Z = Height;
	Result.X = Radius * cosf(Angle);
	Result.Y = Radius * sinf(Angle);
}

void FMath::CartesianToCylindrical(const Vector3& Coord, float& Radius, float& Angle, float& Height)
{
	Height = Coord.Z;

	Vector2 vec(Coord.X, Coord.Y);

	CartesianToPolar2D(vec, Angle, Radius);
}

void FMath::SphericalToCartesian(float Heading, float Pitch, float Radius, Vector3& Result)
{
	// The default implementation of this is to use the following formula:
	// X = Radius * cos(pitch) * sin(heading)
	// Y = Radius * sin(pitch) * sin(heading)
	// Z = Radius * cos(pitch)
	// In this particular implementation, Y is treated as up, as opposed to Z like the default

	if (Radius == 0)
	{
		Result = Vector3::Zero;
		return;
	}
	else if (fabs(Pitch) == HALF_PI)
	{
		Result.X = 0;
		Result.Y = Radius;
		Result.Z = 0;
		return;
	}

	Result = Vector3();

	Result.X = Radius * cosf(Pitch) * sinf(Heading);

	Result.Y = -Radius * sinf(Pitch);
	
	Result.Z = Radius * cosf(Pitch) * cosf(Heading);
}

Vector3 FMath::SphericalToCartesianUnit(float Heading, float Pitch)
{
	Vector3 result;

	result.X = cosf(Pitch) * sinf(Heading);
	result.Y = -sinf(Pitch);
	result.Z = cosf(Pitch) * cosf(Heading);

	return result;
}

void FMath::CartesianToSpherical(const Vector3& Coord, float& Heading, float& Pitch, float& Radius)
{
	Radius = Coord.Length();

	// If the radius is 0 then it is the origin and doesn't have a heading or pitch
	if (Radius == 0)
	{
		Heading = Pitch = 0;
		return;
	}

	Pitch = asinf(-Coord.Y / Radius);

	// If pitch < Half_Pi then it isn't pointing straight up.
	// If it is pointing straight up then the heading should be zero
	Heading = fabs(Pitch) < HALF_PI ? atan2f(Coord.X, Coord.Z) : 0;
}

void FMath::CartesianToSphericalUnit(const Vector3& direction, float& heading, float& pitch)
{	
	if (direction.IsNormalised())
	{
		pitch = asinf(-direction.Y);

		heading = fabs(pitch) < HALF_PI ? atan2f(direction.X, direction.Z) : 0;
	}
	else
	{
		float radius;

		CartesianToSpherical(direction, heading, pitch, radius);
	}
}

float FMath::WrapAngleRadians(float Rad)
{
	if (fabs(Rad) <= PI)
	{
		return Rad - floorf((Rad + PI) / TWO_PI) * TWO_PI;
	}
	
	return Rad;
}