#pragma once

struct Quaternion;
struct Vector3;
struct Vector4;
struct Vector2;
struct Matrix;
struct ERotation;
struct Colour;

class Transform;
class MatrixFactory;