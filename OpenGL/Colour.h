#pragma once
struct Colour
{
public:

	float R;
	float G;
	float B;
	float A;

	Colour() = default;

	Colour(float r, float g, float b, float a) : R(r), G(g), B(b), A(a)
	{}

	Colour operator*(float scale) const
	{
		return Colour(
			R * scale, 
			G * scale, 
			B * scale, 
			A * scale
			);
	}

	Colour& operator*=(float scale)
	{
		R *= scale;
		G *= scale;
		B *= scale;
		A *= scale;

		return *this;
	}

	Colour operator/(float scale) const
	{
		float oneOverScale = 1 / scale;

		return Colour(
			R * oneOverScale, 
			G * oneOverScale, 
			B * oneOverScale, 
			A * oneOverScale
			);
	}

	Colour& operator/=(float scale)
	{
		float oneOverScale = 1 / scale;

		R *= oneOverScale;
		G *= oneOverScale;
		B *= oneOverScale;
		A *= oneOverScale;

		return *this;
	}

	Colour operator*(const Colour& colour) const
	{
		return Colour(
			R * colour.R,			
			G * colour.G,
			B * colour.B,
			A * colour.A
			);
	}

	Colour& operator*=(const Colour& colour)
	{
		R *= colour.R;
		G *= colour.G;
		B *= colour.B;
		A *= colour.A;

		return *this;
	}

	Colour operator/(const Colour& colour) const
	{
		return Colour(
			R / colour.R,
			G / colour.G,
			B / colour.B,
			A / colour.A
			);
	}	

	Colour& operator/=(const Colour& colour)
	{
		R /= colour.R;
		G /= colour.G;
		B /= colour.B;
		A /= colour.A;

		return *this;
	}

	Colour operator+(const Colour& colour)
	{
		return Colour(
			R + colour.R,
			G + colour.G,
			B + colour.B,
			A + colour.A
			);
	}

	Colour& operator+=(const Colour& colour)
	{
		R += colour.R;
		G += colour.G;
		B += colour.B;
		A += colour.A;

		return *this;
	}

	Colour operator-(const Colour& colour)
	{
		return Colour(
			R - colour.R,
			G - colour.G,
			B - colour.B,
			A - colour.A
			);
	}

	Colour& operator-=(const Colour& colour)
	{
		R -= colour.R;
		G -= colour.G;
		B -= colour.B;
		A -= colour.A;

		return *this;
	}
};