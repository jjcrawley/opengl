#pragma once
#include <utility>
#include "Dereferencer.h"

template<typename T = void>
struct LessThan
{
	constexpr bool operator()(const T& left, const T& right) const
	{
		return left < right;
	}
};

template<>
struct LessThan<void>
{
	template<typename T, typename U>
	constexpr bool operator()(const T& left, const U& right) const
	{
		return left < right;
	}
};

template<typename T = void>
struct GreaterThan
{
	bool operator()(const T& left, const T& right) const
	{
		return left > right;
	}
};

template<>
struct GreaterThan<void>
{
	template<typename T, typename U>
	constexpr bool operator()(const T& left, const U& right) const
	{
		return left > right;
	}
};

template<typename T = void>
struct EqualTo
{
	inline bool operator()(const T& left, const T& right) const
	{
		return left == right;
	}
};

template<>
struct EqualTo<void>
{
	template<typename T, typename U>
	constexpr bool operator()(const T& left, const U& right)const
	{
		return left == right;
	}
};

enum class Dereference
{
	True
};

template<typename Predicate>
struct DereferencePredicate
{
	const Predicate& pred;
		
	constexpr DereferencePredicate(const Predicate& predicate) : pred(predicate)
	{}
		
	template<typename T, typename U>
	bool operator()(T& left, U& right)
	{
		typedef Dereferencer<T, true> DerefT;
		typedef Dereferencer<U, true> DerefU;

		return pred(DerefT()(left), DerefU()(right));
	}

	template<typename T, typename U>
	bool operator()(const T& left, const U& right) const
	{
		typedef Dereferencer<T, true> DerefT;
		typedef Dereferencer<U, true> DerefU;

		return pred(DerefT()(left), DerefU()(right));
	}

	template<typename T>
	bool operator()(T& left, T& right)
	{
		typedef Dereferencer<T, true> DerefT;

		return pred(DerefT()(left), DerefT()(right));
	}

	template<typename T>
	bool operator()(const T& left, const T& right) const
	{
		typedef Dereferencer<T, true> DerefT;

		return pred(DerefT()(left), DerefT()(right));
	}
};

template<typename Predicate>
auto DeferenceWrapper(Predicate&& pred)
{
	return DereferencePredicate<Predicate>(std::forward<Predicate>(pred));
}

template<typename T>
void Swap(T& valOne, T& valTwo)
{
	T temp = std::move(valOne);
	valOne = std::move(valTwo);
	valTwo = std::move(temp);
}

namespace Algorithm
{
	// GCD is Greatest Common Divisor
	template<typename T>
	T GCD(T a, T b)
	{
		T mod = 0;

		while (b != 0)
		{
			mod = a % b;
			a = b;
			b = mod;
		}

		return a;
	}

	template<typename T>
	void RotateArrayLeft(T values[], int count, int rotateBy)
	{
		int gcd = GCD(count, rotateBy);

		for (int i = 0; i < gcd; ++i)
		{
			T temp = values[i];

			int currentIndex = i;

			while (true)
			{
				// Wrap the moveToIndex
				int moveToIndex = (currentIndex + rotateBy) % count;

				if (moveToIndex == i)
				{
					break;
				}

				// Replace the currentIndex with the value at moveToIndex
				values[currentIndex] = values[moveToIndex];
				currentIndex = moveToIndex;
			}

			values[currentIndex] = temp;
		}
	}

	// A safe version of RotateArrayLeft.
	// This version allows for negative rotations and will perform some checks to try and avoid unexpected behaviour
	template<typename T>
	void RotateArray(T values[], int count, int rotateBy)
	{
		if (rotateBy == 0 || count <= 1)
		{
			return;
		}

		rotateBy %= count;

		// Check to see if rotateBy is negative, this indicates a right rotation
		if (rotateBy < 0)
		{
			// Convert the rotation into a positive left rotation
			rotateBy = count + rotateBy;
		}

		Algorithm::RotateArrayLeft(values, count, rotateBy);
	}
}