#pragma once
#include "Dereferencer.h"
#include "Sorting.h"

namespace Search
{
	template<typename T, typename U, typename Predicate>
	int LowerBound(const T* values, int count, const U& value, const Predicate& pred)
	{			
		int start = 0;
		int mid = 0;

		while (count > 0)
		{
			count /= 2;

			mid = start + count;

			// Check the upper half of the array
			if (pred(values[mid], value))
			{
				start = ++mid;
				// - 1 to consume mid
				count -= 1;
			}
		}

		return start;
	}

	template<Dereference, typename T, typename U, typename Predicate = LessThan<>>
	int LowerBound(const T* values, int count, const U& value, const Predicate& pred = Predicate())
	{
		DereferencePredicate<Predicate> predicate(pred);

		//SearchPredicate<T, U, dereference, Predicate> predicate(pred);

		return Search::LowerBound(values, count, value, predicate);
	}
		
	template<typename T>
	int LowerBound(const T* values, int count, const T& value)
	{
		return Search::LowerBound(values, count, value, LessThan<T>());
	}
	
	template<typename T, typename U, typename Predicate>
	int UpperBound(const T* values, int count, const U& value, const Predicate& pred)
	{		
		int start = 0;
		int mid = 0;

		while (count > 0)
		{
			count /= 2;

			mid = start + count;

			if (!pred(value, values[mid]))
			{
				start = ++mid;
				count -= 1;
			}
		}

		return start;
	}

	template<Dereference, typename T, typename U, typename Predicate = LessThan<>>
	int UpperBound(const T* values, int count, const U& value, const Predicate& pred = Predicate())
	{
		DereferencePredicate<Predicate> predicate(pred);

		//SearchPredicate<T, U, dereference, Predicate> predicate(pred);

		return Search::UpperBound(values, count, value, predicate);
	}
		
	template<typename T>
	int UpperBound(const T* values, int count, const T& value)
	{
		return Search::UpperBound(values, count, value, LessThan<T>());
	}

	template<typename T, typename U, typename Predicate>
	int BinarySearch(const T* values, int count, const U& value, const Predicate& pred)
	{			
		int index = Search::LowerBound(values, count, value, pred);

		if (index < count && !pred(values[index], value))
		{
			return index;
		}
		else
		{
			return -1;
		}
	}

	template<Dereference, typename T, typename U, typename Predicate = LessThan<>>
	int BinarySearch(const T* values, int count, const U& value, const Predicate& pred = Predicate())
	{
		DereferencePredicate<Predicate> predicate(pred);
				
		return Search::BinarySearch(values, count, value, predicate);
	}
		
	template<typename T>
	int BinarySearch(const T* values, int count, const T& value)
	{
		return Search::BinarySearch(values, count, value, LessThan<T>());
	}

	template<typename T, typename U, typename Predicate>
	int LinearSearch(const T* values, int count, const U& value, const Predicate& pred)
	{		
		for (int i = 0; i < count; ++i)
		{
			if (pred(values[i], value))
			{
				return i;
			}
		}

		return -1;
	}

	template<Dereference, typename T, typename U, typename Predicate = EqualTo<>>
	int LinearSearch(const T* values, int count, const U& value, const Predicate& pred = Predicate())
	{
		DereferencePredicate<Predicate> predicate(pred);

		//SearchPredicate<T, U, dereference, Predicate> predicate(pred);

		return Search::LinearSearch(values, count, value, predicate);
	}
		
	template<typename T>
	int LinearSearch(const T* values, int count, const T& value)
	{
		return Search::LinearSearch(values, count, value, EqualTo<T>());
	}
}