#pragma once

#include "Mathfwd.h"

enum class AxisType
{
	X = 0,
	Y = 1,
	Z = 2,
	Forward = 2,
	Up = 1,
	Right = 0
};

struct Vector3
{
	static const Vector3 Zero;
	static const Vector3 OneVector;
	static const Vector3 Right;
	static const Vector3 Forward;
	static const Vector3 Up;
	static const Vector3 Left;
	static const Vector3 Down;
	static const Vector3 Back;

	float X;
	float Y;
	float Z;

	Vector3() {}
	Vector3(float x, float y, float z) : X(x), Y(y), Z(z)
	{}

	void Set(float x, float y, float z);

	Vector3 Normalise() const;
	bool IsNormalised() const;
	bool IsZero() const;
	bool IsUniform() const;
	float Length() const;
	float SquaredLength() const;
	
	float Dot(const Vector3& right) const;

	Vector3 ProjectOnto(const Vector3& right) const;
	Vector3 Perpendicular(const Vector3& right) const;
	
	Vector3 Cross(const Vector3& crossWith) const;

	Vector3 operator+ (const Vector3& right) const;
	Vector3 operator- (const Vector3& right) const;
	Vector3& operator+= (const Vector3& right);
	Vector3& operator-= (const Vector3& right);
	Vector3 operator* (const Vector3& r) const;
	Vector3 operator/ (const Vector3& r) const;
	Vector3& operator*= (const float scale);
	Vector3& operator/= (const float scale);
	inline Vector3 operator* (const float scale) const;
	Vector3 operator/ (const float scale) const;
	Vector3 operator-() const;
	ERotation ToRotation() const;
	Quaternion ToQuaternion() const;
	Quaternion ToQuaternion(AxisType axis) const;
	
	bool operator==(const Vector3& right) const
	{
		return X == right.X && Y == right.Y && Z == right.Z;
	}

	bool operator!=(const Vector3& right) const
	{
		return !(*this == right);
	}
		
	static Vector3 RotateVector(const Vector3& axis, float angle, const Vector3& v);
	static Vector3 Reflect(const Vector3& v, const Vector3& normal);
};