#include "Texture.h"
#include "Vector3.h"
#include <gl/glew.h>
#include "TextureHelpers.h"
#include "ResourceManager.h"

Texture* Texture::s_whiteTexture = nullptr;

void Texture::Initialise(unsigned int programID, TextureType textureType)
{
	m_textureID = programID;
	m_type = textureType;
}

void Texture::Release()
{
	glDeleteTextures(1, &m_textureID);
	m_textureID = 0;
}

Texture* Texture::LoadTexture(const TCHAR* filePath, TextureType type, bool flip)
{
	Texture* texture = ResourceManager::Get().CreateResource<Texture>();

	unsigned int textureID = 0;

	switch (type)
	{
		case TextureType::BMP:
			textureID = TextureHelpers::LoadBMP(filePath);
			break;
		case TextureType::DDS:
			textureID = TextureHelpers::LoadDDS(filePath, flip);
			break;
		case TextureType::TGA:
			textureID = TextureHelpers::LoadTGA(filePath);
			break;
		default:
			break;
	}

	texture->Initialise(textureID, type);

	return texture;
}

Texture* Texture::WhiteTexture()
{
	if (s_whiteTexture == nullptr)
	{
		s_whiteTexture = ResourceManager::Get().CreateResource<Texture>();
		
		unsigned int textureID = TextureHelpers::CreateSinglePixelColouredTexture(Vector3(1.0f, 1.0f, 1.0f));
		s_whiteTexture->Initialise(textureID, TextureType::Unknown);
	}

	return s_whiteTexture;
}