#pragma once
#include "Macros.h"
#include "StringHelpers.h"

class Debug
{
public:
	static void LogAssert(const TCHAR* message, int line, const TCHAR* function, const TCHAR* file);
	static void LogCheck(const TCHAR* message, int line, const TCHAR* function, const TCHAR* file);
	static bool LogCheck(bool condition, const TCHAR* message, int line, const TCHAR* function, const TCHAR* file);
};

#ifdef TestingTemplates

#define Assert(condition, message)
#define Check (condition, message)
#define CheckOnce(condition, message)
#define IfCheck(condition, message) (condition)

#else
#define Assert(condition, message) { if(!(condition)){ Debug::LogAssert(message, __LINE__, TEXT(__FUNCSIG__), TEXT(__FILE__)); } }

#define Check(condition, message) {if(!(condition)) { Debug::LogCheck(message, __LINE__, TEXT(__FUNCSIG__), TEXT(__FILE__));}}
#define CheckOnce(condition, message) { if(!(condition)) { static bool triggeredCheck = false; if(!triggeredCheck) { Debug::LogCheck(message, __LINE__, TEXT(__FUNCSIG__), TEXT(__FILE__)); triggerCheck = true; } }}
#define IfCheck(condition, message) ((condition) || Debug::LogCheck(!(condition), message, __LINE__, TEXT(__FUNCSIG__), TEXT(__FILE__)))
#endif